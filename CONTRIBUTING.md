# Contribution guidelines
## How to write an issue
Any problem, feature request, suggestion, or question can be submitted via the [issue tracking system](https://gitlab.cern.ch/cms-cactus/web/L1Page/issues) (requires you to be [logged in](https://gitlab.cern.ch/users/sign_in)).

If you are reporting a problem, consider that the first order of business is to be able to reliably reproduce the issue.
Therefore, kindly report as much details as you can, like:
- what operating system and browser version are you running?
- what were the steps you took to arrive at the faulty state?
- what result did you expect from the system? what did you get instead?

If relevant, and possible, screenshots are always welcome.

Do not be afraid to ask a 'dumb question', if there is any issue or frustration,
it is always best it is known as soon as possible.

# Developer Contribution guidelines
This section is targeted towards developers, who intend to contribute to the code. It is not expected of users or reporters to have this intimate knowledge of the development process.

![architecture](docs/development flow.png)

What you see here is a derived version from [Gitlab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/), which itself is derived from [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

## Quick summary

You just want to get working? Great! Here's a quick summary:

1. Create a new issue on [the issue list](https://gitlab.cern.ch/cms-cactus/web/L1Page/issues) or visit an issue assigned to you. There you can click on the ![new branch button](docs/new-branch-button.png) button

  This will create a new branch for you, in the format `<ticketnr>-<ticket-name>`

1. `git clone` the repository and/or `cd` to your local copy
1. `git pull` and `git checkout <branch-name>`
1. Happy coding! Remember to [Commit early. Commit often. And listen to your customers](https://www.x-formation.com/release-early-release-often-and-listen-to-your-customers).
1. When your code reaches a stable state, and you consider it done, [open a new pull request](https://gitlab.cern.ch/cms-cactus/web/L1Page/merge_requests/new) to the master branch.

  Your code will need to be approved before it can be merged. If there are any remarks that require extra coding, you can keep committing to your branch. A pull request always deals with the latest state of a branch.

1. When your branch has been merged, you can close the issue and delete the branch.

## More info

### Master branch
The master branch represents the latest version of the software that is considered stable.

All feature development should take place in a dedicated branch instead of the master branch. In fact, any commit directly to the master branch will be rejected.

### Issue tracker
The master branch cannot just be modified.
Problems or suggestions first needs to be reported in the [issue tracking system](https://gitlab.cern.ch/cms-cactus/web/L1Page/issues).

This presents a central place to discuss about the issue, and to assign work appropriately.

### Feature branches
Every ticket gets a dedicated feature branch.

Feature branches (or Development branches) are branched of the master branch. These are the branches that developers work on, and therefore tend to be large in numbers.

This cleanly separates the codebase for each issue, so developers can freely modify code without disturbing others.

Feature branches' names must follow the `<ticketnr>-<ticket-name>` format.

### Pull requests
When code in a feature branch is ready to be merged back into the master branch. A [new pull request](https://gitlab.cern.ch/cms-cactus/web/L1Page/merge_requests/new) can be opened.
This allows for code review by authorized parties before code is added to the master branch.

An accepted pull request and its merge does not necessarily mean the issue is resolved (although is usually does).
In some cases, an issue can be resolved in multiple stages. In that case, there will be multiple merges to master from the feature branch.

Most of the time tough, a pull requests means that the issue is considered resolved. The interface will give you the option to automatically close the issue and delete the branch when the pull request is accepted.


### Release branch
When it is time for a new release, the current state of the master branch is merged into the release branch by means of a pull request.

After the merge, apply a tag to the merge commit. This will trigger the build process.
After that, all RPMs needed for deployment are available for download as build artifacts.

This action represents a feature freeze. Only critical bugs can be added at this point using hotfix branches.

### Hotfix branches
When a release contains a problem that needs to be fixed urgently, you can create a branch based on the release branch.
They do not require a pull request to be merged into the release branch.

This allows you to quickly patch urgent bugs without too much ceremony.

Hotfix branches must be merged back into the release AND master branch.
Note that only a master can perform these merges.
