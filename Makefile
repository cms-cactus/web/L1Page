SHELL:=/bin/bash
NODE_VERSION:=12.18.3
NODE_ARCH := x64

uname_s := $(shell uname -s | tr '[:upper:]' '[:lower:]')


all: compile

### define useful alias
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
HTDOCS := $(ROOT_DIR)/htdocs
API := $(ROOT_DIR)/api
$(info Root Directory: $(ROOT_DIR))

### log-rotate
install_logrotate := $(ROOT_DIR)/setup/pm2-logrotate/node_modules.tar.gz
install_logrotate : $(install_logrotate)
$(install_logrotate) :
# pm2-logrotate, install and prepare tarball for offline install
	pm2 install pm2-logrotate;cd;tar -czf node_modules.tar.gz .pm2/node_modules;cd -;
	mv -f ~/node_modules.tar.gz setup/pm2-logrotate/node_modules.tar.gz

### on api, install node.js
nodejs := $(API)/node_modules
nodejs : $(nodejs)
$(nodejs) :
	npm --prefix $(API) update

nodejs_clean :
	@echo "Cleaning api/node_modules..."
	rm -rf $(API)/node_modules

### on htdocs, install bower components
bower_htdocs := $(HTDOCS)/bower_components
bower_htdocs : $(bower_htdocs)
$(bower_htdocs) :
	cd $(HTDOCS); bower install --allow-root 
	cd $(HTDOCS)/bower_components ; tar xzf ../../setup/font-roboto/font-roboto.tgz 

bower_htdocs_clean :
	@echo "Cleaning htdocs/bower_components..."
	rm -rf $(HTDOCS)/bower_components

### on htdocs, install grunt
grunt_htdocs := $(HTDOCS)/node_modules
grunt_htdocs : $(grunt_htdocs)
$(grunt_htdocs) :
	# cd docs/RAML; raml2html l1page.raml > ../index.html
	npm --prefix $(HTDOCS) install

grunt_htdocs_clean :
	@echo "Cleaning htdocs/node_modules..."
	rm -rf $(HTDOCS)/node_modules

### compile http interface
###   this task has too many targets and it's anyway repeated always
###   so we skip specific files dependencies and clean
compile: node nodejs bower_htdocs grunt_htdocs config
	cd $(HTDOCS); grunt

### clean
clean : nodejs_clean bower_htdocs_clean grunt_htdocs_clean node_clean
	@echo "Cleaned all files"

###
install_interface: 
	rsync -au --delete --exclude node_modules ./htdocs/ /opt/cactus/L1Page/htdocs

###
install_api: 
	rsync -au --delete --exclude ./api/ /opt/cactus/L1Page/api

node: # for stability at P5, we pack the node binary with our RPMs
	curl -Lo /tmp/node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-${uname_s}-${NODE_ARCH}.tar.xz
	tar -xJvf /tmp/node.tar.xz -C . >/dev/null
	mv node-v${NODE_VERSION}-${uname_s}-${NODE_ARCH} node
	rm -rf /tmp/node.tar.xz

node_clean:
	@echo "Cleaning local node"
	rm -rf node

###
install:
	mkdir -p /opt/cactus/L1Page
	rsync -au --delete --exclude htdocs/node_modules --exclude ./config ./* /opt/cactus/L1Page
	mkdir -p /opt/cactus/L1Page/config/; cp -nvr config/* /opt/cactus/L1Page/config/

config:
	mkdir -p config; cp -nv setup/config/cryptokeys.js setup/config/dbconfig.js setup/config/oauth.js setup/config/notifications.js config/

install_config:
	cp -n setup/database/tnsnames.ora /etc
	cp -r setup/nginx/l1page.conf /etc/nginx/conf.d/l1page.conf

post_install: install_config
	setup/makerpm/after-rpm-install.sh

test:
	# starting test
	pm2 list
	cd docs/RAML;abao l1page.raml --hookfiles=hooks.js --server https://localhost/api/v0 --timeout 60000;
	test -e /var/log/l1page/error.log && tail -n 30 /var/log/l1page/error.log

rpm: compile install_logrotate config
	version=`git describe --always`;fpm \
	-s dir \
	-t rpm \
	-n L1Page \
	-v $$version \
	-d libaio -d oracle-instantclient19.3-basic \
	-d "nginx >= 1.10.0" -d "nodejs >= 6.0.0" \
	-d node-pm2 \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--config-files /etc/nginx/conf.d/l1page.conf \
	--exclude "*/htdocs/node_modules" \
	--exclude "*/rpms" \
	--exclude "*/.git" \
	--exclude "*/._*" \
	--exclude "*/setup/database/*.rpm" \
	--description "Level-1 Page" \
	--url "https://gitlab.cern.ch/cms-cactus/web/L1Page" \
	--provides l1page \
	--after-install ./setup/makerpm/after-rpm-install.sh \
	--after-upgrade ./setup/makerpm/after-rpm-upgrade.sh \
	--after-remove ./setup/makerpm/after-rpm-remove.sh \
	.=/opt/cactus/L1Page \
	./setup/nginx/l1page.conf=/etc/nginx/conf.d/l1page.conf
	for f in *.rpm; \
	do \
		newName=$${f/-1\.x86/\.x86}; \
		mv -i "$$f" "$$newName"; \
	done;

stop:
	systemctl stop nginx
	pm2 stop l1page-api

start:
	systemctl start nginx
	pm2 start api/l1page-api.json

reload:
	systemctl reload nginx
	pm2 reload l1page-api
