"use strict";

var fs = require("fs");

const dbconfig = require('../config/dbconfig.js');
const oracledb = require('oracledb');
oracledb.autocommit = false;
oracledb.extendedMetaData = true;
oracledb.externalAuth = false;
oracledb.fetchAsString = [oracledb.CLOB];
oracledb.maxRows = 100;
oracledb.outFormat = oracledb.OBJECT;
oracledb.poolIncrement = 1;
oracledb.poolMax = 4;
oracledb.poolMin = 0;
oracledb.poolTimeout = 60;
oracledb.prefetchRows = 100;
oracledb.queueRequests = true;
oracledb.queueTimeout = 10000;
oracledb.stmtCacheSize = 30;
const io = require("./_websocket.js");

var _connection;

exports.databases = dbconfig.databases;
exports.databaseName = dbconfig.databaseName;
module.exports.notifyTo = dbconfig.notifyTo ? dbconfig.notifyTo.join(', ') : '';

/*
 * renders an object that contains the parameters to use for db.connect()
 */
exports.getConnectionParams = () => {
  var dbinfo = dbconfig.databases[dbconfig.databaseName];
  var username = dbinfo.username;
  var password = fs.readFileSync(`${dbinfo.securePath}${username.toLowerCase()}${dbinfo.suffix}`, "utf-8").replace("\n","");
  return {
    user: username,
    password: password,
    connectString: dbinfo.connectString,
    database: dbconfig.databaseName
  };
}


module.exports.getConnection = () => _connection;

module.exports.connect = () => {
  console.info(`connecting to ${dbconfig.databaseName} database`);

  return Promise.race([
    oracledb.getConnection(this.getConnectionParams()),
    new Promise( (ok, nope) => {
      var timeout = 5000;
      setTimeout(nope, timeout, `the connection took too long to set up (${timeout}ms)`);
    })
  ])
  .then( connection => {
    connection.clientId = "NodeJS";
    connection.module = "L1Page";

    // Provide convenient alternative to connection.execute, which silently
    // caps results from large tables.
    // This function always gives the full query result in a way that doesn't
    // overload the connection/database
    connection.executeSync = (sql, bindParams, options) => new Promise((ok, damn) => {
      var result = {
        rows: [],
        metaData: []
      };
      var stream = connection.queryStream(sql, bindParams, options);
      stream.on('error', function (error) {
        return damn(error);
      });

      // rows come in one by one
      stream.on('data', function (data) {
        result.rows.push(data);
      });

      stream.on('end', function () {
        return ok(result);
      });

      // metaData seems to come all in one go
      stream.on('metadata', function (metaData) {
        result.metaData = metaData;
      });
    });

    _connection = connection;

    console.success(`connected to ${dbconfig.databaseName} database (${dbconfig.databases[dbconfig.databaseName].username})`);

    return connection.execute("ALTER SESSION SET TIME_ZONE='UTC'")
  })
};
module.exports.disconnect = () => {
  if (_connection) {
    return _connection.release()
    .then( () => console.success(`disconnected to ${dbconfig.databaseName} database`) );
  } else {
    return Promise.reject("connection was not yet establised");
  }
};
