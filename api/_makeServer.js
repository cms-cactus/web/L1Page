"use strict";

const restify = require('restify');
const restifyJSONHAL = require("restify-json-hal");

const prefix = "/api";

const server = restify.createServer({
  name: "L1Page"
});
server.server.setTimeout(60000);
server.formatters['text/html'] = server.formatters['text/plain'];
// server.formatters['application/xml'] = function( req, res, body, cb ) {
//    if (body instanceof Error)
//       return body.stack;
//
//    if (Buffer.isBuffer(body))
//       return cb(null, body.toString('base64'));
//
//    return cb(null, body);
// }
server.use(restify.plugins.gzipResponse());

/*restify.CORS.ALLOW_HEADERS.push('Accept');
restify.CORS.ALLOW_HEADERS.push('Accept-Version');
restify.CORS.ALLOW_HEADERS.push('Content-Type');
restify.CORS.ALLOW_HEADERS.push('Api-Version');
restify.CORS.ALLOW_HEADERS.push('X-Requested-With');
restify.CORS.ALLOW_HEADERS.push('Authorization');
restify.CORS.ALLOW_HEADERS.push('sid');
restify.CORS.ALLOW_HEADERS.push('lang');
restify.CORS.ALLOW_HEADERS.push('origin');
restify.CORS.ALLOW_HEADERS.push('withcredentials');
server.use(restify.CORS());*/

const corsMiddleware = require('restify-cors-middleware')
const cors = corsMiddleware({
  origins: [ `${global}`],
  allowHeaders: ['Accept',
    'Accept-Version',
    'Content-Type',
    'Api-Version',
    'X-Requested-With',
    'Authorization',
    'sid',
    'lang',
    'origin',
    'subsystems',
    'withcredentials' ],
  exposeHeaders: []
})

server.pre(cors.preflight)
server.use(cors.actual)
server.use(restifyJSONHAL(server, {
  overrideJSON: true
}));

server.use(restify.plugins.queryParser( { mapParams : true } ) );
server.use(restify.plugins.bodyParser( { mapParams : true } ) );
server.use(restify.plugins.fullResponse());
server.on('ResourceNotFound', (request, response, error, callback) => {
  response.send(404, error);
  return callback();
});

function unknownMethodHandler(req, res) {
  console.log("unknownMethodHandler", req.method);
  if (req.method.toLowerCase() === 'options') {
    if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

    res.header('Access-Control-Allow-Credentials', true);
    var allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Origin', 'X-Requested-With', 'Authorization'];
    res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
    res.header('Access-Control-Allow-Methods', res.methods.join(', '));
    res.header('Access-Control-Allow-Origin', req.headers.origin);

    return res.send(200);
  } else {
    return res.send(new restify.MethodNotAllowedError());
  }
}

server.on('MethodNotAllowed', unknownMethodHandler);

// remove trailing /, this way our code keeps working as expected
// e.g. /users/ becomes /users
server.pre(restify.pre.sanitizePath());

server.pre(function responseLogger(request, response, next) {
  var realSend = response.send.bind(response);
  response.send = function wrappedSend(code, body, headers) {
    if (code && code.constructor.name === 'Number' && code >= 400 && code != 401) {
      if (request.params.row) {
        delete request.params.row.CONF;
      }
      console.error(`code ${code} returned
endpoint: ${request.method} ${request.url}
user: ${request.user ? request.user.name : "anonymous"}
params: ${JSON.stringify(request.params, null, 2)}
response: ${body.message || body}`);
    }
    realSend(code, body, headers);
  }
  console.info("handling", request.url);
  return next();
});

server.sanitize = function sanitize(request, response, next) {
  // get all `:varname` pieces in endpoint path
  var params = request.route.path.match(/:[a-zA-Z1-9-_]*/g).map( param => param.substring(1))

  // check all those parameters for non-whitelisted characters
  var invalid = params.filter(name => name != "token").some( paramName => !/^[A-Za-z0-9-_ \/]*$/.exec(request.params[paramName]))
  if (!invalid) {
    return next();
  }
  else {
    response.send(400, "invalid input");
    response.end();
  }
}

var _rootFolder = __dirname;
server.getAPIRoot = function(dirname) {
  return prefix + dirname.substring(_rootFolder.length);
}

module.exports = server;
