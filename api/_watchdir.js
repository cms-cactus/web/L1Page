'use strict';

function unloadModule(path, loadedModules) {
  if (path in loadedModules) {
    delete loadedModules[path];
    delete require.cache[require.resolve(path)];
    console.log(`Unloaded module ${path}`);
  }
}

const loadModule = function(path, loadedModules, validatorFunc) {
  try {
    let m = require(path);
    if (validatorFunc(m)) {
      // since objects are passed by reference in js this will persist also outside this function
      loadedModules[path] = m;
      console.log(`${path} loaded`);
    } else {
      delete require.cache[require.resolve(path)];
      console.warn(`${path} did not pass the validation function and was not loaded`);
    }
  } catch (e) {
    console.error(`module ${path} cannot be loaded`, e);
  }
};

// validator function defaults to true, for modules that don't need a validation respectively to keep backwards compatibility
/**
 * Watch and automatically load modules from a location watched by a chokidar instance.
 *
 * This function internally keeps track of modules loaded from the location watched by the chokidar instance.
 * Whenever a file is added, changed or removed the handleFunc is called with all modules that are currently loaded.
 * It is possible to validate the modules before loading by passing them through an optional validatorFunc. If no validator function is passed, all modules are assumed to be valid and are automatically loaded.
 *
 * @param {object returned by chokidar.watch()} watcher - the chokidar instance that emits and handles all the events
 * @param {function} handleFunc - function taking an object where the keys are the paths to the modules and the values are the modules itself
 * @param {function, optional} validatorFunc - function that takes a single module
 */
function watchDirectory(watcher, handleFunc, validatorFunc=function(module){return true;}) {
  let loadedModules = {};

  watcher
    .on('add', path => {
      console.log(`${path} added`);
      loadModule(path, loadedModules, validatorFunc);
      handleFunc(loadedModules);
    }).on('unlink', path => {
      console.log(`${path} removed`);
      unloadModule(path, loadedModules);
      handleFunc(loadedModules);
    }).on('change', path => {
      console.log(`${path} changed`);
      unloadModule(path, loadedModules);
      loadModule(path, loadedModules, validatorFunc);
      handleFunc(loadedModules);
    }).on('error', error => console.log(error));
}

module.exports = watchDirectory;
