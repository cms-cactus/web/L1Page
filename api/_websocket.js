"use strict";

const server = require('http').createServer();
server.listen(3010, "::1");
const io = require('socket.io')(server);
const EventEmitter = require('events');
const emitter = new EventEmitter();

io.on('connection', socket => {

});

io.notifyUpdatedResource = path => {
  // console.debug("updated resource", path);
  io.emit('updated resource', path);
  emitter.emit('updated resource', path);
};

module.exports = io;
module.exports.emitter = emitter;
