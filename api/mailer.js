'use strict';
const nodemailer = require('nodemailer');

module.exports.sendMail = (message, recipients, subject) => {
    let transporter = nodemailer.createTransport({
        sendmail: true,
        newline: 'unix',
        path: '/usr/sbin/sendmail'
    });
    transporter.sendMail({
        from: 'L1Page Notifications <l1page@cern.ch>',
        to: recipients,
        subject: subject,
        text: message
    }, (err, info) => {
        if ( err ) {
            console.error('sendMail: sending notification to', recipients);
            console.error("sendMail: error sending mail: ", err);
        } else {
            console.log('sendMail: notification sent to', recipients);
        }
    });
  }


