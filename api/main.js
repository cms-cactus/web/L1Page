#!/usr/bin/env node
"use strict";

const consoleConfig = require('formal-console');
const server = require('./_makeServer.js');
const db = require('./_database.js');
const io = require("./_websocket.js");

db.connect()
.catch( error => {
  console.error("Error while setting up database connection:", error);
  process.abort();
})
.then( result => {
  require("./v0/index.js")(server); // pieces of the API rely on a working DB connection
  server.listen( 8091, "::1", () => console.log(`${server.name} listening at ${server.url}`) );
})
.catch( error => {
  console.error("Error while starting API:", error);
  process.abort();
});

process.on('SIGINT', () => {
  console.info("Caught SIGINT signal, halting");
  db.disconnect()
  .then( result => {
    process.exit();
  })
  .catch( error => {
    console.error("Error while disconnecting database:", error);
    process.abort();
  });
});

process.on('unhandledRejection', (reason, promise) => {
  console.error("unhandledRejection!", reason, promise);
});
