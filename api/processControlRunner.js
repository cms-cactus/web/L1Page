"use strict";

const os = require("os");
const ssh_exec = require('ssh-exec');
const fs = require('fs');
const exec = require('child_process').exec;

const keyPromise = new Promise((ok, nope) => {
  var username = os.userInfo().username;
  exec(`echo ~${username}`, (error, stdout, stderr) => {
    if (error) {
      nope(error);
    }
    if (stderr && stderr.length != 0) {
      nope(stderr);
    }
    let homefolder = stdout.replace("\n", "");
    let keyfile = fs.readFile(`${homefolder}/.ssh/id_rsa`, "UTF-8", (err, keyfile) => {
      if (err) {
        nope(err);
      }
      else {
        ok(keyfile);
      }
    });
  });
});

process.on('message', function(job) {
  keyPromise.then( key => {
    var user = os.userInfo().username;
    var host = job.host;
    ssh_exec(job.command, {
      user: user,
      host: host,
      key: key
    }, function ssh_callback(err, stdout, stderr) {
      if (err || stderr) {
        process.send({
          code: 1,
          message: `error: ${err.message || err}\nstderr: ${stderr}\nstdout: ${stdout}`
        });
      }
      else {
        process.send({
          code: 0,
          message: `on host '${user}@${host}':\n` + stdout
        });
      }
      process.exit();
    });
  })
  .catch( error => {
    process.send({
      code: 1,
      message: error.message || error
    });
  })
});
