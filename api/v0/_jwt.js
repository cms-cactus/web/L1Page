"use strict";

const restify = require('restify');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const fs = require('fs');
const cryptoInfo = require.main.require('../config/cryptokeys.js');
const public_key = fs.readFileSync(cryptoInfo.location + cryptoInfo.public_key);
const version = __dirname.substring(__dirname.lastIndexOf("/")+1);

passport.use(
  new JwtStrategy({
      secretOrKey: public_key,
      jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
      algorithms: ["RS256"]
    },
    (jwt_data, done) => {
      // jwt_data contains the JWT payload like in https://jwt.io/
      done(jwt_data);
    })
);


module.exports = {

  authenticate: (request, response, next) => {

    var onResponse = (user, info, error) => {
      if (error) {
        if (error.message == "No auth token") {
          response.send(401, {
            code: 401,
            message: "JWT token not found. It must be included in the request headers in the following form: 'Authorization: JWT <token>'"
          });
        }
        else if (error.message == "invalid token") {
          response.send(401, {
            code: 401,
            message: "JWT token is invalid"
          });
        }
        else {
          console.error("passport.jwt.authenticate error", error);
          response.send(403, {
            code: 403,
            message: error.message || error
          });
        }
      }
      else {
        request.user = user;
        try {
          next();
        } catch (e) {
          response.send(500, e);
        }
      }
    };

    passport.authenticate('jwt', onResponse)(request, response, next);
  }

}
