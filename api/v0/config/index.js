"use strict";

const restify = require('restify');
const subsystemconf = require.main.require('../settings/subsystems_config.js');
const external_services = require.main.require('../settings/external_links.js');
var root;

module.exports = server => {
  root = server.getAPIRoot(__dirname);

  server.get(`${root}/`, (request, response) => {
    response.send({});
  });

  // server.get(`${root}/topology`, (request, response) => {
  //   response.send(subsystemconf.topology);
  // });

  server.get(`${root}/subsystems`, (request, response) => {
    response.send({
      subsystems: subsystemconf.subsystems
    });
  });

  server.get(`${root}/links`, (request, response) => {
    response.send({
      apps: external_services
    });
  });

}
