"use strict";

const restify = require('restify');
const db = require.main.require('./_database.js');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const cryptoInfo = require.main.require('../config/cryptokeys.js');
const private_key = fs.readFileSync(cryptoInfo.location + cryptoInfo.private_key);
const htmlfile = fs.readFileSync(__dirname + '/saveJWT.html').toString();
const passport_jwt = require('./_jwt.js');
const io = require.main.require('./_websocket.js');
const http = require('request-promise-native');
const oauth_config = require.main.require("../config/oauth.js");
const {proxy_url} = require.main.require("../settings/proxy");

const poller = require('./subsystems/_flashlist_poller.js');
var flashlistData = "No flashlist data received yet";
poller.on('new data', response => {
  flashlistData = response;
});
poller.on('error', error => {
  flashlistData = error;
});

module.exports = (server) => {
  var root = server.getAPIRoot(__dirname);
  require("./subsystems")(server);
  require("./run")(server);
  require("./config")(server);
  require("./rates")(server);
  require("./shifter-information")(server);
  require("./suggestions")(server);

  server.get(`${root}/`, (request, response, next) => {
    response.send({});
    return next();
  });

  poller.on('new data', response => io.notifyUpdatedResource(`${root}/flashlist`));
  poller.on('error', error => io.notifyUpdatedResource(`${root}/flashlist`));
  server.get(`${root}/flashlist`, (request, response, next) => {
    response.send(flashlistData);
    return next();
  });

  server.get(`${root}/handle_oauth`, (request, response) => {
    var proxy_http = !proxy_url || proxy_url == "" ? http : http.defaults({
      proxy: "http://proxy-vip:3128"
    });
    proxy_http.post('https://oauth.web.cern.ch/OAuth/Token', {
      form: {
        code: request.params.code,
        grant_type: "authorization_code",
        client_secret: oauth_config.secret,
        redirect_uri: oauth_config.redirect_uri,
        client_id: oauth_config.client_id
      },
      json: true
    })
    .then( ({access_token}) => {
      var options = {
        headers: {
          "Authorization": `Bearer ${access_token}`
        },
        json: true
      };
      return Promise.all([
        proxy_http.get('https://oauthresource.web.cern.ch/api/User', options),
        proxy_http.get('https://oauthresource.web.cern.ch/api/Groups', options)
      ])
    } )
    .then( ([user, groups]) => {
      user["e-groups"] = groups.groups.filter( group => oauth_config.egroup_whitelist.indexOf(group) != -1);
      sendToken(request, response, makeJWTToken(user));
    } )
    .catch( error => response.send(500, error))
  })

  function makeJWTToken(userInfo, duration = "24h") {
    delete userInfo.exp;
    // delete userInfo.iat;
    return jwt.sign(userInfo, private_key, {
      expiresIn: duration,
      algorithm: 'RS256'
    });
  }

  server.get(root + '/whoami',
    passport_jwt.authenticate,
    (request, response) => {
      response.send(request.user);
    }
  );

  /**
   * Sends a JWT token to the API consumer
   * It decides between sending a HTML and JSON response
   */
  function sendToken(request, response, token) {
    if (request.accepts('text/html')) {
      response.setHeader('content-type', 'text/html');
      response.send(htmlfile.replace("<%jwt_token%>", token));
    } else {
      response.send({
        token: token
      });
    }

  }

  server.get(`${root}/login`,
    (request, response) => {
      response.setHeader('Location', `https://oauth.web.cern.ch/OAuth/Authorize?client_id=${oauth_config.client_id}&redirect_uri=${oauth_config.redirect_uri}&response_type=code`);
      response.send(307, {});
    }
  )

  server.get(`${root}/test_timeout`, (request, response) => {});

}
