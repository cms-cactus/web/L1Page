"use strict";

const REPOLLING_INTERVAL = 5000; // in ms

const db = require.main.require('./_database.js');
const EventEmitter = require('events');
const connection = db.getConnection();
const eventEmitter = new EventEmitter();
// for runData
const runPoller = require.main.require('./v0/run/_db_poller.js');

module.exports = eventEmitter;

/**
 * database queries to obtain the rates.
 * Bind parameters are: runNumber and LUMI_ID
 */
const rateQueryStrings = {
  global: `select scal.ALGO_RATE as RATE, names.ALGO_NAME as NAME, scal.SCALER_TYPE as TYPE
           from CMS_UGT_MON.ALGO_SCALERS scal, CMS_UGT_MON.UGT_RUN_ALGO_SETTING names
           where scal.ALGO_INDEX=names.ALGO_INDEX and scal.SCALER_TYPE<2
           and names.RUN_NUMBER=:runNumber and scal.LUMI_SECTIONS_ID=:LUMI_ID
           order by scal.ALGO_RATE desc`,

  muon: `select RATE, ALIAS as NAME from CMS_UGMT_MON.VIEW_UGMT_RATES
         where RUN_NUMBER=:runNumber and LUMI_SECTION=:LUMI_ID
         order by RATE desc`
};

/** Names of the emitted events for the different types of rates. */
const eventNames = {
  global: 'new global rates',
  muon: 'new muon rates',
  calo: 'new calo rates'
};

const runLumiUnpackers = {
  lumiId: (runLumiInfo) => {return {runNumber: runLumiInfo.runNumber, LUMI_ID: runLumiInfo.lumiSectionId }; },
  lumiSection: (runLumiInfo) => {return {runNumber: runLumiInfo.runNumber, LUMI_ID: runLumiInfo.lumiSection };}
};

class RepeatedQuery {
  constructor(query, eventName, runLumiUnpacker, emitter=eventEmitter) {
    this.query = query,
    this.eventName = eventName;
    this.emitter = emitter;
    this.debouncer = undefined;
    this.unpacker = runLumiUnpacker;
  }

  startQuerying(runLumiInfo) {
    clearTimeout(this.debouncer);
    this._runRateQuery(runLumiInfo);
  }

  _runRateQuery(runLumiInfo) {
    console.log(`running rate query for ${this.eventName} for lumi section id: ${runLumiInfo.lumiSectionId}`);
    connection.executeSync(this.query, this.unpacker(runLumiInfo))
      .then( ({rows: rates}) => {
        if (rates.length) {
          console.log(`Got array with ${rates.length} rates from query for ${this.eventName}, ${runLumiInfo.lumiSectionId}`);
          let ratesData = {
            lumisection: runLumiInfo.lumiSection,
            rates: rates
          };
          this.emitter.emit(this.eventName, ratesData);
        } else {
          // use 'fat arrow' notation in setTimeout to have this still point to the current object
          console.log(`Got empty response from query for ${this.eventName}, ${runLumiInfo.lumiSectionId}, trying again in a bit`);
          this.debouncer = setTimeout(() => {
            this._runRateQuery(runLumiInfo);
          }, REPOLLING_INTERVAL);
        }
      }).catch( error => {
        this.emitter.emit('error', error);
        // use 'fat arrow' notation in setTimeout to have this still point to the current object
        this.debouncer = setTimeout(() => {
          this._runRateQuery(runLumiInfo);
        }, REPOLLING_INTERVAL);
      });
  }

  stopQuerying() {
    clearTimeout(this.debouncer);
  }

  emitEmpty() {
    this.emitter.emit(this.eventName, {rates: []});
  }
}

const rateQueries = {
  UGT: new RepeatedQuery(rateQueryStrings.global, eventNames.global, runLumiUnpackers.lumiId),
  UGMT: new RepeatedQuery(rateQueryStrings.muon, eventNames.muon, runLumiUnpackers.lumiSection),
  // use a dummy query that always emits rates until calo has a proper query
  CALOL2: {
    startQuerying: function(runLumiInfo) {
      eventEmitter.emit(eventNames.calo, {lumisection: runLumiInfo.lumiSection, rates: []});
    },
    stopQuerying: function() {},
    emitEmpty: function() {
      eventEmitter.emit(eventNames.calo, {rates: []});
    }
  }
};

let _lastLumiId = null;
runPoller.on('new data', newData => {
  let {triggerState, runNumber, trgMoni, includedSubsystems} = newData;
  let {LUMI_SECTION: lumiSection, LUMI_SECTION_ID: lumiSectionId} = trgMoni;

  let runLumiInfo = {runNumber: runNumber, lumiSectionId: lumiSectionId, lumiSection: lumiSection};

  if (!_lastLumiId || _lastLumiId != lumiSectionId) {
    _lastLumiId = lumiSectionId;

    if (triggerState.toLowerCase() == 'running' && lumiSection && lumiSectionId) {

      for (let subsys in rateQueries) {
        if (includedSubsystems.includes(subsys)) {
          rateQueries[subsys].startQuerying(runLumiInfo);
        }
      }

    } else {
      for (let subsys in rateQueries) {
        rateQueries[subsys].stopQuerying();
        rateQueries[subsys].emitEmpty();
      }
    }
  }
});
