"use strict";

const restify = require('restify');
const io = require.main.require('./_websocket.js');
const rates_poller = require('./_rates_poller.js');
const subsystemconf = require.main.require('../settings/subsystems_config.js');
const watchdir = require.main.require('./_watchdir.js');
const chokidar = require('chokidar');
const fs = require('fs');
var root;

/** Dummy map holding the information of which type of rate corresponds to which index*/
const typeIdxMap = {
  0: 'post-PS, pre-DT',
  1: 'pre-PS, pre-DT'
};


module.exports = (server) => {
  root = server.getAPIRoot(__dirname);

  server.get(`${root}/global`, (request, response, next) => {
    response.send(globalRatesData);
    return next();
  });
    server.get(`${root}/muon`, (request, response, next) => {
    response.send(muonRatesData);
    return next();
  });
  server.get(`${root}/calo`, (request, response, next) => {
    response.send(caloRatesData);
    return next();
  });
  server.get(`${root}/thresholds`, (request, response, next) => {
    response.send(thresholdData);
    return next();
  });
}

var _lastRatesError = null;
var _lastRatesWarning = null;
var globalRatesData = module.exports.globalRatesData = {};
var muonRatesData = module.exports.muonRatesData = {};
var caloRatesData = module.exports.caloRatesData = {};

rates_poller.on('error', error => {
  console.error("Error while fetching run data from db", error.message);
  if (error.message != _lastRatesError) {
    _lastRatesError = error.message;
    console.error("full error: ", error);
  }
});
rates_poller.on('warning', warning => {
  if (_lastRatesWarning != warning) {
    _lastRatesWarning = warning;
    console.warn(warning);
  }
});
rates_poller.on('new global rates', newData => {
  _lastRatesError = null;

  let newDataNorm = {};
  newDataNorm.lumisection = newData.lumisection;
  newDataNorm.rates = normalizeRateData(newData.rates, typeIdxMap);

  // check for changes, trigger update notification if needed
  delete globalRatesData._links;
  var somethingChanged = JSON.stringify(globalRatesData) != JSON.stringify(newDataNorm);
  globalRatesData = newDataNorm;
  if (somethingChanged) {
    io.notifyUpdatedResource(`${root}/global`);
  }
});
rates_poller.on('new muon rates', newData => {
  _lastRatesError = null;

  // check for changes, trigger update notification if needed
  delete muonRatesData._links;
  var somethingChanged = JSON.stringify(muonRatesData) != JSON.stringify(newData);
  muonRatesData = newData;
  if (somethingChanged) {
    io.notifyUpdatedResource(`${root}/muon`);
  }
});
rates_poller.on('new calo rates', newData => {
  _lastRatesError = null;

  // check for changes, trigger update notification if needed
  delete caloRatesData._links;
  var somethingChanged = JSON.stringify(caloRatesData) != JSON.stringify(newData);
  caloRatesData = newData;
  if (somethingChanged) {
    io.notifyUpdatedResource(`${root}/calo`);
  }
});


/**
 * Normalize rate data
 */
function normalizeRateData(rawData, typeIdxMap) {
  let normData = [];
  // to avoid having to look up the idx for the name everytime it is encountered
  let nameToIdxMap = {};
  for (let entry of rawData) {
    let entryName = entry.NAME;
    let listIdx = undefined;
    if (entryName in nameToIdxMap) {
      listIdx = nameToIdxMap[entryName];
    } else {
      listIdx = normData.length;
      nameToIdxMap[entryName] = listIdx;
    }

    if (listIdx === normData.length) {
      normData.push({NAME: entryName});
    }

    let normElement = normData[listIdx];
    normElement[typeIdxMap[entry.TYPE]] = entry.RATE;
  }

  return normData;
}


var thresholdData = {};
function updateThresholds(loadedThresholds) {
  console.log('Updating trigger thresholds');
  // reset every time this function is called to make sure that sent thresholds are the most recent ones
  thresholdData = {
    general: undefined,
    specific: {},
    auxInfo: {
      thresholdLocation: thresholdLocation,
      loadedThresholds: [],
      warnings: []
    }};

  for (let threshKey in loadedThresholds) {
    console.log(`Loading thresholds from: ${threshKey}`);
    thresholdData.auxInfo.loadedThresholds.push(threshKey);
    let threshold = loadedThresholds[threshKey];
    // if the module exports only a high and a low threshold assume that it is a general threshold
    // else add the contents to the specific section retaining any already existing thresholds
    if ('high' in threshold && 'low' in threshold) {
      if (!thresholdData.general) {
        thresholdData.general = {high: threshold.high, low: threshold.low};
      } else {
        console.warn('Multiple general thresholds in settings! Ignoring the ones from ', threshKey);
        thresholdData.auxInfo.warnings.push('Multiple general thresholds in settings');
      }
    } else {
      // all thresholds here are validated, so we can simply assign them
      Object.assign(thresholdData.specific, threshold);
    }
  }
  io.notifyUpdatedResource(`${root}/thresholds`);
}

// setup the watcher so that it finds the module with the location of the thresholds
const settingsWatcher = chokidar.watch(__dirname + '/../../../settings/rates/',
                                       {ignored: /examples\//});
watchdir(settingsWatcher, updateThresholdLocation, function(locSettings) {
  let valid = 'location' in locSettings;
  if (!valid) {
    console.warn('location setting for threshold does not have \'threshold\' property, cannot load thresholds');
  }
  return valid;
});

var thresholdLocation = null;
function updateThresholdLocation(loadedLocations) {
  console.log('Updating trigger threshold location'); // make this more visible logs?
  // reset location
  thresholdLocation = null;
  // if there are at any point more than two locations loaded, that is an error
  if (Object.keys(loadedLocations).length > 1) {
    console.error('More than one threshold location present in settings, not loading thresholds');
    // if we don't have a valid location, reset the thresholdData and notify the client
    // if we have a valid location, the current thresholds will be cleared anyway before filling in the new ones
    thresholdData = {
      general: undefined, specific: {},
      auxInfo: {warnings: ['More than one location in settings']}
    };
    io.notifyUpdatedResource(`${root}/thresholds`);
  } else {
    // loop is only here to allow for an arbitrary filename in the settings
    for (let loc in loadedLocations) {
      let warning = undefined;
      thresholdLocation = loadedLocations[loc].location;
      if (thresholdLocation !== '') {
        try {
          let locStat = fs.lstatSync(thresholdLocation);
          if (locStat.isDirectory()) {
            updateThresholdWatcher(thresholdLocation);
          } else {
            console.warn(`${thresholdLocation} specified in ${loc} is not a directory`);
            warning = 'threshold location is not a directory';
          }
        } catch (e) {
          console.error(`${thresholdLocation} specified in ${loc} does not exist. No thresholds loaded`);
          warning = 'threshold location does not exist';
        }
      } else {
        warning = 'no threshold location specified';
      }
      // if we have a warning message here something went wrong above
      if (warning) {
        thresholdData = {
          general: undefined, specific: {},
          auxInfo: {
            warnings: [warning],
            thresholdLocation: thresholdLocation
          }
        };
        io.notifyUpdatedResource(`${root}/thresholds`);
      }
    }
  }
}

function updateThresholdWatcher(location) {
  console.log('New trigger threshold location: ', location);
  const watcher = chokidar.watch(location);
  watchdir(watcher, updateThresholds, validateThresholdModule);
}

function validateThresholdModule(thresholdModule) {
  // a valid module is either a general one that definens high and low property
  // or a specific one that has a high and low property for every key it defines
  function hasOnlyHighLow(thresholdSetting) {
    let keys = Object.keys(thresholdSetting);
    if (keys.length == 2 && keys.includes('high') && keys.includes('low')) return true;
    return false;
  };

  // check for general module
  if (hasOnlyHighLow(thresholdModule)) return true;

  // check specific module
  // TODO: replace this current workaround with .keys -> .map with .values once Object.values is available in node.js (v7) (see below)
  return Object.keys(thresholdModule).map((k) => thresholdModule[k]).every(hasOnlyHighLow);
  // return Object.values(thresholdData).every(hasOnlyHighLow);
}
