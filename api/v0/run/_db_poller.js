"use strict";

const POLLING_INTERVAL = 5000; // in ms

const db = require.main.require('./_database.js');
const EventEmitter = require('events');
const connection = db.getConnection();
const eventEmitter = new EventEmitter();

module.exports = eventEmitter;

function confQuery( L1_HLT_MODE_ID, L1_TRG_CONF_KEY, L1_TRG_RS_KEY ) {
  if ( L1_HLT_MODE_ID ) {
    return connection.execute(
      `select ID, L1_HLT_MODE, VERSION, L1_TRG_CONF_KEY, L1_TRG_RS_KEY, L1_MENU,
        UGT_KEY, UGMT_KEY, CALOL1_KEY, CALOL2_KEY, BMTF_KEY, EMTF_KEY, OMTF_KEY, TWINMUX_KEY, CPPF_KEY,
        UGT_RS_KEY, UGMT_RS_KEY, CALOL1_RS_KEY, CALOL2_RS_KEY, BMTF_RS_KEY, EMTF_RS_KEY, OMTF_RS_KEY, TWINMUX_RS_KEY, CPPF_RS_KEY
        from CMS_L1_HLT.V_L1_FULL_CONF where ID=:L1_HLT_MODE_ID`,  //`
      { L1_HLT_MODE_ID : L1_HLT_MODE_ID } );
  } else if ( L1_TRG_CONF_KEY && L1_TRG_RS_KEY ) {
    return  connection.execute(
      `select * from (select ID, L1_HLT_MODE, VERSION, L1_TRG_CONF_KEY, L1_TRG_RS_KEY, L1_MENU,
        UGT_KEY, UGMT_KEY, CALOL1_KEY, CALOL2_KEY, BMTF_KEY, EMTF_KEY, OMTF_KEY, TWINMUX_KEY, CPPF_KEY,
        UGT_RS_KEY, UGMT_RS_KEY, CALOL1_RS_KEY, CALOL2_RS_KEY, BMTF_RS_KEY, EMTF_RS_KEY, OMTF_RS_KEY, TWINMUX_RS_KEY, CPPF_RS_KEY
        from CMS_L1_HLT.V_L1_FULL_CONF where L1_TRG_CONF_KEY=:L1_TRG_CONF_KEY and L1_TRG_RS_KEY=:L1_TRG_RS_KEY
        order by VERSION desc ) where rownum = 1`,  //`
      { L1_TRG_CONF_KEY : L1_TRG_CONF_KEY, L1_TRG_RS_KEY : L1_TRG_RS_KEY } );
  } else {
    return  Promise.resolve( {rows: []} );
  }
}


function updateDB() {
  connection.execute("select max(runnumber) as runnumber from cms_runinfo.RUNSESSION_PARAMETER where name='CMS.TRG:L1_TRG_CONF_KEY'")
  .then( ({rows: [{RUNNUMBER: runNumber}]}) => Promise.all(
      [
        // pass on runNumber
        Promise.resolve(runNumber),
        // get HLT mode key
        connection.execute(`
          select string_value as l1_hlt_mode_id
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:TRIGGER_MODE_ID_AT_START'`,
          {runnumber: runNumber}),
        // get L1 key
        connection.execute(`
          select L1_TRG_CONF_KEY
          from
            (select time, string_value as L1_TRG_CONF_KEY
             from cms_runinfo.RUNSESSION_PARAMETER
             where runnumber=:runnumber
             and name='CMS.TRG:L1_TRG_CONF_KEY'
             order by time desc)
          where rownum = 1`,
          {runnumber: runNumber}),
        // get TSC key
        connection.execute(`
          select L1_TRG_RS_KEY
          from
             (select time, string_value as L1_TRG_RS_KEY
             from cms_runinfo.RUNSESSION_PARAMETER
             where runnumber=:runnumber
             and name='CMS.TRG:L1_TRG_RS_KEY'
             order by time desc)
          where rownum = 1`,
          {runnumber: runNumber}),
        // get trigger state (e.g. 'Running')
        connection.execute(`
          select TRG_STATE
          from
            (select time, string_value as TRG_STATE
             from cms_runinfo.runsession_parameter
             where runnumber=:runnumber
             and name='CMS.LVL0:TRG_STATE'
             order by time desc)
          where rownum = 1`,
          {runnumber: runNumber}),
              // get run start time
        connection.execute(`
          select string_value as STARTTIME
          from cms_runinfo.runsession_parameter
          where runnumber=:runnumber
          and name='CMS.LVL0:START_TIME_T'`,
          {runnumber: runNumber}),
        // get list of subsystems in this run
        connection.execute(`
          select string_value as IncludedSubs
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.TRG:IncludedSystems'`,
          {runnumber: runNumber}),
        connection.execute(`
          select beam1_stable, beam2_stable
          from (
            select
              ls.lhcfill,
              ls.beam1_stable,
              ls.beam2_stable,
              ls.STARTTIME,
              rownum
            from cms_runtime_logger.LUMI_SECTIONS ls
            where ls.lhcfill is not null
            and ls.lhcfill !=0 and ls.runnumber=:runnumber
            order by starttime desc
            ) haveWeGotBeams
          where rownum = 1`, {runnumber: runNumber}),
        // get DT status
        connection.execute(`
          select string_value
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:DT'`,
          {runnumber: runNumber}),
        // get ECAL status
        connection.execute(`
          select string_value
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:ECAL'`,
          {runnumber: runNumber}),
        // get HCAL status
        connection.execute(`
          select string_value
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:HCAL'`,
          {runnumber: runNumber}),
        // get RPC status
        connection.execute(`
          select string_value
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:RPC'`,
          {runnumber: runNumber}),
        // get CSC status
        connection.execute(`
          select string_value
          from cms_runinfo.RUNSESSION_PARAMETER
          where runnumber=:runnumber
          and name='CMS.LVL0:CSC'`,
          {runnumber: runNumber}),
        // get LumiSection and PrescaleIndex
        connection.execute(`
          select lumi.ID, lumi.LUMI_SECTION, pnames.PRESCALE_INDEX, pnames.PRESCALE_NAME
          from CMS_UGT_MON.LUMI_SECTIONS lumi left outer join CMS_UGT_MON.RUN_PRESCALENAMES pnames
          on lumi.PRESCALE_INDEX=pnames.PRESCALE_INDEX and pnames.RUN_NUMBER=:runNumber
          where (lumi.RUN_NUMBER, lumi.LUMI_SECTION)
            in (select RUN_NUMBER, max(LUMI_SECTION) from CMS_UGT_MON.LUMI_SECTIONS where RUN_NUMBER=:runNumber GROUP BY RUN_NUMBER)`,
          { runNumber: runNumber, runNumber: runNumber })
      ]
    )
  ).then( ([
        runNumber,
        {rows: [obj_L1_HLT_MODE_ID]},
        {rows: [obj_L1_TRG_CONF_KEY]},
        {rows: [obj_L1_TRG_RS_KEY]},
        {rows: [obj_TRG_STATE]},
        {rows: [obj_STARTTIME]},
        {rows: [obj_INCLUDEDSUBS]},
        {rows: [obj_BEAMSTABLE]},
        {rows: [obj_DT_STATUS]},
        {rows: [obj_ECAL_STATUS]},
        {rows: [obj_HCAL_STATUS]},
        {rows: [obj_RPC_STATUS]},
        {rows: [obj_CSC_STATUS]},
        {rows: [obj_TRG_RUN]},
    ]) => {

      var L1_HLT_MODE_ID  = obj_L1_HLT_MODE_ID  ? obj_L1_HLT_MODE_ID.L1_HLT_MODE_ID : undefined;
      var L1_TRG_CONF_KEY = obj_L1_TRG_CONF_KEY ? obj_L1_TRG_CONF_KEY.L1_TRG_CONF_KEY : undefined;
      var L1_TRG_RS_KEY   = obj_L1_TRG_RS_KEY   ? obj_L1_TRG_RS_KEY.L1_TRG_RS_KEY : undefined;

      var TRG_STATE = obj_TRG_STATE ? obj_TRG_STATE.TRG_STATE : undefined;
      var STARTTIME = obj_STARTTIME ? obj_STARTTIME.STARTTIME : undefined;
      var INCLUDEDSUBS = obj_INCLUDEDSUBS ? obj_INCLUDEDSUBS.INCLUDEDSUBS : undefined;
      var BEAM1_STABLE = obj_BEAMSTABLE ? obj_BEAMSTABLE.BEAM1_STABLE == '1' : undefined;
      var BEAM2_STABLE = obj_BEAMSTABLE ? obj_BEAMSTABLE.BEAM2_STABLE == '1' : undefined;
      var TRG_RUN_INFO = (obj_TRG_RUN && TRG_STATE.toLowerCase() == "running") ? obj_TRG_RUN : undefined;

      console.log ( TRG_RUN_INFO );
      /// subsystems
      var arrINCLUDEDSUBS = INCLUDEDSUBS.split(" ");
      arrINCLUDEDSUBS.push("CENTRAL");
      if (arrINCLUDEDSUBS[0] != "AUTO:" && arrINCLUDEDSUBS[0] != "EXPERT:") {
        eventEmitter.emit('warning', `Trigger is configured in undefined mode (${arrINCLUDEDSUBS[0]})`);
        // term.error().bgYellow.white(`Trigger is configured in undefined mode (${arrINCLUDEDSUBS[0]})`).defaultColor().bgDefaultColor(`\n`);
      }
      var includedSubsystems = [];
      for (var i = 1; i < arrINCLUDEDSUBS.length; i++) { // i=1, skip first item
        var subsystemName = arrINCLUDEDSUBS[i];
        if (subsystemName != "") {
          includedSubsystems.push(subsystemName);
        }
      }
      // console.log("dt status", obj_DT_STATUS);
      if ( obj_DT_STATUS && obj_DT_STATUS.STRING_VALUE === 'In' ) {
        includedSubsystems.push("DT");
      }
      if ( obj_ECAL_STATUS && obj_ECAL_STATUS.STRING_VALUE === 'In' ) {
        includedSubsystems.push("ECAL");
      }
      if ( obj_HCAL_STATUS && obj_HCAL_STATUS.STRING_VALUE === 'In' ) {
        includedSubsystems.push("HCAL");
      }
      if ( obj_RPC_STATUS && obj_RPC_STATUS.STRING_VALUE === 'In') {
        includedSubsystems.push("RPC");
      }
      if ( obj_CSC_STATUS && obj_CSC_STATUS.STRING_VALUE === 'In') {
        includedSubsystems.push("CSC");
      }

      var partialInfo = {
        runNumber:          runNumber,
        triggerState:       TRG_STATE,
        startTime:          STARTTIME,
        HLTModeId:          L1_HLT_MODE_ID,
        includedSubsystems: includedSubsystems,
        LHCBeams: {
            beam1Stable: BEAM1_STABLE,
            beam2Stable: BEAM2_STABLE
          },
        PRESCALE_INDEX: TRG_RUN_INFO ? TRG_RUN_INFO.PRESCALE_INDEX : undefined,
        PRESCALE_NAME:  TRG_RUN_INFO ? TRG_RUN_INFO.PRESCALE_NAME : undefined,
        LUMI_SECTION:   TRG_RUN_INFO ? TRG_RUN_INFO.LUMI_SECTION : undefined,
        LUMI_SECTION_ID: TRG_RUN_INFO ? TRG_RUN_INFO.ID : undefined
      };

      return Promise.all(
                  [ Promise.resolve( partialInfo ),
                    confQuery( L1_HLT_MODE_ID, L1_TRG_CONF_KEY, L1_TRG_RS_KEY),
                    L1_HLT_MODE_ID ?
                    connection.execute(
                      `select HLT_KEY from CMS_L1_HLT.V_L1_UGT_CONF where ID=:l1_hlt_mode_id`,
                      {l1_hlt_mode_id: L1_HLT_MODE_ID}
                    ) : Promise.resolve( {rows: []} ),
                    TRG_RUN_INFO ?
                      connection.execute(
                        `select ALGO_RATE from CMS_UGT_MON.ALGO_SCALERS
                          where SCALER_TYPE='7' and LUMI_SECTIONS_ID=:lumi_id`,//`
                        { lumi_id : TRG_RUN_INFO.ID } )
                      : Promise.resolve( {rows: []} )
                  ]
                );
     }
  ).then( ([
            partialInfo,
            {rows: [obj_KEYS]},
            {rows: [obj_HLT_KEY]},
            {rows: [obj_MONI]}
           ]) => {

    var L1_HLT_MODE_ID = partialInfo.L1_HLT_MODE_ID;
    var trgConf = {};
             
    if ( obj_KEYS ) {
      var trgSubsystems = {
        "conf" : {
          UGT     : obj_KEYS.UGT_KEY,
          UGMT    : obj_KEYS.UGMT_KEY,
          CALOL1  : obj_KEYS.CALOL1_KEY,
          CALOL2  : obj_KEYS.CALOL2_KEY,
          BMTF    : obj_KEYS.BMTF_KEY,
          EMTF    : obj_KEYS.EMTF_KEY,
          OMTF    : obj_KEYS.OMTF_KEY,
          TWINMUX : obj_KEYS.TWINMUX_KEY,
          CPPF    : obj_KEYS.CPPF_KEY
        },
        "rs" : {
          UGT     : obj_KEYS.UGT_RS_KEY,
          UGMT    : obj_KEYS.UGMT_RS_KEY,
          CALOL1  : obj_KEYS.CALOL1_RS_KEY,
          CALOL2  : obj_KEYS.CALOL2_RS_KEY,
          BMTF    : obj_KEYS.BMTF_RS_KEY,
          EMTF    : obj_KEYS.EMTF_RS_KEY,
          OMTF    : obj_KEYS.OMTF_RS_KEY,
          TWINMUX : obj_KEYS.TWINMUX_RS_KEY,
          CPPF    : obj_KEYS.CPPF_RS_KEY
        }
      };

      trgConf = {
        L1_HLT_MODE:     obj_KEYS.L1_HLT_MODE,
        L1_HLT_VERSION:  obj_KEYS.VERSION,
        L1_TRG_CONF_KEY: obj_KEYS.L1_TRG_CONF_KEY,
        L1_TRG_RS_KEY:   obj_KEYS.L1_TRG_RS_KEY,
        L1_MENU:         obj_KEYS.L1_MENU,
        HLT_KEY:         obj_HLT_KEY.HLT_KEY,
        TRG_SUBSYSTEMS:  trgSubsystems
      };

      L1_HLT_MODE_ID = L1_HLT_MODE_ID ? L1_HLT_MODE_ID : obj_KEYS.ID;
    }

    var newData = {
        runNumber:          partialInfo.runNumber,
        triggerState:       partialInfo.triggerState,
        startTime:          partialInfo.startTime,
        HLTModeId:          L1_HLT_MODE_ID,
        includedSubsystems: partialInfo.includedSubsystems,
        LHCBeams:           partialInfo.LHCBeams,
        trgConf : trgConf,
        trgMoni : {
          PRESCALE_INDEX:  partialInfo.PRESCALE_INDEX,
          PRESCALE_NAME:  partialInfo.PRESCALE_NAME,
          LUMI_SECTION:    partialInfo.LUMI_SECTION,
          LUMI_SECTION_ID: partialInfo.LUMI_SECTION_ID,
          TOTAL_UGT_RATE: {
            ALGO_RATE:      obj_MONI ? obj_MONI.ALGO_RATE : undefined
          }
        }
      };

      console.debug( "Emit Data:", newData );
      eventEmitter.emit("new data", newData);
      setTimeout(updateDB, POLLING_INTERVAL); // restart polling

    }
  ).catch( error => {
    // term.error().bgRed.white("Error while fetching trigger state from db ").defaultColor().bgDefaultColor(` ${error.message}\n`);
    // console.error(error);
    eventEmitter.emit('error', error);
    setTimeout(updateDB, 2*POLLING_INTERVAL); // restart polling, but hold off a bit (DB might be overloaded)
  })


}

updateDB();
