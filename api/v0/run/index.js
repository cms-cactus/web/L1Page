"use strict";

const restify = require('restify');
const io = require.main.require('./_websocket.js');
const poller = require('./_db_poller.js');
const subsystemconf = require.main.require('../settings/subsystems_config.js');
var root;

module.exports = (server) => {
  root = server.getAPIRoot(__dirname);

  server.get(`${root}/`, (request, response, next) => {
    response.send(runData);
    return next();
  });
}

var runData = module.exports.runData = {};

var _lastError = null;
poller.on('error', error => {
  console.error("Error while fetching run data from db", error.message);
  if (error.message != _lastError) {
    _lastError = error.message;
    console.error("full error: ", error);
  }
});
var _lastWarning = null;
poller.on('warning', warning => {
  if (_lastWarning != warning) {
    _lastWarning = warning;
    console.warn(warning);
  }
});
poller.on('new data', newData => {
  _lastError = null;

  // check for changes, trigger update notification if needed
  delete runData._links;
  var somethingChanged = JSON.stringify(runData) != JSON.stringify(newData);
  runData = newData;
  // make the exported data point to the current data again
  module.exports.runData = runData;
  if (somethingChanged) {
    io.notifyUpdatedResource(`${root}/`);
  }
});
