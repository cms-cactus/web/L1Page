"use strict";

const restify = require('restify');
const db = require.main.require('./_database.js');
const io = require.main.require('./_websocket.js');
const jwt = require('../_jwt.js');
const {egroup_admins} = require.main.require('../config/oauth.js');

module.exports = (server) => {
  var root = server.getAPIRoot(__dirname);

  server.get(root, (request, response) => {
    db.getConnection().execute("select id, timestamp, days_valid, author, type, section, message from CMS_TRG_L1_CONF.L1PAGE_SHIFTER_INFORMATION")
    .then( table => response.send(table))
    .catch( error => response.send(500, error))
  });

  server.post(root,
    jwt.authenticate,
    (request, response) => {
      var author = request.user.name;
      var {days_valid, type, section, message} = request.params;

      if (!type || !section || !message) {
        response.send(400, "not enough input parameters");
      }
      else {
        db.getConnection().execute(`INSERT INTO CMS_TRG_L1_CONF.L1PAGE_SHIFTER_INFORMATION (author, type, section, message, days_valid) VALUES (:author, :type, :section, :message, :days_valid)`, {
          author: author,
          type: type,
          section: section,
          message: message,
          days_valid: days_valid || 0
        }, {
          autoCommit: true
        })
        .then( result => {
          io.notifyUpdatedResource(root);
          response.send(result);
        })
        .catch( error => response.send(500, {
          code: 500,
          message: error.message || error
        }))

      }
    }
  )

  server.put(`${root}/:id`,
    jwt.authenticate,
    (request, response) => {
      var id = request.params.id;
      var connection = db.getConnection();
      var author = request.user.name;
      var {days_valid, type, section, message} = request.params;

      if (!type || !section || !message) {
        response.send(400, "not enough input parameters");
      }
      else {
        db.getConnection().execute(`UPDATE CMS_TRG_L1_CONF.L1PAGE_SHIFTER_INFORMATION SET author = :author, type = :type, section = :section, message = :message, days_valid = :days_valid WHERE id = :id`, {
          author: author,
          type: type,
          section: section,
          message: message,
          days_valid: days_valid || 0,
          id: id
        }, {
          autoCommit: true
        })
        .then( result => {
          io.notifyUpdatedResource(root);
          response.send(result);
        })
        .catch( error => response.send(500, {
          code: 500,
          message: error.message || error
        }))

      }
    }
  )

  server.del(`${root}/:id`,
    jwt.authenticate,
    (request, response) => {
      var id = request.params.id;
      var connection = db.getConnection();

      connection.execute("select author from CMS_TRG_L1_CONF.L1PAGE_SHIFTER_INFORMATION where id = :id", {id: id})
      // check if a record, and only one record, is returned
      .then( result => {
        if (result.rows.length == 0) {
          throw {
            code: 500,
            message: `a record with id ${id} does not exist`
          }
        }
        else if (result.rows.length != 1) {
          throw {
            code: 500,
            message: `fetching id ${id} returned multiple records`
          }
        }
        else {
          return result.rows[0];
        }
      })
      // check if the user is authorized
      .then( row => {
        // only the author and some e-group members can delete the entry
        var isAdmin = request.user["e-groups"].some( user_egroup => egroup_admins.indexOf(user_egroup) != -1 );
        var isAuthor = request.user.name == row.AUTHOR;
        if (!isAdmin && !isAuthor) {
          throw {
            code: 403,
            message: `only ${row.AUTHOR} and administrators are authorized to remove this entry`
          }
        }
      })
      // delete the entry
      .then( () => connection.execute(`DELETE FROM CMS_TRG_L1_CONF.L1PAGE_SHIFTER_INFORMATION WHERE id = :id`, {
        id: id
      }, {
        autoCommit: true
      }) )
      .then( result => {
        response.send(result);
        io.notifyUpdatedResource(`${root}/${id}`);
      })
      .catch( error => {
        response.send(error.code || 500, {
          code: error.code || 500,
          message: error.message || error
        })
      })
    }
  )

}
