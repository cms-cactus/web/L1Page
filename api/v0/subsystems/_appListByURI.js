module.exports = subsystems => {
  var result = {};
  for (var subsystemName in subsystems) {
    var subsystem = subsystems[subsystemName];
    for (var appName in subsystem.apps) {
      var app = subsystem.apps[appName];
      result[app.uri] = {
        subsystem: subsystemName,
        app: appName
      }
    }
  }
  return result;
}
