"use strict";

const {url: FLASHLIST_URI, polling_interval: POLLING_INTERVAL} = require.main.require("../settings/ts_flashlist.js");

const http = require('http');
const url = require('url');
const EventEmitter = require('events');

const eventEmitter = new EventEmitter();

const _httpGetOptions = url.parse(FLASHLIST_URI);
var timeout;
var request;
function makeRequest() {
  request = http.request(_httpGetOptions, _callback);
  request.on('error', _onError);
  request.setTimeout(POLLING_INTERVAL, () => {
    console.error("request timed out");
    request.abort();
  });
  request.end();
}
function prepareNewRequest() {
  timeout = setTimeout( makeRequest, POLLING_INTERVAL);
}

const _callback = response => {
  // process response
  var result = "";
  response.on('data', chunk => result += chunk);
  response.on('end', () => {
    try {
      result = JSON.parse(result);
      eventEmitter.emit("new data", result);
    } catch (e) {
      eventEmitter.emit("error", e);
    } finally {
      prepareNewRequest();
    }
  });
}
const _onError = error => {
  eventEmitter.emit("error", error);
  prepareNewRequest();
}

makeRequest(); // start polling

module.exports = eventEmitter;
