"use strict";

const {types:check_types, check_timeout} = require.main.require("../settings/health_checks.js");
const subsystems = require.main.require('../settings/subsystems_config.js').subsystems;
const http = require('http');
const https = require('https');
const url = require('url');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = {


  [check_types.httpGetIsOk]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {
    var app = subsystems[subsystemName].apps[appName];
    // console.debug("health check for", subsystemName+"::"+appName, "type is httpGetIsOk");
    var app_url = url.parse(app.uri);
    var engine = app_url.protocol == "https:" ? https : http;
    var request = engine.request(app_url, response => {
      // console.debug(subsystemName+"::"+appName, "is healthy")
      resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: true,
        //message: "system responds to HTTP GET"
        message: "system responds to HTTP request"
      });
    });
    request.on('error', error => {
      var message = error.message ? error.message : error;
      // console.warn(subsystemName+"::"+appName, "is unhealthy", message);
      resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: false,
        message: message
      });
    });
    request.setTimeout(check_timeout, () => {
      console.warn("health check for", subsystemName+"::"+appName, "timed out");
      resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: false,
        //message: "HTTP GET timed out"
        message: "HTTP request timed out"
      });
    });
    request.end();
  }),


  [check_types.httpGetIsValidTStore]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {
    var app = subsystems[subsystemName].apps[appName];
    var request = http.request(url.parse(app.uri), response => {
      var result = "";
      response.on('data', chunk => result += chunk);
      response.on('end', () => {
        if (result.match(/<td.*>bad<\/td>/)) {
          resolve({
            subsystem: subsystemName,
            app: appName,
            healthy: false,
            message: `bad response from TStore ${subsystemName}.${appName}`
          });
        } else {
          resolve({
            subsystem: subsystemName,
            app: appName,
            healthy: true,
            message: "system gives valid TStore response"
          });
        }
      });
      response.on('error', error => resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: false,
        message: `error while reading health data of ${subsystemName}.${appName}: ${error}`
      }));
    });
    request.on('error', error => {
      var message = error.message ? error.message : error;
      resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: false,
        message: message
      });
    });
    request.setTimeout(check_timeout, () => {
      resolve({
        subsystem: subsystemName,
        app: appName,
        healthy: false,
        //message: "HTTP GET timed out"
        message: "HTTP request timed out"
      });
    });
    request.end();
  }),


  [check_types.ts_flashlist]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {
    resolve({
      subsystem: subsystemName,
      app: appName,
      healthy: false,
      message: "ts_flashlist is unimplemented"
    });
  }),

  [check_types.dummy_check_false]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {
    // console.log('Now in dummy_check_false');
    // throw({message: 'This is an alwyas false check'});
    resolve({
      subsystem: subsystemName,
      app: appName,
      healthy: false,
      message: "This is an always false health check"
    });
  }),

  [check_types.dummy_check_true]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {
    // console.log('Now in dummy_check_true');
    resolve({
      subsystem: subsystemName,
      app: appName,
      healthy: true,
      message: "This is an always true health check"
    });
  }),

  [check_types.dummy_random_true]: ({subsystem: subsystemName, app: appName}) => new Promise( resolve => {

    resolve({
      subsystem: subsystemName,
      app: appName,
      healthy: Math.random() > 0.94, // this should give a 50 % chance of 12 consecutive fails (about a minute of polling)
      message: "This is a random true health check"
    });
  })

}


// check config, every type must have a function
for (var checkName in check_types) {
  if (!module.exports[check_types[checkName]]) {
    console.error(`Missing health check code for type ${checkName}`);
  }
}
