"use strict";

const EventEmitter = require('events');
const subsystemconf = require.main.require('../settings/subsystems_config.js');
const {
  types: CHECK_TYPES,
  polling_interval: POLLING_INTERVAL
} = require.main.require("../settings/health_checks.js");
const check_functions = require("./_health_check_functions.js");


var checklist = [];
for (var subsystemName in subsystemconf.subsystems) {
  let subsystem = subsystemconf.subsystems[subsystemName];
  for (var appName in subsystem.apps) {
    let app = subsystem.apps[appName];
    checklist.push({
      subsystem: subsystemName,
      app: appName,
      checkType: app.telemetry.healthCheck
    });
  }
}


const eventEmitter = new EventEmitter();



function doHealthCheck() {

  var chain = Promise.resolve();
  var results = [];
  for (var i = 0; i < checklist.length; i++) {
    let check = checklist[i];
    chain = chain.then( result => {
      if (result) {
        results.push(result);
      }
      return check_functions[check.checkType](check);
    });
  }


  chain
  .then( result => {        // add last result
    results.push(result);
    return results;
  })
  .then( results => {
    eventEmitter.emit("new data", results);
  })
  .catch( error => {
    eventEmitter.emit("error", error);
  })
  .then( () => {
    setTimeout( doHealthCheck, POLLING_INTERVAL);
  })
}

doHealthCheck();



module.exports = eventEmitter;
