"use strict";

const os = require('os');
const restify = require('restify');
const poller = require('./_flashlist_poller.js');
const healthChecker = require('./_health_checker.js');
const healthCheckSettings = require.main.require("../settings/health_checks.js");
const healthChecks = healthCheckSettings.types;
const subsystemconf = require.main.require('../settings/subsystems_config.js');
const io = require.main.require('./_websocket.js');
const userid = require('userid');
const fork = require('child_process').fork;
var root;
const appListByURI = require('./_appListByURI.js')(subsystemconf.subsystems);
const moment = require('moment');
const mailer = require.main.require('./mailer.js');
// for runData
const run = require.main.require('./v0/run');

const notificationRecipients = require.main.require('../config/notifications.js').crash_notify_recipients;

checkHealthCheckSettings(healthCheckSettings, notificationRecipients);

module.exports = (server) => {
  root = server.getAPIRoot(__dirname);

  server.get(`${root}/`, (request, response) => {
    if (!subsystemsData) {
      response.send(503, `The server did not receive any subsystem data yet`);
    } else {
      response.send({
        subsystems: subsystemsData
      });
    }
  });

  server.get(`${root}/:subsystemName`, (request, response) => {
    if (!subsystemsData) {
      response.send(503, `The server did not receive any subsystem data yet`);
    } else {
      var subsystemName = request.params.subsystemName;
      var subsystem = subsystemsData[subsystemName];
      if (subsystem) {
        response.send({
          [subsystemName]: subsystem
        });
      } else {
        response.send(404, {
          code: 404,
          message: `The subsystem ${request.params.subsystemName} does not exist or its data has not been fetched yet`
        });
      }
    }

  });

  server.post(`${root}/:subsystemName/control`, (request, response) => {
    if (!subsystemsData) {
      response.send(503, `The server did not receive any subsystem data yet`);
    } else {
      var subsystemName = request.params.subsystemName;
      var subsystem = subsystemsData[subsystemName];
      if (!subsystem) {
        response.send(404, {
          code: 404,
          message: `The subsystem ${request.params.subsystemName} does not exist or its data has not been fetched yet`
        });
      }
      else {
        var action = request.params.action;
        var actions = subsystem.actions;
        if (!actions) {
          response.send(400, {
            code: 400,
            message: "This subsystem does not have any available actions"
          });
        }
        else if (!action) {
          response.send(400, {
            code: 400,
            message: "An action must be provided. Valid actions are " + subsystem.actions.map( action => `'${action}'`).join(", ") + "."
          });
        }
        else if (actions.indexOf(action) == -1) {
          response.send(400, {
            code: 400,
            message: `The action '${action}' is not available. Valid actions are ` + subsystem.actions.map( action => `'${action}'`).join(", ") + "."
          });
        }
        else {
          console.log(`Performing action ${action} on subsystem ${subsystemName}`);
          var controller = subsystemconf.subsystems[subsystemName].controller;
          var command = controller.getcommand(action);
          var hosts = typeof controller.host == "string" ? [controller.host] : controller.host;
          var jobs = hosts.map( hostName => new Promise( (done, fail) => {
            hostName = hostName.replace("https://", "").replace("http://", "");
            var uid;
            try {
              uid = userid.uid(controller.username);
            } catch (e) {
              return fail(`${hostName} must be accessed as user ${controller.username}, but this user is not configured on this machine (ssh keys missing)`);
            }
            var child = fork('./processControlRunner', [], {
              uid: uid
            });
            child.on('message', function(result) {
              var f = result.code == 0 ? done : fail;
              f(result.message);
            });
            child.on('error', error => {
              fail(error);
            });
            child.send({
              host: hostName,
              command: command
            });
          }));
          Promise.all(jobs)
          .then( results => {
            response.send(results);
            notifyAction(subsystemName, action, results);
          })
          .catch( error => {
            response.send(500, {
              code: 500,
              message: "process control failed: " + error
            });
            notifyAction(subsystemName, action, "process control failed:\n\n" + error);
          });
        }
      }
    }
  })
}

function notifyAction(subsysName, action, output) {
  // only notifiy for "state changing" actions
  if (action === 'status') return;
  let message = `${subsysName}:\naction \'${action}\' requested from L1Page\n`;

  message += '\noutput:\n------------------------------------------------------------\n';
  message += output + '\n------------------------------------------------------------\n';

  message += compileContextInfo(run.runData);

  if (notificationRecipients.length) {
    // only send a notification email when recipients are set
    mailer.sendMail(message, notificationRecipients, 'L1Page subsystem action notification');
  }
}

/**
 * Collect the some context information that will be added to notification emails
 */
function compileContextInfo(runData) {
  let message = '\nsystem info:\n------------------------------------------------------------\n';
  message += 'L1 status: ' + `${runData.triggerState}\n`;
  message += 'included subsystems: ' + runData.includedSubsystems.join(', ') + '\n';
  message += 'run number / lumi section: ' + runData.runNumber + ' / ' + runData.trgMoni.LUMI_SECTION + '\n';
  message += 'current key: ' + runData.trgConf.L1_HLT_MODE + '/' + runData.trgConf.L1_HLT_VERSION + '\n';
  message += '------------------------------------------------------------\n';

  let now = moment(/*now*/);
  message += now.format('ddd MMM D YYYY HH:mm:ss (Z)') + ` on ${process.env.HOSTNAME}\n`;

  return message;
}

// populate skeleton subsystemsData according to subsystem config
// while making sure that the exported subsystemsData is always in sync with the internally used one
// As long as there is no assignment to the subsystemsData variable after the first one, this will not break
module.exports.subsystemsData = {};
var subsystemsData = module.exports.subsystemsData;
for (var subsystemName in subsystemconf.subsystems) {
  let subsystem = subsystemsData[subsystemName] = {apps: {}};
  let conf = subsystemconf.subsystems[subsystemName];
  for (var appName in conf.apps) {
    var app = conf.apps[appName];
    subsystem.apps[appName] = {
      uri: app.uri,
      critical: app.telemetry.critical,
      label: app.label,
      // filled by flashlist
      operations: [],
      alarms: [],
      lastUpdate: new Date(), // now. to not have to make handling at startup different
      // filled by health checks
      healthy: false,
      lastHealthy: new Date() // now. to not have to make handling at startup different
    };
  }
  let controller = conf.controller;
  subsystem.actions = [];
  if (controller) {
    subsystem.actions = Array.isArray(controller.actions) ? controller.actions : Object.keys(controller.actions);
  }
}


var _lastPollError = null;
poller.on('error', error => {
  console.error("Error while fetching flashlist", error.message);
  if (error.message != _lastPollError) {
    _lastPollError = error.message;
    console.error("full error: ", error);
  }
});
var _lastHealthError = null;
healthChecker.on('error', error => {
  console.error("Error while checking subsystem health", error.message);
  if (error.message != _lastHealthError) {
    _lastHealthError = error.message;
    console.error("full error: ", error);
  }
});
healthChecker.on('new data', healthData => {
  _lastHealthError = null;

  var changedSubsystems = {}; // to keep track of changes
  for (var i = 0; i < healthData.length; i++) {
    var result = healthData[i];
    var subsystemName = result.subsystem;
    if (subsystemName == "CENTRAL_UPGRADE") {subsystemName = "CENTRAL"}
    if (!subsystemsData[subsystemName]) {subsystemsData[subsystemName] = {apps: {}};}
    if (!subsystemsData[subsystemName].apps[result.app]) {subsystemsData[subsystemName].apps[result.app] = {};}

    if (!subsystemsData[subsystemName] || !subsystemsData[subsystemName].apps[result.app]) {
      // this should be impossible to happen, since only apps defined in the configuration
      // have health checks
      console.error('health check got a result from app ', result.app, ' in subsystem ', subsystemName, ' which is not defined in the configuration');
    }

    let app = subsystemsData[subsystemName].apps[result.app];
    if (app.healthy != result.healthy) {
      changedSubsystems[subsystemName] = true;
    }
    app.healthy = result.healthy;
    app.healthMessage = result.message;
    if (app.healthy) {
      app.lastHealthy = new Date();
    }
  }

  // all flashlist data is processed now, notify any changes
  for (var subsystemName in changedSubsystems) {
    io.notifyUpdatedResource(`${root}/${subsystemName}/`);
  }
});
poller.on('new data', response => {
  _lastPollError = null;

  // new flashlist received

  // 2 error cases
  // 1) subsystem app listed in config, but not in flashlist (and config says it should)
  //   > generate a warning
  // 2) subsystem app not listed in config, but appears in flashlist
  //   > this is ignored, config is source of truth

  var apiInfo = {
    apps: {
      "API": {
        uri: `https://${os.hostname()}`,
        alarms: [],
        cpu: (() => {
          var cpu = os.cpus()[0];
          return 1-(cpu.idle/(cpu.idle+cpu.user+cpu.nice+cpu.sys+cpu.irq));
        })(),
        timestamp: new Date().toJSON(),
        memory: os.freemem()/os.totalmem()
      }
    }
  }

  var _URIsInFlashlist = {}; // to check for error case 1

  var changedSubsystems = {}; // to keep track of changes
  for (var i = 0; i < response.table.rows.length; i++) {
    var app = response.table.rows[i];
    // build full URI
    if (app.urn[0] != "/") {
      app.urn = `/${app.urn}`;
    }

    // for each app, get the corresponding app in the already existing information and uppdate
    // that if necessary.
    let appURI = app.context + app.urn;
    if (appListByURI[appURI]) {
      _URIsInFlashlist[appURI] = true;
    } else {
      console.log('Got flashlist information from uri=', uri, ' which is an app not listed in the configuration');
      continue; // since config is source of truth, don't do any additional work here
    }

    let newApp = {
      uri: appURI,
      cpu: app.CPU,
      timestamp: app.timestamp,
      operations: app.Operations.rows,
      alarms: app.Alarms.rows,
      lastUpdate: new Date()
    };

    let {subsystem: subsystemName, app: appName} = appListByURI[appURI];

    // check timestamp of the flashlist data and if it is not recent, add a warning to the alarms
    if (isOlderThan(app.timestamp, healthCheckSettings.flashlist_stale_warning)) {
    let appTimestamp = moment(app.timestamp);
      console.warn(`Flashlist information for ${subsystemName} - ${app.name} is more then 5 minutes old (${appTimestamp})`);
      newApp.alarms.push({
        severity: 'WARNING',
        details: `Flashlist information has last been updated ${appTimestamp.fromNow()} (${appTimestamp})`,
        name: 'flashlist updater'
      });
    }

    // since we are asking for an app that is in the configuration, this check should never fail
    if (!subsystemsData[subsystemName] ||
        !subsystemsData[subsystemName].apps[appName]) {
      console.error('App ', appName, ' in subsystems ', subsystemName, ' not already present from configuration although it should');
    }

    // small helper function to make comparing app data easier
    let getAppData = function( app ) {
      let appData = {};
      for (let prop of ['cpu', 'uri', 'timestamp', 'operations', 'alarms']) {
        appData[prop] = app[prop];
      }
      return appData;
    };

    let oldApp = subsystemsData[subsystemName].apps[appName];
    // let newData = getAppData(newApp); // NOTE: currently redundant since all fields are used
    // check if something has changed
    if (JSON.stringify(getAppData(newApp)) != JSON.stringify(getAppData(oldApp))) {
      changedSubsystems[subsystemName] = true;
      // update app data, but only with the information from the flashlist
      Object.assign(oldApp, newApp);
    }
  }


  // check for error case 1)
  for (var uri in appListByURI) {
    var {subsystem: subsystemName, app: appName} = appListByURI[uri];

    // this should never fail since the appListByURI is built from the configuration
    let app = subsystemsData[subsystemName].apps[appName];

    // if it was not in the flashlist AND the subsystem app config says it should
    var shoudBeInFlashlist = subsystemconf.subsystems[subsystemName].apps[appName].telemetry.dataSource == healthChecks.ts_flashlist;
    // if (!_URIsInFlashlist[uri] && shoudBeInFlashlist) {
    if (!_URIsInFlashlist[uri] && shoudBeInFlashlist) {
      console.warn(`app ${appListByURI[uri].subsystem} - ${appListByURI[uri].app} has no record in the flashlist, but config says it should. (add 'telemetry.dataSource: null' to config if this is expected)`);
      apiInfo.apps["API"].alarms.push({
        details: `app ${appListByURI[uri].subsystem} - ${appListByURI[uri].app} has no record in the flashlist, but config says it should`,
        name: "flashlist validator",
        severity: "WARNING"
      });
      // don't spam the warning list with this but update the warning message
      let flashlistAlarm = app.alarms.find(alarm => alarm.name === 'flashlist validator');
      if (!flashlistAlarm) {
        app.alarms.push({
          details: `app has had no record in the flashlist update for ${moment(app.lastUpdate).fromNow()} now, but config says it should`,
          name: 'flashlist validator',
          severity: 'WARNING'
        });
      } else {
        flashlistAlarm.details = `app has had no record in the flashlist update for ${moment(app.lastUpdate).toNow(true)} now, but config says it should`;
      }
    }
  }

  if (JSON.stringify(subsystemsData["L1Page"].apps) != JSON.stringify(apiInfo.apps)) {
    Object.keys(apiInfo.apps).map( appName => {
      if(!subsystemsData['L1Page'].apps[appName]) {
        console.error(`Trying to update ${appName} of L1Page, which is not in configuration. Skipping this`);
        return;
      }
      Object.assign(subsystemsData['L1Page'].apps[appName], apiInfo.apps[appName]);
    });
    changedSubsystems["L1Page"] = true;
  }

  // all flashlist data is processed now, notify any changes
  for (var subsystemName in changedSubsystems) {
    io.notifyUpdatedResource(`${root}/${subsystemName}/`);
  }
});


// function dumpSubsystemData(subsystemsData) {
//   for (let subsystemName of Object.keys(subsystemsData)) {
//     console.log(`========== subsystem: ${subsystemName} ==========`);
//     console.log(subsystemsData[subsystemName].apps);
//     console.log('--------------------------------------------------');
//   }
// }

// make this a function to be able to easily reset it
function createNotificationTracker() {
  // keep track of notifications in this global object
  var notificationTrackerByURI = {};
  // fill with the appListByURI
  for (let uri in appListByURI) {
    notificationTrackerByURI[uri] = {
      notifiedAt: 0,
      notificationMessage: ''
    };
  }
  return notificationTrackerByURI;
}

var notificationTrackerByURI = createNotificationTracker();
/**
 * Check if all subsystems are OK (i.e. all apps of the subsystems respond and don't have any errors from the flashlist)
 * If an app has an error notify the corresponding people
 */
function checkNotify(subsysData, inclSubsystems) {
  if (!inclSubsystems) {
    console.warn('runData did not contain \'includedSubsystems\'. Skipping the notification checks');
    return;
  }
  for (let uri in appListByURI) {
    let {subsystem: subsystemName, app: appName} = appListByURI[uri];
    // Check if the subsystem is included in the run and don't notify if it is not
    // NOTE: If you want to see the test systems at work disable the check
    if (!inclSubsystems || !inclSubsystems.includes(subsystemName)) continue;

    let app = subsysData[subsystemName].apps[appName];

    let unhealthy = isOlderThan(app.lastHealthy, healthCheckSettings.notification_time);
    // not all apps expect updates from the flashlist
    let expectFLUpdate = subsystemconf.subsystems[subsystemName].apps[appName].telemetry.dataSource == healthChecks.ts_flashlist;
    let noFLUpdates = expectFLUpdate && isOlderThan(app.lastUpdate, healthCheckSettings.notification_time);

    var notificationTracker = notificationTrackerByURI[uri];

    // reset the tracker for this given app to detect future failures
    if(!unhealthy && !noFLUpdates) {
      notificationTracker.notificationMessage = '';
      notificationTracker.notifiedAt = null;
    }

    if (unhealthy || noFLUpdates) {
      var message = `app ${appName} - ${subsystemName}:\n`;
      let {time, unit} = healthCheckSettings.notification_time;
      if (unhealthy) {
        message += `did not respond to an HTTP request for at least ${time} ${unit} (last healthy: ${app.lastHealthy})\n`;
      }
      if (noFLUpdates) {
        message += `did not get flashlist updates for at least ${time} ${unit} (last update: ${app.lastUpdate})\n`;
      }

      if (!notificationTracker.notifiedAt &&
          notificationTracker.notificationMessage != message) {
        let now = moment(/*now*/);
        notificationTracker.notificationMessage = message;
        notificationTracker.notifiedAt = now;

        // add the timestamp only after adding it to the notification tracker otherwise it is possible the check just detects different time stamps
        message += compileContextInfo(run.runData);

        if (notificationRecipients.length) {
          // don't send anything if there are no recipients specified
          mailer.sendMail(message, notificationRecipients, 'L1Page subsystem/app crash notification');
        }
        console.warn(subsystemName, appName, notificationTracker);
      }
    }
  }
}

var _lastRun = null;
healthChecker.on('new data', healthData => {
  if (!_lastRun || _lastRun != run.runData.runNumber) {
    _lastRun = run.runData.runNumber;
    // reset the notificationTracker
    notificationTrackerByURI = createNotificationTracker();
    console.log('reset notification tracking object');
  }

  let {includedSubsystems} = run.runData;
  checkNotify(subsystemsData, includedSubsystems);
});

/**
 * Check if the time stamp is more recent than the timeOut (w.r.t. now)
 * @param {date or moment} time - a timestamp that can be used to construct a moment object
 * @param {Object} timeOut - an Object with a 'time' field specifying the amount and a 'unit' field specifying the unit of the timeout
 * @returns {Boolean} - true if the passed time is older than the timeOut w.r.t. now
 */
function isOlderThan(time, timeOut) {
  return moment(time).add(timeOut.time, timeOut.unit).isBefore(/*now*/);
}

/**
 * Check if the settings for the health checks are valid
 * Check if the two time settings are present and if they are valid for moment.js
 */
function checkHealthCheckSettings(healthCheckSettings, recipients) {
  function checkTimeOK(timeSetting) {
    if (!timeSetting) return false;
    // moment duration returns 0, if either the time value is 0 or if the unit is not valid
    if (moment.duration(timeSetting.time, timeSetting.unit) == 0) return false;
    return true;
  }

  let bothOK = true;
  if (!checkTimeOK(healthCheckSettings.flashlist_stale_warning)) {
    console.error('\'flashlist_stale_warning\' not set or not set properly. Check settings/health_checks.js');
    bothOK = false;
  }
  if (!checkTimeOK(healthCheckSettings.notification_time)) {
    console.error('\'notification_time\' not set or not set properly. Check settingns/health_checks.js');
    bothOK = false;
  }
  if (!bothOK) {
    throw 'health check settings error';
  }

  if (recipients.length) {
    console.info('Sending notification emails in case of crashes to:', recipients);
  } else {
    console.info('Sending notification emails in case of crashes disabled');
  }
}
