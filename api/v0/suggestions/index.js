"use strict";

const restify = require('restify');
const io = require.main.require('./_websocket.js');
const chokidar = require('chokidar');
const watchdir = require.main.require('./_watchdir.js');
var root;

const run = require('../run');
const subsystems = require('../subsystems');

var suggestions = [];

module.exports = server => {
  root = server.getAPIRoot(__dirname);

  server.get(root, (request, response) => {
    response.send({
      suggestions: suggestions,
      // TODO: fix the wonky retrieval using substrings
      // COULDDO: Remove, since unused in client
      loadedRuleFiles: Object.keys(loadedModules).map(path => path.substring(22))
    })
  });
}

var debouncer;
io.emitter.on('updated resource', path => {
  var match = path.startsWith("/api/v0/subsystems") || path.startsWith("/api/v0/run");
  if (match) {
    clearTimeout(debouncer);
    // debouncer = setTimeout(() => {renderSuggestions(loadedModules);}, 100);
    debouncer = setTimeout(renderSuggestions, 100);
  }
});
// function renderSuggestions(loadedModules) {
function renderSuggestions() {
  suggestions = Object.keys(loadedModules).reduce( (suggestions, modulePath) => {
    try {
      var module_suggestions = loadedModules[modulePath](run.runData, subsystems.subsystemsData);
      if (module_suggestions) {
        return suggestions.concat( module_suggestions );
      }
      else {
        return suggestions;
      }
    } catch (e) {
      console.error(`module ${modulePath} encountered an error`, e);
      return suggestions;
    }
  }, []);
  io.notifyUpdatedResource(root);
}

function updateSuggestions(moduleList) {
  console.log('Updating suggestion modules');
  loadedModules = {};
  for (let module in moduleList) {
    console.log(`Loading suggestions from ${module}`);
    loadedModules[module] = moduleList[module];
  }
}

// setup the watcher so that it ignores all examples and actually finds the modules while working with a relative path
const watcher = chokidar.watch(__dirname + '/../../../settings/suggestions/',
                               {ignored: /examples\/.*\.js/});

watchdir(watcher, updateSuggestions);

var loadedModules = {};
