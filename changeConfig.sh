#!/bin/bash

OLD_HOSTNAME="l1page.cms"
read -p "==> Type the new hostname (default ${OLD_HOSTNAME}): " NEW_HOSTNAME
NEW_HOSTNAME=${NEW_HOSTNAME:-${OLD_HOSTNAME}}

echo "### Redefining path for host key-pair"
OLD_PATH='/nfshome0/centraltspro/secure/keys/'
read -p "==> Type the new PATH (default ${OLD_PATH}) : " NEW_PATH
NEW_PATH=${NEW_PATH:-${OLD_PATH}}

OLD_KEY="l1page"
read -p "==> Type the new keyname (default 'l1page'[.crt|.key]): " NEW_KEY
NEW_KEY=${NEW_KEY:-${OLD_KEY}}

[[ $NEW_PATH != */ ]] && NEW_PATH="$NEW_PATH"/

echo "### Setting the notification destination (for crash reports and other email notifications)"
read -p "==> If you want to enable notifications enter destination email address (default ''): " NEW_NOTIFY_DEST

echo
echo "replacing $OLD_HOSTNAME -> $NEW_HOSTNAME"
echo "replacing $OLD_PATH -> $NEW_PATH"
echo "replacing l1page[.crt|.key] -> $NEW_KEY[.crt|.key]"
echo


mkdir -p config/ config/nginx
cp -f setup/config/oauth.js      config/
cp -f setup/config/cryptokeys.js config/
cp -f setup/nginx/l1page.conf    config/nginx/
cp -f setup/config/notifications.js config/

declare -a FILES=( "config/oauth.js"
    "config/nginx/l1page.conf"
    "config/cryptokeys.js"
    "config/notifications.js")

FILELIST=''
FILEPRINT=''
BADFILES=''
for LFILE in "${FILES[@]}"; do
    test -e ${LFILE} || continue
    touch ${LFILE} 2> /dev/null
    if [ $? -eq 0 ]; then
        FILELIST="${FILELIST} ${LFILE}"
        FILEPRINT="${FILEPRINT}\n   ${LFILE}"
    else
        BADFILES="${BADFILES}\n   ${LFILE}"
    fi
done

sed -i -- "s|${OLD_HOSTNAME}|${NEW_HOSTNAME}|g" ${FILELIST}
sed -i -- "s|${OLD_KEY}.crt|${NEW_KEY}.crt|g" ${FILELIST}
sed -i -- "s|${OLD_KEY}.key|${NEW_KEY}.key|g" ${FILELIST}
sed -i -- "s|${OLD_PATH}|${NEW_PATH}|g" ${FILELIST}

if [ -n "${NEW_NOTIFY_DEST}" ]; then
    echo "crash reports and other notifications will be sent to: "${NEW_NOTIFY_DEST}
    # The following sed command first removes all entries of the list and then puts in the desired recipient
    sed -i "/\[/,/\]/ {//!d}; /\[/a\'${NEW_NOTIFY_DEST}\'" config/notifications.js
fi
if [ -z "${NEW_NOTIFY_DEST-empty}" ]; then
    echo "crash reports and other notifications are disabled"
    # The following sed command removes all entries of the list and so disables the notifications via email
    sed -i "/\[/,/\]/ {//!d}" config/notifications.js
fi


echo "The following files have been changed:"
echo -e "${FILEPRINT}"
echo ""

if [ -n "${BADFILES}" ]; then
    echo "WARNING: the following files cannnot be edited:"
    echo -e "${BADFILES}"
    echo ""
fi



echo "installing..."


cp -fv config/nginx/l1page.conf /etc/nginx/conf.d/l1page.conf
cp -fv config/cryptokeys.js     /opt/cactus/L1Page/config/cryptokeys.js
cp -fv config/oauth.js          /opt/cactus/L1Page/config/oauth.js
cp -fv config/notifications.js  /opt/cactus/L1Page/config/notifications.js
