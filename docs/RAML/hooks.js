var hooks = require('hooks');
var https = require('https');
var url = require('url');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
var JWT_Header;

// abao automatically follows redirects, so it will never see a 3xx response
hooks.skip("GET /login -> 302");
hooks.skip("GET /whoami -> 200");
hooks.skip("POST /shifter-information -> 200");

hooks.skip("GET /run -> 200"); // table doesn't exist in dev db
