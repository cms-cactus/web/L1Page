# How to compile

## directories description

- `api` : contains the server source files
- `config` : contains host configuratiuon files
- `docs` : contains documentation
- `errors` : contains html errore responses
- `htdocs` : contains interface source files
- `settings` : contains configurable modules of the L1Page
- `setup` : contains files for installation and post-installation configuration

## first installation
First, a set of prerequisites must be accomplished: [installation prerequisites](install-prerequisites.md).
Right after, the compilation and installation steps are performed through:

- `make`
- `sudo make install`
- `sudo make post_install`

In order to run a custom instance in a private host, extra configuration steps are needed: [configuration](configuration.md).

##  after editing api section
- `sudo make install_api`

##  after editing interface section (htdocs)
- `make` (aka `make compile`)
- `sudo make install_interface`

## Targets description

### main targets
- `all` aka `compile` actually does the local compile actions in htdocs (grunt)
- `install_interface` installs only the interface files without altering the local setup of the installation
- `install_api` installs only the api files without altering the local set the installation
- `install` installs api, interface and l1page configuration files
- `post_install` installs configuration files, performs pm2 configuration
- `rpm` creates rpms
- `config` creates the template files for host configuration and put them in the `config` directory

### secondary targets

- `install_logrotate`: packs logrotate
- `nodejs`: downloads api components
- `bower_htdocs`: installs bower locally
- `grunt_htdocs`: downloads interface components
- `install_config` installs `tnsnames.ora` and `nginx` config file
