# Configuration

Configuration files are located in the `config` folder.

In the installed version of the L1Page (e.g. installed through rpm or `make install`), this folder is located here: `/opt/cactus/L1Page/config`.

The codebase assumes the hostname of the machine is `l1page.cms`, as well as a default place for the public/private key pair(`/nfshome0/centraltspro/secure/keys/`).

In order to use a custom host, those variables must be customised, together (hint: creating `l1page.crt` and `l1page.key` as symlink to your actual key-payr files to reduce the amount of changes).

It is also set up to send notification emails of subsystem crashes to the `cactus-l1page-crash-reports@cern.ch` e-group. In order to not spam it, please change it to either your own email address or disable notifications completely.

The simplest way is to use the `changeConfig.sh` script as follows located in the project root directory (in the rpm installation: `/opt/cactus/L1Page/`).

Before any editing, be sure `nginx` and `l1page` are not running (issue `make stop` from the root directory).

```bash
sudo ./changeConfig.sh 
==> Type the new hostname (default l1page.cms): l1page-dev.cern.ch
### Redefining path for host key-pair
==> Type the new PATH (default /nfshome0/centraltspro/secure/keys/) : /nfshome0/gcodispo/L1T-APPS/generated-pairs
==> Type the new keyname (default 'l1page'[.crt|.key]): 
### Setting the notification destination (for crash reports and other email notifications)
==> If you want to enable notifications enter destination email address (default ''): l1page.developer@cern.ch

replacing l1page.cms -> l1page-dev.cern.ch
replacing /nfshome0/centraltspro/secure/keys/ -> /nfshome0/gcodispo/L1T-APPS/generated-pairs/
replacing l1page[.crt|.key] -> l1page[.crt|.key]

crash reports and other notifications will be sent to: l1page.developer@cern.ch
The following files have been changed:

   config/oauth.js
   config/nginx/l1page.conf
   config/cryptokeys.js
   config/notifications.js

installing...
‘config/nginx/l1page.conf’ -> ‘/etc/nginx/conf.d/l1page.conf’
‘config/cryptokeys.js’ -> ‘/opt/cactus/L1Page/config/cryptokeys.js’
‘config/oauth.js’ ->  ‘/opt/cactus/L1Page/config/oauth.js’
‘config/notifications.js’ -> ‘/opt/cactus/L1Page/config/notifications.js’
```

This script will replace files in the system directories: if you need sudo rights in order to run.

Do not forget to edit `config/oauth.js` inserting the correct`Secret` as described in the final part of [installation prerequisites](install-prerequisites.md).

To disable notifications completely using the `changeConfig.sh` script simply press enter when prompted for an email address without putting in any text.

### configuring the database
The `config/dbconfig.js` file specifies the databases (development/production) L1Page will connect to and the password files location.

The production copy is located in `/opt/cactus/L1Page/config/dbconfig.js`.
