# Installation prerequisites

This part of the guide is meant to describe prerequisites for the
installation of the L1Page that may non be explicitly covered
elsewhere. They are mostly important if you want to run your own test instance in a custom host.

If you have troubles running the L1Page, please check that all the steps here are covered.

## Dependencies

As first, all the dependencies must be installed in the host where the L1CE will run.
The simplest way is to download the build artofacts from the project [cc7-l1ce-ready](https://gitlab.cern.ch/cms-cactus/web/cc7-l1ce-ready/tags) or to use the `install.sh` script there provided:

```bash
sudo ./install.sh all
```


## Firewall settings

SELinux needs to be disabled. So edit the SELinux config file
`/etc/sysconfig/selinux` and set:


```
SELINUX=permissive
```

then disable it:

```
sudo setenforce 0
```

Finally, the firewall need to be also disabled:

```
sudo service firewalld stop
sudo systemctl disable firewalld
```
## Database
In a custom installation, you need to have a proper tnsnames.ora configuration.
You may copy it from the installation directory:

```
sudo cp /opt/cactus/L1Page/setup/database/tnsnames.ora /etc/
```


## Custom key pair
L1Page uses a public/private key pair to secure the web interface and to encrypt its tokens.

Such a key pair is coupled with the hostname it is deployed on. Therefore, if you changed the hostname, you will have to acquire a new key pair.

#### getting a key pair for the `.cms` domain
The Level-1 Online Software group maintains the key pairs generated for the `*.cms` domain.

The people who can generate a new key pair for you are listed [here](https://gitlab.cern.ch/cactus/cms-ca/project_members)

#### getting a key pair for the `.cern.ch` domain
Request a CERN Grid Host certificate [here](https://ca.cern.ch/ca/host/Request.aspx?template=ee2host).

- Select the recommended option `Automatic certificate generation`.
- Enter `<hostname>.cern.ch` as the Certificate Subject.
- Enter `<hostname>.cern.ch` as the Subject Alternative Names (important for chrome > 58).

You will be able to download a .p12 file.
Convert the .p12 file to .key (private key) and .pem (public key) with the following commands:

```bash
openssl pkcs12 -in myhostname.p12 -out myhostname.crt -clcerts -nokeys
openssl pkcs12 -in myhostname.p12 -out myhostname.key -nocerts -nodes
```


## Registering a new host with CERN SSO

Go to the CERN OAuth management portal [here](https://sso-management.web.cern.ch/OAuth/RegisterOAuthClient.aspx) and register a new application with the following parameters:

- *client_id*: Can be anything, but must be globally unique, ex: `L1Page`; **use preferably myhostname.cern.ch**, ex: `l1page-dev.cern.ch`
- *redirect_uri*: https://myhostname.cern.ch//api/v0/handle_oauth, ex: `https://l1page-dev.cern.ch/api/v0/handle_oauth`
- *Application Homepage*: https://myhostname.cern.ch, ex: `https://l1page-dev.cern.ch`
- *Application description*: any meaningful description, ex: `l1page test instance`

Click on `generate` to generate the 'secret' and finally to `Send Registration Request`.
Finally, edit the following file, inserting the correct`Secret` and `client_id` in the running instance:

```
config/oauth.js
```

Warning: this file is overwritten by the script `chanheConfig.sh`, be sure to put again the correct `Secret` after running the script.
