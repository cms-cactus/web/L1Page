# Installation guide

This guide assumes you want to start from zero (just the source).

Alternatively, you can [install from pre-built RPMs](install-rpm.md).

In both cases, the basic installation is preconfigured for P5 deployment, and will require extra configuration steps.

First, a set of prerequisites must be accomplished: [installation prerequisites](install-prerequisites.md).

## Getting the code

This guide assumes you have `git` installed.

```bash
git clone https://gitlab.cern.ch/cactus/L1Page.git
```

Now you have the latest version of the code. If you want to have a specific (or the latest) release, run

```bash
git tag -l
```
This will give you a list of tags, after which you can get the code of a particular release (e.g. v0.3.0) like this:

```bash
git checkout tags/0.3.0
```


## Installing from source
This project requires at least:

 - a C++11 compatible compiler
 - Python 2.6

This is because the oracledb connector needs to be compiled.

Building can be performed with the make utility:

 - `make` installs web components and "compiles" local code
 - `make install` installs L1Page (requires sudo rights)

After the first installation, one single command completes the configuration of the external dependencies and starts the processes:

```bash
make post_install
```

Optionally you can install pm2-logrotate:

```bash
make install_logrotate
```

In order to run a custom instance in a private host, extra configuration steps are needed: [configuration](configuration.md).

In order to run tests on your installation, use:


```bash
make test
```

You can crate rpms for tests using:

```bash
make rpm
```

Have a look to [compilation guide](compilation-guide.md) for a full description of the make targets.

Reading `.gitlab-ci.yml` will give you a live example of these instructions. They are used to build the test environment and build the RPMs.


## Common mistakes

*  prerequisites not fully accomplished (dependencies missing, SELinux of firewall enabled, ... )
*  host not registered
*  missing keypairs
*  the key pair path and names are no correctly spelled (you will see errors both in the L1Page and nginx log files)
*  the hostname is not properly spelled


## Additional debug hints

The L1Page relies on `nginx`.
In case of authentication problems check if `nginx` is down or there are errors in `/var/log/nginx/error.log`.

The command to start `nginx` is:
```
sudo systemctl start nginx
```

while the stop it:

```
sudo systemctl stop nginx
```

The `pm2` Node.js Process Manager is responsible instead for the start of L1Page process.
The most relevant commands are:

* list processes:
```
sudo pm2 list all
```
* start L1Page:
```
sudo pm2 start l1page-api
```

* stop L1Page:
```
sudo pm2 stop l1page-api
```

* restart L1Page:
```
sudo pm2 reload l1page-api
```



## How to setup the P5 test instace

Download the `artifact.zip` from the tag page and copy it to `cmsusr`.
Then login to the `l1ce-test.cms` pc and unzip the package:

```bash
unzip artifact.zip
```

make sure you have sudo rights, then:

```bash
cd /opt/cactus/L1Page/
sudo make stop
sudo yum remove L1Page
sudo yum localinstall /path/to/l1ce/rpm/L1Page-*.x86_64.rpm
sudo pm2 stop l1page-api
sudo systemctl stop nginx
sudo killall nginx
```

In order to avoid conflicts, double check that all the relevant processes are not running:

```bash
ps aux | grep node
ps aux | grep nginx
```

Set up the local configuration priding the hostname (`l1page-test.cms`) and the path to the keypairs:

```bash
sudo ./changeConfig.sh 
```

Restart the applications:

```bash
sudo make start
```

## How to quickly patch a running instance

### server
In order to test a small change or apply an hotfix, is enough to rsync the content of the `api` folder with you local checkout:

```bash
rsync -e ssh -avSz --exclude api/node_modules ./api root@${DESTINATION-HOST}:/opt/cactus/L1Page
```

### interface

In order to test a small change or apply an hotfix, you need first to locally "compile" (e.g. run grunt, sass, etc.), then copy the compiled files to the server:

```bash
make
rsync -e ssh -avSz --exclude htdocs/node_modules ./htdocs ./api root@${DESTINATION-HOST}:/opt/cactus/L1Page 
```
