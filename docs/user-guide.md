# L1Page User Guide

This guide documents common use cases of L1page and provides some getting-started instructions.


## supported browsers

- firefox >= 51
- chrome/chromium >= 49

## getting started

### security settings
<img src="pitfall.png" height="50">
The most occuring problem experienced by users is failure to follow this section correctly. Please be cautious and don't skip steps.

L1Page is a web-app that is served over HTTPS, meaning that a [chain of trust](https://datacenteroverlords.com/2011/09/25/ssl-who-do-you-trust/) needs to be established.

There are several sources of trust that need to be installed on your system, or
your browser will refuse to connect to L1Page:

 - [CERN Certification Authorities and CERN Grid Certification Authority](https://cafiles.cern.ch/cafiles/)
 - [CERN CMS Level-1 Software Certificate Authority](https://gitlab.cern.ch/cactus/cms-ca)

When these certificate authorities have been installed, you should be able to connect
to L1Page without a security warning.


## common problems

### Connection stuck

- Be sure that all certification autorities are properly installed, with at least the first checkbox selected ("trust this certificate for identifying websites").
- Be sure you are using a supported browser.
- Check network stability.

