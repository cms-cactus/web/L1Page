AuthEnabled = function(superClass) {

  return class extends superClass {
    static get properties() {
      return {
        userInfo: Object,
        isLoggedIn: {
          type: Boolean,
          computed: "_getIsLoggedIn(userInfo)"
        }
      }
    }
    static get egroup_admins() {
      return [
        "cms-l1t-operations",
        "cms-cactus-admins"
      ]
    }
    constructor() {
      super();
      this._onLocalStorageChange = function onLocalStorageChange() {
        if (!localStorage["CERN Session"]) {
          this.set('userInfo', null);
        }
        else {
          var tokenData = jwt_decode(localStorage["CERN Session"]);
          var ms_remaining = new Date(tokenData.exp*1000) - new Date();
          if (ms_remaining < 0) {
            console.log("token expired");
            this.set('userInfo', null);
          }
          else {
            this.set('userInfo', tokenData);
            setTimeout(function () {
              this.set('userInfo', null);
            }.bind(this), ms_remaining);
          }
        }
      }.bind(this);
    }

    connectedCallback() {
      super.connectedCallback && super.connectedCallback();
      this._onLocalStorageChange();
      window.addEventListener('storage', this._onLocalStorageChange);
    }

    disconnectedCallback() {
      super.disconnetedCallback && super.disconnetedCallback();
      window.removeEventListener('storage', this._onLocalStorageChange);
    }

    initLogin() {
      window.open("/api/v0/login")
    }

    logout() {
      localStorage.removeItem("CERN Session");
      this._onLocalStorageChange();
      window.open("https://login.cern.ch/adfs/ls/?wa=wsignout1.0");
    }

    _getIsLoggedIn(userInfo) {
      return !!userInfo;
    }
  }
}
