class L1Home extends AuthEnabled(WhenLifeGivesYouLemonsMakeToast(Polymer.Element)) {
  static get is() { return 'l1-home' }
  static get properties() {
    return {
      /**
       * icon for the light/dark theme toggle
       */
      subsystemData: Object,
      runData: Object,
      urlData: Object,
      urlDataTail: Object,
      route: Object

    };
  }

  constructor() {
    super();
  }

  openWidget(name, tail) {
    this.set("urlData.activeWidget", name);
    if (tail) {
      this.set("urlDataTail.path", "/" + tail);
    }
  }

  openWidgetNewShifterInfo() {
    this.openWidget('new-shifter-info');
  }

  openWidgetSubsystem(event, subsystemName) {
    this.openWidget('subsystem', subsystemName);
  }

  openWidgetProcessController() {
    this.openWidget('process-controller');
  }

  _widgetActive(desiredPath, urlData) {
    return urlData.activeWidget == desiredPath;
  }

  _closeActiveWidget() {
    this.set("urlData.activeWidget", "");
  }

  // fires when any element executes `this.dispatchAction("set_location")`
  set_location(action) {
    this.set('route.path', action.payload);
    action.resolve();
  }

}
customElements.define(L1Home.is, L1Home);
