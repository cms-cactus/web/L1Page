// References:
// http://www.html5rocks.com/en/tutorials/speed/animations/
// https://developer.mozilla.org/en-US/docs/Web/Events/scroll
var ticking = false; // throttle requestAnimationFrame calls
var header_hidden = true;
var toolbar = this.$.toolbar;
function handleScroll() {
  if (window.scrollY == 0 && !header_hidden) {
    toolbar.classList.remove('shadow');
    header_hidden = true;
  }
  else if (window.scrollY != 0 && header_hidden) {
    toolbar.classList.add('shadow');
    header_hidden = false;
  }
}
window.addEventListener('scroll', function on_scroll(e) {
  if (!ticking) {
    window.requestAnimationFrame(function() {
      handleScroll();
      ticking = false;
    });
  }
  ticking = true;
});
handleScroll();
