class BigStatusIcon extends Polymer.Element {
  static get is() { return 'big-status-icon' }
  static get properties() {
    return {
      subsystemData: Object,
      runData: Object,
      runState: {
        type: String,
        computed: "_getRunState(runData)",
        reflectToAttribute: true
      },

      runStatus: {
        type: String,
        computed: "_getRunStatus(subsystemData, runData)",
        reflectToAttribute: true
      }
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getRunState(runInfo) {
    return runInfo.triggerState;
  }

  _getRunStatus(subsystemData, runData) {
    if (!subsystemData || !runData) {
      return;
    }
    var statusnum = runData.includedSubsystems.reduce( function(severity, subsystemName) {
      var subsystem = subsystemData[subsystemName];
      if (!subsystem) { return severity };
      var subsystem_severity = Object.keys(subsystem.apps).reduce( function(severity, appName) {
        var app = subsystem.apps[appName];
        var app_severity = app.alarms.reduce( function(severity, alarm) {
          var alarm_severity = alarm.severity != "ERROR" ? alarm.severity != "WARNING" ? 0 : 1 : 2;
          return alarm_severity > severity ? alarm_severity : severity;
        }, 0);
        return app_severity > severity ? app_severity : severity;
      }, 0);
      return subsystem_severity > severity ? subsystem_severity : severity;
    }, 0);

    // return statusnum != 2 ? statusnum != 1 ? "INFO" : "WARNING" : "ERROR";
    return statusnum;
  }
}
customElements.define(BigStatusIcon.is, BigStatusIcon);
