class L1Page extends Nebula.DispatcherBehavior(AuthEnabled(WhenLifeGivesYouLemonsMakeToast(Polymer.Element))) {
  static get is() { return 'l1-page' }
  static get properties() {
    return {
      /**
       * icon for the light/dark theme toggle
       */
      themeIcon: {
        type: String,
        value: "image:brightness-3"
      },

      apiConnected: {
        type: Boolean,
        value: window.socket && socket.connected
      },

      stickyMenu: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      urlData: {
        type: Object,
         observer: '_urlDataChanged'
      },

      _selectedPage: {
        type: String,
        notify : true
      },

      runData: Object,

      _running: {
        type: Boolean,
        computed: "_isRunning(runData.triggerState)"
      },

      isMainView: {
        type: Boolean,
        value: true,
        reflectToAttribute: true
      },

      isRateView: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    };
  }

  static get observers() {
    return [
      //"__routeChanged(route)"
      //"__isRunning(runData.triggerState)"
    ];
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();

    this._setApiConnected = function() {
      this.apiConnected = true;
    }.bind(this);
    this._unsetApiConnected = function() {
      this.apiConnected = false;
    }.bind(this);

    this.__addSocketListeners = function() {
      socket.on('connect', this._setApiConnected);
      socket.on('disconnect', this._unsetApiConnected);
    }.bind(this);

    if (window.socket) {
      this.__addSocketListeners();
    }
    else {
      document.addEventListener('socket-io-loaded', this.__addSocketListeners);
    }

    @import "_scrollCheck.js";

    // window.scrollTo(0,document.body.scrollHeight);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    if (window.socket) {
      document.removeEventListener('socket-io-loaded', this.__addSocketListeners);
      socket.removeListener('connect', this._setApiConnected);
      socket.removeListener('disconnect', this._unsetApiConnected);
    }
  }

  toggleDrawer() {
    this.$.drawer.toggle();
  }

  _toggleThemeMode() {
    var html = document.body;
    var isDark = html.classList.toggle("dark");
    this.themeIcon = isDark ? "image:wb-sunny" : "image:brightness-3";
  }

  _drawerSelected(event, detail) {
    if (detail.selected) {
      this.set("tail", detail.selected);
    }
  }

  _urlDataChanged(pageData, oldPageData) {
    if ( pageData && pageData.activeWidget == 'rates' ) {
      this._selectedPage = 'rates';
      this.isRateView = true;
      this.isMainView = false;
    } else {
      this._selectedPage = "home";
      this.isRateView = false;
      this.isMainView = true;
    }
  }

  gotoMain() {
    this.set( "route.path", "/");
  }

  gotoRates() {
    this.set( "route.path", "/rates" );
  }

  openWidget(name, tail) {
    this.set("urlData.activeWidget", name);
    if (tail) {
      this.set("urlDataTail.path", "/" + tail);
    }
    console.log('configuration', this.urlData.activeWidget, this.urlDataTail.path );
  }

  openWidgetTrigger() {
    this.openWidget('configuration');
  }

  openWidgetCall() {
    this.openWidget('call');
  }

  _widgetActive(desiredPath, urlData) {
    return urlData.activeWidget == desiredPath;
  }

  _closeActiveWidget() {
    this.set("urlData.activeWidget", "");
  }

  // fires when any element executes `this.dispatchAction("set_location")`
  set_location(action) {
    this.set('route.path', action.payload);
    action.resolve();
  }

  _isRunning(triggerState) {
    return triggerState.toLowerCase() == "running";
  }

  _hasPrescale(trgMoni) {
    return trgMoni && trgMoni.PRESCALE_NAME;
  }

  _displayRate(rate){
    return rate ? rate.toFixed(0) : 0;//number of decimals
  }

  _passThreshold(rate){
    if (rate > 100000){
      return 1;
    }
    else {
      return 0;
    }
  }

}
customElements.define(L1Page.is, L1Page);
