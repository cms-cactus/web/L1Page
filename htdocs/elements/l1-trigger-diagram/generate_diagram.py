#!/usr/bin/env python2

import svgwrite
import operator
from collections import OrderedDict
from lxml import etree


_defcornerradius=5

def prettyPrintXml(xmlFilePathToPrettyPrint):
    assert xmlFilePathToPrettyPrint is not None
    parser = etree.XMLParser(resolve_entities=False, strip_cdata=False)
    document = etree.parse(xmlFilePathToPrettyPrint, parser)
    document.write(xmlFilePathToPrettyPrint,
                   pretty_print=True, encoding='utf-8')


def testA():
    doc = svgwrite.Drawing(filename="test-svgwrite.svg",
                           size=("800px", "600px"))

    doc.add(doc.rect(insert=(0, 0), rx=10, ry=10,
                     size=("200px", "100px"),
                     stroke_width="1",
                     stroke="black",
                     fill="rgb(255,255,0)"))

    t = doc.text("Hello World", insert=(210, 110))
    # t.add(t.tspan(font-family="Helvetica Neue", font-size="16",
    # font-weight="500", fill="black"))
    doc.add(t)

    print(doc.tostring())

    doc.save()


class ServiceBox(object):

    iconsize = (45., 45.)
    """Wrapper class for services

    Attributes:
        name (str):
        title (str):
        insert (:tuple:):
        size (:tuple:):
        rx (int):
        ry (int):
    """

    textoffset = 2

    def __init__(self, name, title, insert, size, rx=_defcornerradius, ry=_defcornerradius, cls=None, style=None, icons=True):
        super(ServiceBox, self).__init__()
        self.name = name
        self.title = title
        self.insert = (insert[0] - size[0] / 2., insert[1] - size[1] / 2.)
        self.size = size
        self.rx = rx
        self.ry = ry
        self.cls = cls
        self.style = style
        self.icons = icons

    #--------------------------------------------------------------------------
    def addTo(self, dwg, parent):

        # The Box
        rect = parent.add(
            dwg.rect(
                id=self.name,
                rx=self.rx,
                ry=self.ry,
                insert=self.insert,
                size=self.size
            )
        )

        # Box name
        text = parent.add(
            dwg.text(
                "",
                for_=self.name,
                insert=self.insert,
                transform=self.translate(
                    self.size[0] / 2, (self.size[1] / 2) + self.textoffset
                ),
                debug=False
            )
        )
        text.add(
            dwg.tspan(self.title, text_anchor="middle",
                      alignment_baseline="middle", # Chrome
                      dominant_baseline="middle" # Firefox
                      )
        )

        # Icon set
        svg = parent.add(
            dwg.g(for_=self.name, transform=self.translate(
                self.size[0] - self.iconsize[0] / 2, -self.iconsize[1] / 2), debug=False)
        ).add(
            dwg.svg(insert=self.insert)
        )

        if self.icons:
            svg.add(dwg.use("#error", icon='error', debug=False, transform="scale(2)"))
            svg.add(dwg.use("#warning", icon='warning', debug=False, transform="scale(2)"))
            svg.add(dwg.use("#info", icon='info', debug=False, transform="scale(2)"))
            svg.add(dwg.use("#dead", icon='dead', debug=False, transform="scale(0.07)"))

        if self.cls:
            rect['class'] = self.cls
            text['class'] = self.cls

        if self.style:
            rect['style'] = self.style
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    def translate(self, dx=0, dy=0):
        return 'translate({0} {1})'.format(dx, dy)
    #--------------------------------------------------------------------------


    #--------------------------------------------------------------------------
    def anchor(self, side, n, i):
        """Calculates the anchor point coordinates

        Args:
            side (str): side on which the anchor point lies
            n (int): number of anchors
            i (int): anchor index

        Returns:
            tuple(int, int): coordinates of the anchor
        """

        if i > n - 1:
            raise RuntimeError(
                'anchor index {0} out of bounds 0-{1}'.format(i, n - 1))

        if side == 't':
            start = self.corner('t', 'l')
            stop = self.corner('t', 'r')
        elif side == 'b':
            start = self.corner('b', 'l')
            stop = self.corner('b', 'r')
        elif side == 'l':
            start = self.corner('t', 'l')
            stop = self.corner('b', 'l')
        elif side == 'r':
            start = self.corner('t', 'r')
            stop = self.corner('b', 'l')
        else:
            raise RuntimeError("Unknown anchor side {0}".format(side))

        dist = tuple(map(operator.sub, stop, start))

        pt = tuple(
            map(lambda x, y: x + (float(i + 1) / float(n + 1)) * y, start, dist))
        print start, stop, dist, pt, i + 1
        return pt
    #--------------------------------------------------------------------------


    #--------------------------------------------------------------------------
    def corner(self, v, h):

        """Computes the corner coordinates

        v (str): Vertical edge identifier
        h (str): Horizontal edge identifier
        """

        if h == 'l':
            x = self.insert[0]
        elif h == 'r':
            x = self.insert[0] + self.size[0]
        else:
            raise RuntimeError("Unknown horizontal selector {0}".format(h))

        if v == 't':
            y = self.insert[1]
        elif v == 'b':
            y = self.insert[1] + self.size[1]
        else:
            raise RuntimeError("Unknown vertical selector {0}".format(v))

        return x, y
    #--------------------------------------------------------------------------


#------------------------------------------------------------------------------
def drawDiagram(filename, viewBox, debug=False):
    dwg = svgwrite.Drawing(filename=filename,
                           viewBox=viewBox
                           )


    # dwg.add(
    #     dwg.rect(
    #         size=viewBox
    #     )
    # )
    # errorGrp = dwg.g(id="error", transform="scale(2)")
    # errorGrp.


    # Error icon
    dwg.defs.add(
        dwg.g(id="error")
    ).add(
        dwg.path(
            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z")
    )

    # Warning icon
    dwg.defs.add(
        dwg.g(id="warning")
    ).add(
        dwg.path(d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z")
    )

    # Info icon
    dwg.defs.add(
        dwg.g(id="info")
    ).add(
        dwg.path(d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z")
    )

    # Dead icon
    deadGrp = dwg.defs.add(dwg.g(id="dead"))
    deadGrp.add(
        dwg.path(d="M122.855,335.491c3.514,11.463,7.147,23.318,8.96,35.258c0.741,4.866,3.647,40.261,5.218,59.707"
                   "c0.602,7.451,6.252,13.508,13.643,14.626l25.821,3.904c3.723,0.563,6.692,3.402,7.417,7.097l3.402,17.401"
                   "c1.479,7.563,8.108,13.018,15.814,13.014l200.241-0.119c7.572-0.005,14.118-5.283,15.726-12.682l3.896-17.915"
                   "c0.86-3.536,3.782-6.193,7.383-6.717l31.437-4.566c7.554-1.097,13.3-7.344,13.759-14.964c1.206-20.008,3.457-56.434,4.155-61.425"
                   "c1.653-11.784,5.131-23.432,8.49-34.764c4.709-15.857,9.578-32.225,9.578-49.248c0.006-108.709-86.402-197.132-192.608-197.132"
                   "s-192.613,88.471-192.613,197.204C112.575,301.952,117.801,319.004,122.855,335.491z M330.285,325.459"
                   "c1.221-30.877,28.91-38.641,59.787-37.421c30.877,1.221,53.249,10.965,52.028,41.842c-1.221,30.877-27.241,54.918-58.118,53.697"
                   "C353.105,382.356,329.064,356.336,330.285,325.459z M312.119,386.048l20.042,29.679c1.778,2.632,2.021,6.011,0.639,8.871"
                   "l-2.875,5.951c-1.489,3.081-4.609,5.038-8.03,5.038h-17.167h-17.167c-3.422,0-6.542-1.958-8.03-5.038l-2.875-5.951"
                   "c-1.382-2.86-1.138-6.238,0.639-8.871l20.042-29.679C300.873,380.811,308.583,380.811,312.119,386.048z M220.299,288.039"
                   "c30.877-1.221,58.566,6.543,59.787,37.421c1.221,30.877-22.82,56.897-53.697,58.118c-30.877,1.221-56.897-22.82-58.118-53.697"
                   "C167.05,299.003,189.422,289.259,220.299,288.039z"""
                 )
    )
    deadGrp.add(
        dwg.path(d="M556.35,58.611c2.402-14.75-2.089-30.405-13.499-41.757c-18.326-18.234-48.224-18.566-66.929-0.721"
                   "c-19.558,18.659-19.834,49.646-0.828,68.652l-17.47,17.47c-6.98,6.981-6.98,18.299,0,25.28l29.802,29.802"
                   "c6.98,6.981,18.299,6.981,25.279,0l17.47-17.47c19.007,19.007,49.994,18.73,68.652-0.828"
                   "c17.845-18.705,17.512-48.604-0.722-66.929C586.754,60.7,571.1,56.209,556.35,58.611z"
                 )
    )
    deadGrp.add(
        dwg.path(d="M81.824,139.866l17.47,17.47c6.981,6.981,18.299,6.981,25.279,0l29.802-29.802c6.981-6.981,6.981-18.299,0-25.28"
                   "l-17.47-17.47c19.007-19.006,18.73-49.993-0.828-68.652C117.372-1.712,87.474-1.38,69.148,16.854"
                   "C57.739,28.206,53.248,43.861,55.65,58.611C40.9,56.209,25.246,60.7,13.893,72.109c-18.234,18.325-18.567,48.224-0.722,66.929"
                   "C31.83,158.596,62.817,158.872,81.824,139.866z"
                 )
    )
    deadGrp.add(
        dwg.path(d="M598.829,472.962c-18.659-19.559-49.646-19.834-68.652-0.828l-17.47-17.47c-6.981-6.981-18.299-6.981-25.279,0"
                   "l-29.802,29.802c-6.98,6.981-6.98,18.299,0,25.28l17.47,17.47c-19.007,19.007-18.73,49.994,0.828,68.652"
                   "c18.705,17.845,48.604,17.513,66.929-0.721c11.41-11.352,15.901-27.007,13.499-41.757c14.75,2.402,30.404-2.089,41.756-13.499"
                   "C616.341,521.565,616.673,491.667,598.829,472.962z"
                 )
    )
    deadGrp.add(
        dwg.path(d="M55.65,553.389c-2.402,14.75,2.089,30.405,13.499,41.757c18.326,18.234,48.224,18.566,66.929,0.721"
                 "c19.558-18.659,19.834-49.646,0.828-68.652l17.47-17.47c6.981-6.98,6.981-18.299,0-25.28l-29.802-29.802"
                 "c-6.981-6.981-18.299-6.981-25.279,0l-17.47,17.47c-19.007-19.007-49.994-18.731-68.652,0.828"
                 "c-17.845,18.705-17.513,48.603,0.721,66.929C25.246,551.3,40.9,555.791,55.65,553.389z"
                 )
    )

    # Marker
    dwg.defs.add(
        dwg.marker(orient="auto", overflow="visible", markerUnits="strokeWidth",
                   id="FilledArrow_Marker", viewBox="-1 -4 10 8", markerWidth="10", markerHeight="8", refX="9")
    ).add(
        dwg.g()
    ).add(
        dwg.path(d="M 8 0 L 0 -3 L 0 3 Z", fill="currentColor",
                 stroke="currentColor", stroke_width="1")
    )


    #---------------------------------------------------------------------------
    if debug:
        x, y, w, h = (int(p) for p in viewBox.split())
        dwg.add(
            dwg.rect(
                size=(w, h),
                insert=(x,y),
                style="fill:rgb(255,255,255);stroke-width:1;stroke:rgb(0,0,0)"
            )
        )
    #---------------------------------------------------------------------------

    mainGrp = dwg.add(dwg.g())

    # Geometry
    boxH = 40
    boxW = 80

    y0 = 90
    dy = 85

    # uGT (central anchor)
    gtX0 = 415
    dx = 110

    # calo (central anchor)
    caloX0 = gtX0 - dx * 1.25
    ecalX0 = caloX0 - 0.5* dx
    hcalX0 = caloX0 + 0.5* dx
    hfX0 = caloX0 + dx

    # muon (central anchor)
    muX0 = gtX0 + dx * 1.25
    bmtfX0 = muX0 - dx * 1.2
    emtfX0 = muX0 + dx * 1.2
    tmuxX0 = muX0 + (bmtfX0-muX0)*0.5
    cppfX0 = muX0 + (emtfX0-muX0)*0.6
    dtX0 = muX0 - dx
    cscX0 = muX0 + dx

    # Services
    centralW = boxW*1.3
    srvW = boxW*1.1

    centralX0 = ecalX0+centralW/2.-boxW*0.7
    srvX0 = emtfX0-srvW/2.+boxW*0.7


    y1 = y0 + dy
    y2 = y0 + 2 * dy
    y2_5 = y0 + 2.5 * dy
    y3 = y0 + 3 * dy
    y4 = y0 + 4.25 * dy
    # Boxes
    boxes = OrderedDict([
        ('central', ServiceBox("CENTRAL", "Central Cell", (centralX0, y0-boxH/2),
                            size=(centralW, boxH))),
        ('l1ce', ServiceBox("L1CE", "L1CE", (srvX0, y0-boxH*0.5),
                            size=(srvW, boxH), rx=0, ry=0, cls='service')),
        ('xaas', ServiceBox("XDAQ_Services", "Monitoring", (srvX0, y0+boxH*0.5),
                            size=(srvW, boxH), rx=0, ry=0, cls='service')),

        ('ugt', ServiceBox("UGT", u"\u03BC" + "GT", (gtX0, y0), size=(boxW, boxH))),
        ('ugmt', ServiceBox("UGMT", u"\u03BC" +
                            "GMT", (muX0, y1), size=(boxW, boxH))),
        ('calol2', ServiceBox("CALOL2", "Calo L2",
                              (caloX0, y1), size=(boxW, boxH))),
        ('calol1', ServiceBox("CALOL1", "Calo L1",
                              (caloX0, y2_5), size=(boxW, boxH))),
        ('bmtf', ServiceBox("BMTF", "BMTF", (bmtfX0, y2), size=(boxW, boxH))),
        ('omtf', ServiceBox("OMTF", "OMTF", (muX0, y2), size=(boxW, boxH))),
        ('emtf', ServiceBox("EMTF", "EMTF", (emtfX0, y2), size=(boxW, boxH))),
        ('twinmux', ServiceBox("TWINMUX", "TWINMUX",
                               (tmuxX0, y3), size=(boxW*1.4, boxH))),
        ('cppf', ServiceBox("CPPF", "CPPF", (cppfX0, y3), size=(boxW, boxH))),
        ('ecal', ServiceBox("ECAL", "ECAL", (ecalX0, y4), size=(boxW, boxH))),
        ('hcal', ServiceBox("HCAL", "HCAL", (hcalX0, y4), size=(boxW, boxH))),
        # ('hf', ServiceBox("HF", "HF", (hfX0, y4), size=(boxW, boxH))),
        ('dt', ServiceBox("DT", "DT", (dtX0, y4), size=(boxW, boxH))),
        ('rpc', ServiceBox("RPC", "RPC", (muX0, y4), size=(boxW, boxH))),
        ('csc', ServiceBox("CSC", "CSC", (cscX0, y4), size=(boxW, boxH))),

        # ('l1ce', ServiceBox("L1CE", "L1CE", (ecalX0, y0 + 4.75 * dy),
        #     size=(boxW, boxH), cls='service')),
        # ('l1page', ServiceBox("L1Page", "L1Page",
                            #   (caloX0, y0 + 4.75 * dy), size=(boxW, boxH), cls='service')),
        # ('xaas', ServiceBox("XDAQ_Services", "XAAS", (hfX0, y0 + 4.75 * dy),
        #     size=(boxW, boxH), cls='service')),
    ])

    # for name,box in boxes.iteritems():
    # box.add(dwg, mainGrp)

    def draw_connection(boxes, from_box, to_box, r=0.5):
        """
        Return a line connecting two boxes

        Args:
            boxes (list): List of all boxes
            from_box (:dict:): box name, args to anchor function
            to_box (:dict:): box name, args to anchor function
            r (float):

        Returns:
            svg line connecting the two boxes
        """
        def makeOrthConn( a, b, r=0.5):
            return [a, (a[0],a[1]+(b[1]-a[1])*r), (b[0], a[1]+(b[1]-a[1])*r), b]

        return dwg.polyline(
            makeOrthConn(
                boxes[from_box['name']].anchor(*from_box['anchor']),
                boxes[to_box['name']].anchor(*to_box['anchor']),
                r=r),
            fill='none',
            marker_end="url(#FilledArrow_Marker)",
            id="_".join([from_box['name'], to_box['name']])
        )
    # Connecting lines

    links = [
        draw_connection(boxes, # ugmt -> ugt
                        {'name': 'ugmt', 'anchor': ('t', 1, 0)},
                        {'name': 'ugt', 'anchor': ('b', 2, 1)}),
        draw_connection(boxes, # calol2 -> ugt
                        {'name': 'calol2', 'anchor': ('t', 1, 0)},
                        {'name': 'ugt', 'anchor': ('b', 2, 0)}),
        draw_connection(boxes, # calol1 -> calol2
                        {'name': 'calol1', 'anchor': ('t', 1, 0)},
                        {'name': 'calol2', 'anchor': ('b', 1, 0)}),

        draw_connection(boxes,  # bmtf -> ugmt
                        {'name': 'bmtf', 'anchor': ('t', 1, 0)},
                        {'name': 'ugmt', 'anchor': ('b', 3, 0)}),
        draw_connection(boxes,  # omtf -> ugmt
                        {'name': 'omtf', 'anchor': ('t', 1, 0)},
                        {'name': 'ugmt', 'anchor': ('b', 3, 1)}),
        draw_connection(boxes,  # emtf -> ugmt
                        {'name': 'emtf', 'anchor': ('t', 1, 0)},
                        {'name': 'ugmt', 'anchor': ('b', 3, 2)}),

        draw_connection(boxes,  # twinmux -> bmtf
                        {'name': 'twinmux', 'anchor': ('t', 4, 1)},
                        {'name': 'bmtf', 'anchor': ('b', 1, 0)}),
        draw_connection(boxes,  # twinmux -> omtf
                        {'name': 'twinmux', 'anchor': ('t', 4, 2)},
                        {'name': 'omtf', 'anchor': ('b', 3, 0)}),
        draw_connection(boxes,  # cppf -> emtf
                        {'name': 'cppf', 'anchor': ('t', 1, 0)},
                        {'name': 'emtf', 'anchor': ('b', 3, 0)}),

        draw_connection(boxes,  # ecal -> calo1
                        {'name': 'ecal', 'anchor': ('t', 1, 0)},
                        {'name': 'calol1', 'anchor': ('b', 2, 0)},
                        r=0.3),
        draw_connection(boxes,  # hcal -> calo1
                        {'name': 'hcal', 'anchor': ('t', 1, 0)},
                        {'name': 'calol1', 'anchor': ('b', 2, 1)},
                        r=0.3),

        # draw_connection(boxes,  # hf -> calo1
        #                 {'name': 'hf', 'anchor': ('t', 1, 0)},
        #                 {'name': 'calol1', 'anchor': ('b', 3, 0)},
        #                 r=0.3),

        draw_connection(boxes,  # dt -> twinmux
                        {'name': 'dt', 'anchor': ('t', 1, 0)},
                        {'name': 'twinmux', 'anchor': ('b', 4, 1)}),
        draw_connection(boxes,  # rpc -> twinmux
                        {'name': 'rpc', 'anchor': ('t', 3, 0)},
                        {'name': 'twinmux', 'anchor': ('b', 4, 2)}),
        draw_connection(boxes,  # rpc -> omtf
                        {'name': 'rpc', 'anchor': ('t', 3, 1)},
                        {'name': 'omtf', 'anchor': ('b', 3, 1)}),
        draw_connection(boxes,  # rpc -> cppf
                        {'name': 'rpc', 'anchor': ('t', 3, 2)},
                        {'name': 'cppf', 'anchor': ('b', 1, 0)}),
        draw_connection(boxes,  # csc -> omtf
                        {'name': 'csc', 'anchor': ('t', 3, 0)},
                        {'name': 'omtf', 'anchor': ('b', 3, 2)},
                        r=0.3),
        draw_connection(boxes,  # csc -> emtf
                        {'name': 'csc', 'anchor': ('t', 3, 2)},
                        {'name': 'emtf', 'anchor': ('b', 1, 0)},
                        r=0),
    ]

    decorators = [
        dwg.line(
            start=(boxes['ecal'].corner('t', 'l')[0], y0 + 3.8 * dy),
            end=(boxes['emtf'].corner('t', 'r')[0], y0 + 3.8 * dy),
            stroke_linecap="butt", stroke_dasharray="1,9"
        ),
    ]

    for line in links:
        if debug:
            line['style'] = "stroke:rgb(128,128,128);stroke-width:1"
        mainGrp.add(line)

    # for line in connectingLines:
    #     line['style']="stroke:rgb(255,0,0);stroke-width:1"
    #     mainGrp.add(line)

    for line in decorators:
        if debug:
            line['style'] = "stroke:rgb(128,128,128);stroke-width:1"
        mainGrp.add(line)

    for name, box in boxes.iteritems():
        if debug:
            box.style = "fill:rgb(255,255,255);stroke-width:1;stroke:rgb(0,0,0)"
            box.icons = False
        box.addTo(dwg, mainGrp)


    dwg.save()
    prettyPrintXml(filename)
    with open(filename) as svg:
        for line in svg:
            print line,
#------------------------------------------------------------------------------


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    parser.add_argument('-d', '--debug', default=False, action='store_true')
    args = parser.parse_args()

    drawDiagram(args.filename, ('155 30 600 460'), args.debug)
