class L1TriggerDiagram extends Polymer.Element {
  static get is() { return 'l1-trigger-diagram' }
  static get properties() {
    return {
      /**
       * subsystems data as received from the API
       */
      subsystemData: {
        type: Object
      },

      /**
       * list subsystems in the current run as received from the API
       */
      includedSubsystems: {
        type: Array
      }
    }
  }
  static get observers() {
    return [
      "_renderSubsystemData(subsystemData, includedSubsystems)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
    var elements = this.root.querySelectorAll('rect');
    // elements is an array-like object
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      element.addEventListener('click', this._registerClick(element.id));
    }
  }

  _registerClick(name) {
    return function elementClicked() {
      this.dispatchEvent(new CustomEvent('subsystem-click', {detail: name}, {bubbles: true}))
    }.bind(this);
  }

  _renderSubsystemData(subsystemData, includedSubsystems) {
    var boxes = this.root.querySelectorAll("rect");
    for (var i = 0; i < boxes.length; i++) {
      var box = boxes[i];
      box.dataset.inRun = includedSubsystems && includedSubsystems.indexOf(box.id) != -1 || box.dataset.isService == "true";
      var text = this.root.querySelector("text[for='" + box.id + "']");
      text.dataset.inRun = box.dataset.inRun;
    }

    // re-draw connections if necessary
    var connections = this.root.querySelectorAll("polyline");
    let boxesArray = Array.from(boxes); // to not create them repeatedly in the following loop; necessary to us Array.find()
    for (let i = 0; i < connections.length; i++) {
      var connection = connections[i];
      let [idFrom, idTo] = connection.id.split('_');
      let fromBox = boxesArray.find( box => { return box.id.toLowerCase() === idFrom; });
      let toBox = boxesArray.find( box => { return box.id.toLowerCase() === idTo; });

      // if the subsystem from which the connection originates or to which it feeds is not in the run, make the line dashed
      if (fromBox.dataset.inRun === 'false' || toBox.dataset.inRun === 'false') {
        connection.dataset.inactive = 'true';
      } else {
        connection.dataset.inactive = "false";
      }
    }

    subsystemData && Object.keys(subsystemData).forEach( function(subsystemName) {
      var apps = subsystemData[subsystemName].apps;
      var logState = 0; // 0=ok, 1=warning, 2=error
      var healthState = 0; // 0=ok, 1=non-critical-death, 2=critical-death

      // check for dead apps
      Object.keys(apps).forEach( function(appName) {
        var app = apps[appName];
        var appstate = app.healthy == false ? app.critical ? 2 : 1 : 0;
        if (appstate > healthState) {
          healthState = appstate;
        }
      });

      // check for warnings/errors
      Object.keys(apps).forEach( function(appName) {
        var app = apps[appName];
        var appstate = app.alarms.reduce( function(appstate, alarm) {
          var severity = alarm.severity == 'ERROR' ? 2 : alarm.severity == 'WARNING' ? 1 : 0;
          return severity > appstate ? severity : appstate;
        }, 0);
        if (appstate > logState) {
          logState = appstate;
        }
      });

      logState = logState == 0 ? "info" : logState == 1 ? "warning" : "error";
      var icon = healthState == 2 ? "dead" : logState;
      healthState = healthState == 0 ? "healthy" : healthState == 1 ? "non-critical-death" : "critical-death";

      var box = this.root.querySelector("#" + subsystemName);
      var iconbox = this.root.querySelector("g[for=" + subsystemName + "]");
      if (box && iconbox) {
        box.dataset.logState = logState;
        box.dataset.healthState = healthState;
        iconbox.dataset.icon = icon;
      }
      else {
        console.warn(`${subsystemName} is not in the l1-trigger diagram, its status is ${healthState} and ${logState}`);
      }

    }.bind(this));
  }
}
customElements.define(L1TriggerDiagram.is, L1TriggerDiagram);
