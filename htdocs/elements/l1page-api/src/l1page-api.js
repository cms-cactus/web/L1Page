class L1pageApi extends Polymer.Element {
  static get is() { return 'l1page-api' }

  /**
   * Fired when a request is sent.
   *
   * @event request
   */
  /**
   * Fired when a response is received.
   *
   * @event response
   */
  /**
   * Fired when an error is received.
   *
   * @event error
   */
  static get properties() {
    return {
      /**
       * The data you wish to put the result of the ajax call into
       */
      data: {
        type: Object,
        //we notify manually, if the response data would be identical the normal notify would not fire
        // notify: false
        notify: true
      },
      /**
       * The format of the callback data. Will probably be either json or text
       */
      handleAs: {
        type: String,
        value: 'json'
      },
      /**
       * The url endpoint to make the request to
       */
      url: {
        type: String,
        value: "l1-api url not set",
        observer: "_urlChanged"
      },
      /**
       * The timeout after which an update attempt must abort
       */
      timeout: {
        type: Number,
        value: 30000
      },
      /**
       * Names of the parameter variables to send along with the request
       * This is just a list of names, set the values like this:
       * this.push('parameters', 'myvariable');
       * this.myvariable = 5;
       */
      parameters: {
        type: Array,
        value: function() {return [];},
        observer: "_parametersChanged"
      },
      /**
       * Javascript date string representing the last time an update was successful
       */
      lastUpdated: {
        type: String,
        value: "never"
      },
      /**
       * if set, l1-api will make a request immediately and when url changes
       * or any of the parameters listed in the parameters property
       */
      auto: {
        type: Boolean,
        value: false,
        observer: "_autoChanged"
      },
      _loading: {
        type: Boolean
      },
      /*
       * True while request is in flight.
       */
      loading: {
        type: Boolean,
        notify: true,
        computed: "_getLoading(_loading)"
      },
      method: {
        type: String,
        value: "GET"
      },

      // name of the key in localStorage that holds the desired JWT token
      // if unset, no authentication info is sent
      authTokenName: String
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
    this.isAttached = true;
    if (this.auto) {
      this.generateRequest();
    }

    // auto renew data if a reconnection happened
    this.__onConnect = function() {
      if (this.auto && this.method.toLowerCase() == "get") {
        console.log("renewing resource", this.url, "after API reconnected");
        this.generateRequest();
      }
    }.bind(this);

    // listen for change notifications
    // also updates on sub-resources, therefore we need a debouncer
    this.__onUpdatedResourceDebounce;
    this.__markedForUpdate = false;
    this.__onUpdatedResource = function(path) {
      if (this.auto && this.method.toLowerCase() == "get" && path.indexOf(this.url) > -1) {
        this.__markedForUpdate = true;
      }
      clearTimeout(this.__onUpdatedResourceDebounce);
      this.__onUpdatedResourceDebounce = setTimeout( function() {
        if (this.__markedForUpdate) {
          this.__markedForUpdate = false;
          console.log("updating resource ", this.url);
          this.generateRequest()
        }
      }.bind(this), 100);
    }.bind(this);

    this.__addSocketListeners = function() {
      socket.on('connect', this.__onConnect);
      socket.on('updated resource', this.__onUpdatedResource);
    }.bind(this);

    if (window.socket) {
      this.__addSocketListeners();
    }
    else {
      document.addEventListener('socket-io-loaded', this.__addSocketListeners);
    }
  }
  disconnectedCallback() {
    super.disconnectedCallback();
    if (window.socket) {
      document.removeEventListener('socket-io-loaded', this.__addSocketListeners);
      socket.removeListener('connect', this.__onConnect);
      socket.removeListener('updated resource', this.__onUpdatedResource);
    }
  }

  _getLoading(_loading) {
    return _loading
  }

  /**
   * AJAX response handler
   */
  _onResponse(event, detail, sender) {
    event.preventDefault();
    event.stopPropagation();
    if (!detail.response) {
      console.error(this, "got back a non-json response");
      this.dispatchEvent(new CustomEvent('400-error', {detail: detail}, { bubbles: true }));
    }
    else {
      this.set('lastUpdated', Date());
      this.set('data', detail.response);
      this.dispatchEvent(new CustomEvent('response', {detail: detail}, { bubbles: false }));
    }
  }

  /**
   * AJAX error handler
   */
  _onError(event, detail, sender) {
    if (detail.request.xhr.status == 401 || detail.request.xhr.status == 403) {
      this.dispatchEvent(new CustomEvent('401-error', {detail: detail}, { bubbles: true }));
    // } else if (detail.request.xhr.status == 504 || detail.request.xhr.status == 502) {
    //   // this.throwToast({
    //   //   'type': 'error',
    //   //   'message': 'Request timed out. Please try again later.'
    //   // });
    //   this.dispatchEvent(new CustomEvent('error', detail, { bubbles: false }));
    } else {
      var report = {
        code: detail.request.status,
        response: detail.request.response
      };
      console.error('error in l1-api', report);
      this.dispatchEvent(new CustomEvent('error', {detail: report}, { bubbles: false }));
      this.dispatchEvent(new CustomEvent('5xx-error', {detail: report}, { bubbles: true }));
    }
    event.preventDefault();
    event.stopPropagation();
  }

  _generateRequest() {
    if (this.isAttached) {
      this.generateRequest();
    }
  }

  /**
   * fire a new ajax request
   */
  generateRequest() {
    var ajax = this.$.ajax;
    if (this.url == "l1-api url not set") {
      return;
    }
    ajax.url = this.url;
    ajax.method = this.method;

    if (this.parameters) {
      var params = this.parameters.reduce( function(params, param) {
        var value = this[param];
        if ( typeof value == "undefined") {
          value = this.getAttribute(Polymer.CaseMap.camelToDashCase(param));
          if (!value) {
            console.error(this, this.url, `${param} is in the list of parameters but is undefined, set value to null if this is intentional`);
            return;
          }
        }
        params[param] = value;
        return params;
      }.bind(this), {});

      if (this.method.toLowerCase() != "get") {
        ajax.body = params;
      } else {
        var strParams = Object.keys(params).reduce( function(strParams, key) {
          return strParams + `&${key}=${params[key]}`;
        }, "");
        if (ajax.url.indexOf("?") == -1) {
          // replace the first '&' with '?'
          strParams = "?" + strParams.substring(1);
        }
        ajax.url += strParams;
      }
    }

    if (localStorage["CERN Session"]) {
      ajax.headers = {
        "Authorization": "JWT " + localStorage["CERN Session"]
      };
    }
    ajax.handleAs = this.handleAs;
    ajax.timeout = this.timeout;

    ajax.generateRequest();
    this.dispatchEvent(new CustomEvent('request', ajax.lastRequest, { bubbles: true }));
  }

  _parametersChanged(parameters, old) {
    // // register parameters in Polymer property effect system
    // // then they get picked up by __setProperty(), which triggers _propertyChanged()
    // for (var i = 0; i < parameters.length; i++) {
    //   if (!this._propertyEffects[parameters[i]]) {
    //     this._propertyEffects[parameters[i]] = [];
    //   }
    // }
  }

  _autoChanged(auto, old) {
    if (auto && typeof old != "undefined") {
      this._generateRequest();
    }
  }

  _urlChanged(url) {
    if (this.auto) {
      this._generateRequest();
    }
  }

  _propertyChanged(property, value, old) {
    if (this.parameters && this.parameters.indexOf(property) > -1) {
      // we tricked Polymer in firing _propertyChanged(), but our
      // properties are not really registered.
      // we must perform the update ourselves.
      this[property] = value;
      if (this.auto && value) {
        this._generateRequest();
      }
    }
  }
}
customElements.define(L1pageApi.is, L1pageApi);
