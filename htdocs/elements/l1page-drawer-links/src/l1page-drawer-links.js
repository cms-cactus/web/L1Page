class L1pageDrawerLinks extends Polymer.Element {
  static get is() { return 'l1page-drawer-links' }
  static get properties() {
    return {};
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getSections(links) {
    return Object.keys(links.apps).map( function(sectionName) {
      return {
        name: sectionName,
        apps: links.apps[sectionName]
      }
    })
  }
}
customElements.define(L1pageDrawerLinks.is, L1pageDrawerLinks);
