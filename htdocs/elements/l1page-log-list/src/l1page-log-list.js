class L1pageLogList extends Nebula.DispatchBehavior(Polymer.Element) {
  static get is() { return 'l1page-log-list' }
  static get properties() {
    return {
      /**
       * subsystems data as received from the API
       */
      subsystemData: Object,
      runData: Object,
      logs: {
        type: Array,
        computed: "_getLogs(subsystemData, runData.includedSubsystems, runData.triggerState)",
        value: []
      }
    };
  }
  static get observers() {
    return [];
  }
  isInRun(triggerState) {
    return triggerState == "Running";
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getLogs(subsystemData, includedSubsystems, triggerState) {
    if (!subsystemData || !includedSubsystems || !triggerState) { return }
    if (!this.isInRun(triggerState)) {
      return [];
    }
    return includedSubsystems.reduce( function(result, subsystemName) {
      var subsystem = subsystemData[subsystemName];
      if (!subsystem) { return result };
      return result.concat(Object.keys(subsystem.apps).reduce( function(logs, appName) {
        var app = subsystem.apps[appName];
        return logs.concat(app.alarms.map( function(alarm) {
          // example alarm:
          // { details: 'Restart is needed',
          //   name: 'module2',
          //   severity: 'ERROR' }
          return {
            subsystem: subsystemName,
            app: appName,
            name: alarm.name,
            severity: alarm.severity,
            message: alarm.details
          }
        }));
      }, []));
    }, [])
    .filter( alarm => alarm.severity != "INFO")
    // }, [{
    //   subsystem: "DUMMY",
    //   app: "app1",
    //   name: "module 1",
    //   severity: "ERROR",
    //   message: "Warning: no l1ts_cell flashlist available for CENTRAL subsystem!"
    // },
    // {
    //   subsystem: "DUMMY",
    //   app: "app1",
    //   name: "module 1",
    //   severity: "INFO",
    //   message: "Warning: no l1ts_cell flashlist available for CENTRAL subsystem!"
    // },
    // {
    //   subsystem: "DUMMY",
    //   app: "app1",
    //   name: "module 1",
    //   severity: "WARNING",
    //   message: "Warning: no l1ts_cell flashlist available for CENTRAL subsystem!"
    // }]);
  }

  _severity2num(severity) {
    return severity == "ERROR" ? 2 : severity == "WARNING" ? 1 : 0;
  }
  _sortLogs(a,b) {
    var sev_a = this._severity2num(a.severity);
    var sev_b = this._severity2num(b.severity);
    if (sev_a == sev_b) {
      return 0;
    }
    else if (sev_a > sev_b) {
      return -1;
    }
    else {
      return 1;
    }
  }

  _isEmpty(logsLength) {
    return logsLength == 0;
  }

  _getLogEntrySubtitle(app, piece) {
    return app + "::" + piece;
  }

  _handleClick(event) {
    var entry = event.model.entry;
    this.dispatchAction('set_location', "/subsystem/" + entry.subsystem);
  }

  _openApp(event) {
    var entry = event.model.entry;
    var url = this.subsystemData[entry.subsystem].apps[entry.app].uri;
    window.open(url);
    event.stopPropagation();
  }
}
customElements.define(L1pageLogList.is, L1pageLogList);
