class LogEntry extends Polymer.Element {
  static get is() { return 'log-entry' }
  static get properties() {
    return {
      severity: {
        type: String,
        reflectToAttribute: true
      },
      title: String,
      subtitle: String,
      message: String
    };
  }
  static get observers() {
    return [
      "_renderMessage(message)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getIcon(severity) {
    if (severity == "ERROR") {
      return "error";
    }
    else if (severity == "WARNING") {
      return "warning";
    }
    else if (severity == "INFO") {
      return "info-outline"
    }
    else {
      return severity;
    }
  }

  _renderMessage(message) {
    this.$.message.innerHTML = message.replace(/\\n/g, "<br>");
  }
}
customElements.define(LogEntry.is, LogEntry);
