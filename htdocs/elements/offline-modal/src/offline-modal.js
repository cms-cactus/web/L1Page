class OfflineModal extends Polymer.Element {
  static get is() { return 'offline-modal' }
  static get properties() {
    return {};
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
    this.setupTest();
  }

  setupTest() {
    this._test = setTimeout(this.testAPI.bind(this), 5000);
  }

  testAPI() {
    var script = document.createElement('script');
    script.src = location.protocol + "//" + location.hostname + ":3001/socket.io/socket.io.js";
    script.onload = function onSocketLoaded() {
      console.log("api is onine now");
      location.reload();
    }
    script.onerror = function onSocketFailed() {
      console.log("api is still offline");
      document.head.removeChild(script);
      script = null;
      this.setupTest();
    }.bind(this);
    document.head.appendChild(script);
  }

  reload() {
    setTimeout(function () {
      location.reload();
    }, 300);
  }
}
customElements.define(OfflineModal.is, OfflineModal);
