class OopsDinosaur extends Polymer.Element {
  static get is() { return 'oops-dinosaur' }
  static get properties() {
    return {
      message: String
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }
}
customElements.define(OopsDinosaur.is, OopsDinosaur);
