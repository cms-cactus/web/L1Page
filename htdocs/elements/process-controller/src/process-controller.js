class ProcessController extends WhenLifeGivesYouLemonsMakeToast(Polymer.Element) {
  static get is() { return 'process-controller' }
  static get properties() {
    return {
      subsystemData: Object,
      triggerState: String,
      loading: {
        type: Boolean,
        value: false
      },

      busy: {
        type: Boolean,
        computed: "_isBusy(subsystemData, loading)"
      }
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getSubsystems(subsystems) {
    return subsystems && Object.keys(subsystems).map( function(subsystemName) {
      var r = subsystems[subsystemName];
      r.name = subsystemName;
      return r;
    });
  }

  _filterSubsystems(subsystem) {
    return subsystem.actions && subsystem.actions.length == 4;
  }

  _notifyResize() {
    this.dispatchEvent(new Event('iron-resize', {bubbles: true}))
  }

  _isBusy(subsystemData, loading) {
    return !subsystemData || loading;
  }

  requestStatus() {
    this._doAction("status");
  }
  requestStart() {
    this._doAction("start");
  }
  requestStop() {
    this._doAction("stop");
  }
  requestRestart() {
    this._doAction("restart");
  }
  _doAction(action) {

    var checkboxes = this.$.subsystems.querySelectorAll('paper-checkbox[checked]');
    var subsystems = [];
    for (var i = 0; i < checkboxes.length; i++) {
      subsystems.push(checkboxes[i].textContent);
    }

    if (action != 'status') { // Only status is considered "safe"
      let sendRequest = this._sendRequests.bind(this);
      const inRun = this.triggerState && this.triggerState.toLowerCase() === 'running';
      let warning = {
        'type': inRun ? 'stern-confirmation' : 'confirmation',
        'header': 'WARNING',
        'message': inRun ? 'THIS IS (very probably) NOT WHAT YOU WANTED! CMS is in run! action \'' + action.toUpperCase() + '\' requires the run to be stopped! Are you sure you want to proceed?' :
          '\'' + action.toUpperCase() + '\' is an expert operation. Please check with the L1 DOC before proceeding. Are you sure you want to proceed?',
        'blocking': true,
        'options': ['proceed', 'cancel'],
        'callback': function callback(response) {
          if (response.action === 'cancel') {
            console.log('Sending request', action, 'canceled');
          } else {
            console.log('Sending request', action);
            sendRequest(action, subsystems);
          }
        }
      };
      this.throwToast(warning);
    } else {
      this._sendRequests(action, subsystems);
    }
  }

  _sendRequests(action, subsystems) {
    this.loading = true;
    var promises = subsystems.map( function(subsystemName) {
      return new Promise(function(jep) {
        var ajax = document.createElement('iron-ajax');
        ajax.method = "post";
        ajax.url = "/api/v0/subsystems/" + subsystemName + "/control";
        ajax.body = "action=" + action;
        ajax.addEventListener('response', function(event) {
          jep({
            subsystem: subsystemName,
            success: true,
            message: event.detail.response.join("\n")
          });
        });
        ajax.addEventListener('error', function(event) {
          jep({
            subsystem: subsystemName,
            success: false,
            message: event.detail.request.response.message
          });
        });
        ajax.generateRequest();
      });
    });

    Promise.all(promises)
    .then( function(results) {
      this.loading = false;
      this.$.resultbox.innerHTML = results.reduce( function(result, item) {
        var s = item.success ? "<div success>" : "<div fail>";
        s += "<h3>" + item.subsystem + "</h3><p>" + item.message + "</p></div>";
        return result + s;
      }, "");
      this._notifyResize();
    }.bind(this))
    .catch( function(error) {
      this.loading = false;
      this.throwToast({
        type: 'error',
        blocking: true,
        message: error.message || error
      });
    }.bind(this))
  }

  _selectAll() {
    var checkboxes = this.$.subsystems.querySelectorAll('paper-checkbox:not([checked])');
    checkboxes.forEach( checkbox => checkbox.checked = true );
  }

  _selectNone() {
    var checkboxes = this.$.subsystems.querySelectorAll('paper-checkbox[checked]');
    checkboxes.forEach( checkbox => checkbox.checked = false );
  }
}
customElements.define(ProcessController.is, ProcessController);
