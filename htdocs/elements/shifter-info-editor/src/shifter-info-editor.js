class ShifterInfoEditor extends AuthEnabled(WhenLifeGivesYouLemonsMakeToast(Polymer.Element)) {
  static get is() { return 'shifter-info-editor' }
  static get properties() {
    return {
      newShifterInfoSelectedType: {
        type: String,
        value: "any"
      },
      newShifterInfoSelectedSection: {
        type: String,
        value: "general"
      },
      newShifterInfoDaysValid: {
        type: Number,
        value: 0
      },
      newShifterInfoLoading: {
        type: Boolean,
        value: false
      },
      newShifterInfoMessage: {
        type: String,
        value: "<p>type your new message here</p>"
      },
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
    this._editor = $("#wysiwyg");
    this._editor.trumbowyg('html', this.newShifterInfoMessage);
    this._editor.on('tbwchange', function(event) {
      this.newShifterInfoMessage = this._editor[0].innerHTML;
    }.bind(this));
  }

  _widgetActive(desiredPath, urlData) {
    var hit = urlData.activeWidget == desiredPath;
    return hit;
  }

  _wysiwygValidInput(isLoggedIn, newShifterInfoSelectedType, newShifterInfoSelectedSection, newShifterInfoMessage, newShifterInfoLoading) {
    return isLoggedIn &&
           newShifterInfoSelectedType &&
           newShifterInfoSelectedSection &&
           newShifterInfoMessage &&
           newShifterInfoMessage != "" &&
           newShifterInfoMessage != "<p><br></p>" &&
           newShifterInfoMessage != "<p>type your new message here</p>" &&
           !newShifterInfoLoading;
  }

  _submitNewShifterInfo() {
    var isEditing = this._isEditing(this.urlDataTail.path);
    var ajax = document.createElement('l1page-api');
    this.root.appendChild(ajax);
    ajax.url = "/api/v0/shifter-information";
    if (isEditing) {
      ajax.url += this.urlDataTail.path;
    }
    ajax.method = isEditing ? "PUT" : "POST";
    ajax.parameters = [
      "type",
      "section",
      "message",
      "days_valid"
    ];
    ajax.type = this.newShifterInfoSelectedType;
    ajax.section = this.newShifterInfoSelectedSection;
    ajax.message = this.newShifterInfoMessage;
    ajax.days_valid = this.newShifterInfoDaysValid;

    ajax.addEventListener('response', function onNewShifterInfoResponse() {
      this.newShifterInfoLoading = false;
      this._closeActiveWidget();
      this.throwToast({
        type: 'info',
        message: "new shifter info entry inserted"
      });
    }.bind(this));
    ajax.addEventListener('error', function onNewShifterInfoError(event) {
      this.newShifterInfoLoading = false;
      this.throwToast({
        type: 'error',
        message: "an error occured while inserting new shifter info: " + event.detail.response.message || event.detail.response
      });
    }.bind(this));
    this.newShifterInfoLoading = true;
    ajax.generateRequest();
  }

  _closeActiveWidget() {
    this.set("urlData.activeWidget", "");
  }

  _isEditing(path) {
    return path && path != "" && !!path.match(/\/([0-9]*)/);
  }

  _getID(path) {
    return path.match(/\/([0-9]*)/)[1];
  }

  _getSubmitMessage(path) {
    return this._isEditing(path) ? "Edit" : "Submit";
  }
}
customElements.define(ShifterInfoEditor.is, ShifterInfoEditor);
