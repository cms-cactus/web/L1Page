class ShifterInfoEntry extends Nebula.DispatchBehavior(AuthEnabled(WhenLifeGivesYouLemonsMakeToast(Polymer.Element))) {
  static get is() { return 'shifter-info-entry' }
  static get properties() {
    return {
      entry: Object,
      _deleting: {
        type: Boolean,
        value: false
      },

      new: {
        type: Boolean,
        computed: "_isNew(entry, _refresher)",
        reflectToAttribute: true
      },

      _refresher: {
        type: Number,
        value: 0
      }
    };
  }
  static get observers() {
    return [
      "_renderMessage(entry)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }
  disconnectedCallback() {
    super.disconnectedCallback();
    clearTimeout(this._refresherTimeout);
  }

  _isNew(entry, _refresher) {
    var diff = new Date(entry.TIMESTAMP) - (new Date() - 1000*60*60*24*2); // 2 days
    if (diff > 0) {
      clearTimeout(this._refresherTimeout);
      this._refresherTimeout = setTimeout(function () {
        this._refresher = 1;
      }.bind(this), diff + 100); // add some buffer time to make sure it is no longer considered new
    }
    return diff > 0;
  }

  _renderMessage(entry) {
    this.$.message_container.innerHTML = entry.MESSAGE;
  }

  _canDelete(entry, userInfo) {
    if (!entry || !userInfo) {
      return;
    }
    var isAuthor = entry.AUTHOR == userInfo.name;
    var isAdmin = ShifterInfoEntry.egroup_admins.some( function(adminGroup) {
      return userInfo["e-groups"] && userInfo["e-groups"].indexOf(adminGroup) != -1;
    });
    return isAuthor || isAdmin;
  }

  delete() {
    this.throwToast({
      blocking: true,
      type: 'warning',
      message: "Are you sure you want to permanently delete this message?<table><tr><td>Subsystem: </td><td>" + this.entry.SUBSYSTEM + "</td></tr><tr><td>Author: </td><td>" + this.entry.AUTHOR + "<td></tr><tr><td>created at: </td><td>" + this.entry.TIMESTAMP + "</td></tr>",
      options: ["oops", "yes"],
      callback: function(response) {
        if (response.action == "yes") {
          var ajax = document.createElement('l1page-api');
          this.root.appendChild(ajax);
          ajax.url = "/api/v0/shifter-information/" + this.entry.ID;
          ajax.method = "DELETE";

          ajax.addEventListener('response', function onNewShifterInfoResponse() {
            this._deleting = false;
            this.throwToast({
              type: 'info',
              message: "shifter info entry deleted"
            });
          }.bind(this));
          ajax.addEventListener('error', function onNewShifterInfoError(event) {
            this._deleting = false;
            this.throwToast({
              type: 'error',
              message: "an error occured while deleting shifter info entry: " + event.detail.response.message || event.detail.response
            });
          }.bind(this));
          this._deleting = true;
          ajax.generateRequest();
        }
      }.bind(this)
    })
  }

  edit() {
    $("#wysiwyg").trumbowyg('html', this.entry.MESSAGE);
    var editor = document.querySelector('shifter-info-editor');
    editor.newShifterInfoSelectedSection = this.entry.SECTION;
    editor.newShifterInfoSelectedType = this.entry.TYPE;
    this.dispatchAction('set_location', "/new-shifter-info/" + this.entry.ID);
  }
}
customElements.define(ShifterInfoEntry.is, ShifterInfoEntry);
