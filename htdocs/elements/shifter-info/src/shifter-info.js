class ShifterInfo extends Polymer.Element {
  static get is() { return 'shifter-info' }
  static get properties() {
    return {
      shifterInfo: Object,
      filterParams: Object,
      runData: Object,
      sortingPreference: {
        type: String,
        value: "section"
      },
      selectedType: {
        type: String,
        value: "any"
      },
      selectedSection: {
        type: String,
        value: "general"
      },
      searchValue: {
        type: String,
        value: ""
      }
    };
  }
  static get observers() {
    return [
      "_refreshFilterParameters(selectedType, selectedSection)",
      "_renderFilterOptions(shifterInfo.rows)",
      "_updateSelectedType(runData.HLTModeId)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _updateSelectedType(HLTMode) {
    if (HLTMode.indexOf("cosmics") != -1) {
      this.selectedType = "cosmics";
    }
    else if (HLTMode.indexOf("collisions") != -1) {
      this.selectedType = "collisions"
    }
  }

  _renderFilterOptions(shifterInfoRows) {
    var types = shifterInfoRows.reduce((types, row) => {
      types[row.TYPE] = true;
      return types;
    }, {});
    var sections = shifterInfoRows.reduce((sections, row) => {
      sections[row.SECTION] = true;
      return sections;
    }, {});

    var typesContainer = this.$.types.querySelector('paper-listbox');
    typesContainer.innerHTML = "";
    Object.keys(types).forEach( type => {
      var option = document.createElement('paper-item');
      option.setAttribute('name', type);
      option.textContent = type[0].toUpperCase() + type.substring(1);
      typesContainer.appendChild(option);
    });

    var sectionsContainer = this.$.sections.querySelector('paper-listbox');
    sectionsContainer.innerHTML = "";
    Object.keys(sections).forEach( section => {
      var option = document.createElement('paper-item');
      option.setAttribute('name', section);
      option.textContent = section[0].toUpperCase() + section.substring(1);
      sectionsContainer.appendChild(option);
    });
    this._refreshFilterParameters();
  }

  _nodeListToArray(nodelist) {
    var result = [];
    for (var i = 0; i < nodelist.length; i++) {
      result.push(nodelist[i].textContent);
    }
    return result;
  }
  _refreshFilterParameters() {
    this.filterParams = {
      type: this.selectedType,
      section: this.selectedSection
    }
  }
  _getFilter(filterParams, searchValue) {
    if (!filterParams) { return };
    var filter_type = filterParams.type != "any";
    var filter_section = filterParams.section != "general";
    return function(item) {
      var type_matches = filter_type ? filterParams.type == item.TYPE || item.TYPE == "any" : true;
      var section_matches = filter_section ? filterParams.section == item.SECTION || item.SECTION == "general" : true;
      var search_matches = searchValue ? item.MESSAGE.toLowerCase().indexOf(searchValue.toLowerCase()) != -1 : true;
      return type_matches && section_matches && search_matches;
    }
  }

  _getSorter(sortingPreference) {
    return sortingPreference == "date" ? dateSorter : sectionSorter;

    function dateSorter(a, b) {
      if (a.TIMESTAMP == b.TIMESTAMP) { return 0 }
      var date_a = new Date(a.TIMESTAMP);
      var date_b = new Date(b.TIMESTAMP);
      return date_a < date_b ? 1 : -1;
    }

    function sectionSorter(a, b) {
      if (a.SECTION == b.SECTION) { return dateSorter(a, b) }
      return a.SECTION < b.SECTION ? -1 : 1;
    }
  }

  _getSeachIcon(searchValue) {
    return searchValue == "" ? "search" : "clear";
  }

  clearSearch() {
    this.searchValue = "";
  }

  _scrollDown() {
    // window.scrollTo(0,document.body.scrollHeight);
  }
}
customElements.define(ShifterInfo.is, ShifterInfo);
