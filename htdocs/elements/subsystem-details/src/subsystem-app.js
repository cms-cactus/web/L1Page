class SubsystemApp extends Polymer.Element {
  static get is() { return 'subsystem-app' }
  static get properties() {
    return {
      appData: Object
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _getOperationMessage(operation) {
    if (operation.result && operation.warningMessage) {
      return operation.result + ". " + operation.warningMessage;
    }
    else if (operation.result || operation.warningMessage) {
      return operation.result + operation.warningMessage;
    }
    else {
      return "no message";
    }
  }

  _num2severity(level) {
    return level == 0 ? "INFO" : level == 1 ? "WARNING" : "ERROR";
  }

  _isUnhealthy(appData) {
    return appData.healthy == false;
  }

  _isCritical(appData){
    return appData.critical;
  }

  _healthSummary(appData) {
    return (appData.healthy == false ? "dead! " : "healthy: ") + (appData.healthMessage ? appData.healthMessage : "unmonitored, assumed healthy");
  }

  _healthIcon(appData) {
    return appData.healthy == false ? "report" : "check";
  }
}
customElements.define(SubsystemApp.is, SubsystemApp);
