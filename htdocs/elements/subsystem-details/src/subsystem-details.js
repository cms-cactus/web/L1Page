class SubsystemDetails extends WhenLifeGivesYouLemonsMakeToast(Polymer.Element) {
  static get is() { return 'subsystem-details' }
  static get properties() {
    return {
      includedSubsystems: Array,
      subsystemData: Object,
      route: Object,
      urlData: Object,
      triggerState: String,

      _subsystem: {
        computed: "_getSubsystem(subsystemData, urlData.selectedSubsystem)"
      },
      _apps: {
        computed: "_getApps(_subsystem)"
      },
      _applicationStatus: {
        type: Number,
        computed: "_getAppsStatus(_subsystem)"
      },
      _alarmStatus: {
        type: Number,
        computed: "_getAlarmsStatus(_subsystem)"
      },
      _operationStatus: {
        type: Number,
        computed: "_getOperationsStatus(_subsystem)"
      },

      _actionBusy: {
        type: Boolean,
        value: false
      }
    };
  }
  static get observers() {
    return [
      // "_renderInfo(subsystemData, urlData.selectedSubsystem)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _inRunText(selectedSubsystem, includedSubsystems) {
    if (!selectedSubsystem || !includedSubsystems) {
      return;
    }
    if (includedSubsystems.indexOf(selectedSubsystem) == -1) {
      return "not in run";
    }
    else {
      return "in run";
    }
  }

  _getSubsystem(subsystemData, selectedSubsystem) {
    if (!subsystemData || !selectedSubsystem) {
      return;
    }
    if (!subsystemData[selectedSubsystem]) {
      return {};
    }
    return subsystemData[selectedSubsystem];
  }

  _getApps(subsystem) {
    if (!subsystem.apps) {
      return [];
    }
    //map() method creates a new array with the results of calling a function for every array element
    return Object.keys(subsystem.apps).map( function(appName) {
      var app = subsystem.apps[appName];
      //exchange data to/from a web server, the data has to be a string
      var result = JSON.parse(JSON.stringify(app));
      result.name = app.label;
      return result;
      // return Object.assign(subsystem[appName], {name: appName});
    })
  }

  _getAppsLink(subsystem){
    if(subsystem.apps["SUPERVISOR"]) {
      var app = subsystem.apps["SUPERVISOR"];
    }
    else if(subsystem.apps["API"]){
      var app = subsystem.apps["API"];
    }
    return app.uri;
  }

  _showAppsButton(subsystem){
    return (subsystem.name != "XDAQ_Services" && subsystem.apps);
  }

  _getAppsStatus(subsystem) {
    if (!subsystem.apps) {
      return -1;
    }
    return Object.keys(subsystem.apps).reduce( function(status, appName) {
      var app = subsystem.apps[appName];
      var s = app.healthy == false ? app.critical ? 2 : 1 : 0;
      return status > s ? status : s;
    }, 0);
  }

  _getAlarmsStatus(subsystem) {
    if (!subsystem.apps) {
      return -1;
    }
    return Object.keys(subsystem.apps).reduce( function(status, appName) {
      var app = subsystem.apps[appName];
      var severity = app.alarms.reduce( function(severity, alarm) {
        var s = alarm.severity == "ERROR" ? 2 : alarm.severity == "WARNING" ? 1 : 0;
        return severity > s ? severity : s;
      }, 0);
      return status > severity ? status : severity;
    }, 0);
  }

  _getOperationsStatus(subsystem) {
    if (!subsystem.apps) {
      return -1;
    }
    return Object.keys(subsystem.apps).reduce( function(status, appName) {
      var app = subsystem.apps[appName];
      var severity = (app.operations || []).reduce( function(severity, operation) {
        var s = operation.severity == "ERROR" ? 2 : operation.severity == "WARNING" ? 1 : 0;
        return severity > s ? severity : s;
      }, 0);
      return status > severity ? status : severity;
    }, 0);
  }

  _alarmSummary(subsystem) {
    if (!subsystem.apps) {
      return "alarm status unknown";
    }
    var count = Object.keys(subsystem.apps).reduce( function(count, appName) {
      var app = subsystem.apps[appName];
      return count + (app.alarms ? app.alarms.length : 0);
    }, 0);
    return (count == 0 ? "no" : count )+ (count == 1 ? " alarm" : " alarms");
  }

  _operationSummary(subsystem) {
    if (!subsystem.apps) {
      return "operation status unknown";
    }
    var count = Object.keys(subsystem.apps).reduce( function(count, appName) {
      var app = subsystem.apps[appName];
      return count + (app.operations ? app.operations.length : 0);
    }, 0);
    return (count == 0 ? "no" : count )+ (count == 1 ? " operation" : " operations");
  }

  _applicationSummary(_applicationStatus) {
    if (_applicationStatus == -1) {
      return "application status unknown"
    }
    else if (_applicationStatus == 2) {
      return "critical application offline";
    }
    else if (_applicationStatus == 1) {
      return "non-critical application offline";
    }
    else {
      return "all applications online";
    }
  }

  _isDangerous(action) {
    return action != "status";
  }

  //put TSTORE APP at last and SWATCH cells first
  //If compareFunction(a, b) is less than 0, sort a to a lower index than b, i.e. a comes first.
  //If compareFunction(a, b) returns 0, leave a and b unchanged with respect to each other
  _sort(a,b) {
  //_sort(a) {
    var nameA = a.name;
    var nameB = b.name;
    if (nameA == "TSTORE"){
      return 1;
    }
    if (nameA.indexOf("SWATCH") == -1){
      return -1;
    }

    return 0;
  }

  _isEqual(first,second){
    return first == second;
  }

  _nothingToSee(_appsLength, _subsystemActions) {
    return !_appsLength && !_subsystemActions;
  }

  _executeAction(event) {
    if (this._isDangerous(event.model.action)) {
      let sendRequest = this._executeSendRequest.bind(this);
      let inRun = this._inRunText(this.urlData.selectedSubsystem, this.includedSubsystems) === 'in run';
      let running = this.triggerState && this.triggerState.toLowerCase() === 'running';
      let action = event.model.action.toUpperCase();
      let warning = {
        'type': inRun && running ? 'stern-confirmation' : 'confirmation',
        'header': 'WARNING',
        'message': inRun && running ? 'THIS IS (very probably) NOT WHAT YOU WANTED! CMS is in run! \'' + action + '\' requires the run to be stopped! Are you sure you want to proceed?' :
          '\'' + action + '\' is an expert operation. Please check with the L1 DOC before proceeding. Are you sure you want to proceed?',
        'options': ['proceed', 'cancel'],
        'blocking': true,
        'callback': function callback(response) {
          if (response.action === 'cancel') {
            console.log('Sending request', event.model.action, 'canceled');
          } else {
            console.log('Sending request', event.model.action);
            sendRequest(event);
          }
        }
      };
      this.throwToast(warning);
    } else {
      this._executeSendRequest(event);
    }
  }

  _executeSendRequest(event) {
    this._actionBusy = true;
    var ajax = document.createElement('iron-ajax');
    ajax.method = "post";
    ajax.url = "/api/v0/subsystems/" + this.urlData.selectedSubsystem + "/control";
    ajax.body = "action=" + event.model.action;
    var process_result = this.root.querySelector("#process_result");
    ajax.addEventListener('response', function(event) {
      this._actionBusy = false;
      console.log(event.detail.response.join("\n"));
      process_result.innerHTML = event.detail.response.join("\n");
    }.bind(this));
    ajax.addEventListener('error', function(event) {
      this._actionBusy = false;
      console.log(event.detail.request.response.message);
      this.throwToast({
        'type': 'error',
        'message': event.detail.request.response.message,
        'blocking': false
      });
      process_result.innerHTML = "";
    }.bind(this));
    ajax.generateRequest();
  }

  _displayApp(appData, appsToKeep) {
    if (appData.operations.length) {
      return true;
    }
    for (let appName of appsToKeep) {
      if (_isEqual(appName, appData.name)) {
        return true;
      }
    }
    return false;
  }
}
customElements.define(SubsystemDetails.is, SubsystemDetails);
