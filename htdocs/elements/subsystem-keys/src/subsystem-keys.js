class SubsystemKeys extends Polymer.Element {
  static get is() { return 'subsystem-keys' }
  static get properties() {
    return {
      runData: Object
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  // _toArray(Keys) {
  //   return Object.keys(Keys).map(function(key) {
  //     var result = {};
  //     result.name = key;
  //     result.value = Keys[key];
  //     return result;
  //   });
  // }

  _toArray(trg_subsystems) {
    return Object.keys(trg_subsystems.conf).map((key) => {
      return {
        name: key,
        conf: trg_subsystems.conf[key],
        rs: trg_subsystems.rs[key]
      };
    });
  }
}
customElements.define(SubsystemKeys.is, SubsystemKeys);
