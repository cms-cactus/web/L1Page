class SuggestionEntry extends Nebula.DispatchBehavior(Polymer.Element) {
  static get is() { return 'suggestion-entry' }
  static get properties() {
    return {
      entry: Object,
      _parsedEntry: {
        type: Object,
        computed: "_parseEntry(entry)"
      }
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  _parseEntry(entry) {
    if (typeof entry == "string") {
      return {
        message: entry
      }
    }
    else {
      entry.links = entry.links ? Object.keys(entry.links).map( function(linkName) {
        return {
          name: linkName,
          url: entry.links[linkName]
        }
      }) : [];
      entry.actions = entry.actions ? Object.keys(entry.actions).map( function(actionName) {
        var action = entry.actions[actionName];
        return {
          name: actionName,
          url: action.url,
          method: action.method || "GET",
          parameters: action.parameters || {}
        }
      }) : [];
      return entry;
    }
  }

  _followLink(event) {
    console.log("_followLink", event);
    var url = event.model.link.url;
    if (url.indexOf('/') != 0) {
      // external URL
      window.open(url);
    }
    else {
      this.dispatchAction('set_location', url);
    }

  }

  // _doAction(event) {
  //   console.log("_doAction", event);
  // }
}
customElements.define(SuggestionEntry.is, SuggestionEntry);
