class SuggestionList extends Polymer.Element {
  static get is() { return 'suggestion-list' }
  static get properties() {
    return {
      suggestions: Object,
      triggerState: String
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  isInRun(triggerState) {
    return triggerState == "Running";
  }

  _isEmpty(logsLength, triggerState) {
    return logsLength == 0 || !this.isInRun(triggerState);
  }
}
customElements.define(SuggestionList.is, SuggestionList);
