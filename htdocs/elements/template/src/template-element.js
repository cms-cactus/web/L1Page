/**
 * Fired when `template-element` changes its awesomeness level.
 *
 * @event awesome-change
 * @param {number} newAwesome New level of awesomeness.
 */

class TemplateElement extends Polymer.Element {
  static get is() { return 'template-element' }
  static get properties() {
    return {
      /**
       * Metadata describing what has been made awesome on the page.
       *
       * @type {{elements: Array<HTMLElement>, level: number}}
       */
      someObject: {
        type: Object,
        value: function() {
          return {
            name: 'deinonychus',
            image: 'http://emilywilloughby.com/gallery-data/images/full/deinonychus-family.jpg'
          }
        }
      }
    };
  }
  static get observers() {
    return [];
  }
  constructor() {
    super();
    // instance is instantiated
  }
  connectedCallback() {
    super.connectedCallback();
    // instance is attached to the DOM
  }

  /**
   * this is an example description of a documented function
   * @param {Egg} egg The dinosaur egg.
   * @return {Dinosaur}
   */
  makeDinosaur(egg) {
    return new Dinosaur(egg);
  }
}
customElements.define(TemplateElement.is, TemplateElement);
