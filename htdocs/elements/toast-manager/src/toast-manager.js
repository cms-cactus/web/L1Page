class ToastManager extends Polymer.Element {
  static get is() { return 'toast-manager' }
  static get properties() {
    return {
      /**
       * The current toast
       */
      _toast: {
        type: Object,
        value: function() { return {} }
      },
      _toastHideDelay: Object,
      _toastTimer: Object,
      /**
       * The duration in milliseconds to show the toast.
       */
      duration: {
        type: Number,
        value: 10000
      },
      _stack: {
        type: Array,
        value: function() {return [];}
      },
      /**
       * An Array of past toasts, sorted from most recent
       */
      log: {
        type: Array,
        value: function() {return [];}
      }
    };
  }
  static get observers() {
    return [
      '_stackChanged(_stack.splices)'
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
    if (typeof toaster === "undefined") {
      window.toaster = this;
    } else {
      console.error(this, "Trying to register as toaster, but another is already registerd", toaster);
    }
  }

  _inputDisabled(input, input_invalid) {
    return input && input_invalid;
  }

  _dialogButtonClick(event, details) {
    this.log[0].answer = event.model.option;
    this._toast.callback({
      action: event.model.option,
      input: this._toast.input && this._toast.input.value
    });
    this._toast = {};
    this.$.modal.close();
  }

  _toastButtonClick(event, details) {
    clearTimeout(this._toastTimer);
    this.log[0].answer = event.model.option;
    this._toast.callback({
      action: event.model.option
    });
    this.$.toast.hide();
    this._toastHideDelay = setTimeout(function () {
      this._toast = {};
      this._stackChanged();
    }.bind(this), 200);
  }

  /**
   * Generate a notification based on the given toast.
   * The `Toast` object accepts the following properties:<br>
   * type (String): Can be 'info', 'warning', or 'error'.<br>
   * header (String) (optional): Can be used to set a message to the header of the toast. If not specified the type property will be used as header. <br>
   * message (String | HTMLElement): The actual message to show.<br>
   * options (Array of Strings) (optional): Options presented to the user. The chosen option is returned.
   * blocking (optional): This will force the user to stop what they're
   * doing and choose one of the supplied options.
   * callback (function): The function to execute when an option has been selected or a timeout has passed.
   *
   * @param {Toast} toast The toast object.
   */
  throwToast(toast, sender) {
    if (!toast.options) {
      toast.options = ["ok"];
      if (!toast.callback) {
        toast.callback = function() {};
      }
    }
    if (!toast.header) {
      toast.header = toast.type;
    }
    toast.sender = sender;
    toast.timestamp = new Date();
    this.push('_stack', toast);
    // this._stack.push(toast);
  }

  _oldToastIsHigherPriority() {
    var t_current = this._toast.type;
    var t_new = this._stack[0].type;

    var p_current = t_current == "error" ? 2 : t_current == "warning" ? 1 : 0;
    var p_new = t_new == "error" ? 2 : t_new == "warning" ? 1 : 0;

    return p_current > p_new;
  }

  _stackChanged() {

    // if we have something new to show && the current one is just a toast && current toast is not higher priority
    // otherwise it will just remain in the to-do stack
    if(this._stack.length > 0 && !(this._toast.blocking) && !this._oldToastIsHigherPriority() ) {
      clearTimeout(this._toastHideDelay);

      // if a toast is still active, we need to hide it first
      if (this.$.toast.opened) {
        clearTimeout(this._toastTimer);
        this.$.toast.hide();
        this._toastTimeout();
        // give the toast time to hide before loading the new one
        this._toastHideDelay = setTimeout(function () {
          this._nextToast();
        }.bind(this), 200);
      // otherwise we just show the next one
      } else {
        this._nextToast();
      }
    }
  }

  _toastTimeout() {
    if (this._toast.callback) {
      this._toast.callback(null);
    }
  }

  _nextToast() {
    this._toast = this._stack[0];
    if (this._toast && this._toast.type && this._toast.message) {
      this.unshift('log', this._toast);
      // this.log.unshift(this._toast);

      if (this._toast.options && !(this._toast.callback) ) {
        console.error(this._toast, "toast has options but provides no callback");
        this._toast.callback = function() {console.error("no callback was specified for this toast")};
      }

      if (this._toast.blocking) {
        // show a dialog
        this.$.messagecontainer.innerHTML = this._toast.message;
        this.$.modal.open();
      } else {
        // show a toast
        this.$.toast.show();
        this._toastTimer = setTimeout(function () {
          this._toastTimeout();
          this._toastHideDelay = setTimeout(function () {
            this._toast = {};
            this._stackChanged();
          }.bind(this), 200);
          this._stackChanged();
        }.bind(this), this.duration);
      }
    } else {
      console.error(this, "received an invalid toast: ", this._toast);
      this._toast = {};
    }
    this.shift('_stack');
  }

  _modalClosed() {
    this._stackChanged();
  }

  /**
   * This will output the entire log to the console.
   */
  showLog() {
    console.log('showing toaster logs');
    for (var i = 0; i < this.log.length; i++) {
      var toast = this.log[i];

      var output = toast.message + "\n";
      if (toast.answer) {
        output += "-> user responded: " + toast.answer + "\n";
      }
      output += "-> received at " + toast.timestamp + " by ";

      if (toast.type == "info") {
        console.info(output, toast.sender);
      } else if (toast.type == "warning") {
        console.warn(output, toast.sender);
      } else if (toast.type == "error") {
        console.error(output, toast.sender);
      } else {
        console.log(output, toast.sender);
      }
    }
  }

  _showTextArea(input) {
    return input && input.type == "textarea";
  }
  _showInput(input) {
    return input && input.type != "textarea";
  }

}
customElements.define(ToastManager.is, ToastManager);
