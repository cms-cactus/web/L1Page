class TriggerRates extends WhenLifeGivesYouLemonsMakeToast(Polymer.Element) {
  static get is() { return 'trigger-rates' }
  static get properties() {
    return {
      route: Object,
      urlData: Object,
      runData: Object,
      globalRatesData: Object,
      muonRatesData: Object,
      caloRatesData: Object,
      thresholdData: Object,
      route: Object,

      // toggle for global rates
      sortGlobalByName: {
        type: Boolean,
        value() { return false; }
      },

      // filtering for global rates
      filterGlobalRates: String,

      // The order of the subsystems here defines the order in which they are shown in the panel
      muonSubsystems: {
        type: Array,
        value() {
          return [
            ['sorter', 'Total'],
            ['EMTF+'],
            ['OMTF+'],
            ['BMTF'],
            ['OMTF-'],
            ['EMTF-']
          ];
        }
      }
    };
  }
  static get observers() {
    return [
      // "_renderInfo(subsystemData, urlData.selectedSubsystem)"
    ];
  }
  constructor() {
    super();
  }
  connectedCallback() {
    super.connectedCallback();
  }

  isLoaded(ratesData) {
    //console.error( Array.isArray(globalRatesData), globalRatesData && globalRatesData.length>0, globalRatesData );
    // return ratesData && ratesData.length>0;
    return ratesData && ratesData.lumisection && ratesData.rates.length > 0;
  }

  isInRun(triggerState) {
    return triggerState == "Running";
  }

  _isEmpty(ratesData, triggerState) {
    return ( !ratesData || ratesData.rates.length == 0 ) || !this.isInRun(triggerState);
  }

  backToHome() {
    this.set( "route.path", "/");
  }

  getValues( ratepair ) {
    console.log( ratepair.NAME, ratepair,RATE );
    return ratepair.NAME;
  }

  _setSortName() {
    this.sortGlobalByName = true;
  }

  _setSortRate() {
    this.sortGlobalByName = false;
  }

  _filterTriggerName(filterString) {
    let filterRgx = undefined;
    if (filterString) {
      try {
        filterRgx = new RegExp(filterString, "i"); // match case insensitive
      } catch (e) {
        console.log('Cannot construct Regex from input: ', filterString);
      }
    }
    return function(triggerName) {
      if (!filterRgx) return true;
      if (!triggerName) return false;
      return triggerName.NAME.match(filterRgx) != null;
    };
  }

  _sortGlobal(sortByName) {
    if (sortByName) {
      return function(triggerA, triggerB) {
        if (triggerA.NAME < triggerB.NAME) return -1;
        if (triggerB.NAME < triggerA.NAME) return 1;
        return 0;
      };
    } else {
      // sort rates using the post prescale rate
      const sortRate = 'post-PS, pre-DT';
      return function(triggerA, triggerB) {
        let rateA = triggerA[sortRate] ? triggerA[sortRate] : 0.0;
        let rateB = triggerB[sortRate] ? triggerB[sortRate] : 0.0;
        // reverse sorting here to have highest rate on top
        if (rateA > rateB) return -1;
        if (rateA < rateB) return 1;
        return 0;
      };
    }
  }

  _displaySystem(system, lumisection) {
    return `${system} at LS ${lumisection}`;
  }

  _displayRate(rate){
    return rate ? rate.toFixed(0) : 0;//number of decimals
  }

  _displayRateGlobal(rate, type) {
    if (rate && rate[type]) {
      return rate[type].toFixed(0);
    }
    return 0;
  }

  _displayName(name){
    return name.replace("rate [Hz]", "");
  }

  _displayLS(lumisection) {
    return `LS ${lumisection}`;
  }

  _subsystemFilter(subsystems) {
    return function(trigger) {
      // if any of the trigger matches any of the subsystems return true
      return subsystems.some(function(subsystem) {
        return (trigger.NAME.indexOf(subsystem) != -1);
      });
    };
  }

  _displaySubsystemName(subsystems) {
    // this works for now, but probably has to be refined at some point
    // the sorter and Total rates go the top currently and don't need their own header
    return subsystems.length == 1;
  }

  _passGlobalThreshold(rate,thresholdData){
    if ( ! thresholdData || ! thresholdData.general ) {
      return 0;
    } else if (rate > thresholdData.general.high){
      return 1;
    } else {
      return 0;
    }
  }

  // core functionality to check for specific rate thresholds with fallback to general rates
  _checkThreshold(rate, thresholdData) {
    // check if we have thresholdData available, and only then do the work
    if (!thresholdData) return 0;

    let thresholdHigh = undefined;
    let thresholdLow = undefined;
    // first check if there is a general threshold that can be used as fall back
    if (thresholdData.general) {
      thresholdHigh = thresholdData.general.high;
      thresholdLow = thresholdData.general.low;
    }
    // if there is a specific threshold present, use that
    let trigger = rate.NAME;
    if (thresholdData.specific && trigger in thresholdData.specific) {
      thresholdHigh = thresholdData.specific[trigger].high;
      thresholdLow = thresholdData.specific[trigger].low;
    }

    if (!thresholdLow || !thresholdHigh) {
      return 0;
    } else if (rate.RATE > thresholdHigh) {
      return 1;
    } else if (rate.RATE < thresholdLow) {
      return 2;
    }

    return 0;
  }

  _passThreshold(rate,thresholdData){
    if ( ! thresholdData || ! thresholdData.general ) {
      return 0;
    } else if (rate > thresholdData.general.high){
      return 1;
    } else if (rate < thresholdData.general.low) {
      return 2;
    } else {
      return 0;
    }
  }

  _hasLowTriggerRate(RatesData,thresholdData) {
    var status=false;
    if ( !RatesData  || !thresholdData || !thresholdData.general ) {
      return false;
    }
    return Object.keys(RatesData).reduce(function(status, triggerName) {
      var trigger = RatesData[triggerName];
      if (trigger.RATE < thresholdData.general.low){
        status=true;
      }
      return status;
    },false);
  }

  _hasHighTriggerRate(RatesData, thresholdData) {
    var status=false;
    if ( !RatesData  || !thresholdData || !thresholdData.general ) {
      return false;
    }
    return Object.keys(RatesData).reduce(function(status, triggerName) {
      var trigger = RatesData[triggerName];
      if (trigger.RATE > thresholdData.general.high){
        status=true;
      }
      return status;
    },false);
  }

  // For sorting the muon rates in the individual sub panels
  // NOTE: Since this function is only called when the sorting actually changes it can be a bit more expensive to call compared to the actual sorting functions that it returns
  _sortMuon(subsystems) {
    // TODO: currently can't do this on the html side, but should be possible, respectively in the end
    // we want this to be switchable from page, right now it is hardcoded
    let desiredSort = this._getMuonOrdering(subsystems);

    if (desiredSort === 'muon total rates') {
      return function(triggerA, triggerB) {
        let desiredOrderMuonRates = [
          'Final sorter',
          'Total input',
          'Endcap+',
          'Overlap+',
          'Barrel',
          'Overlap',
          'Endcap-'
        ];

        let a = desiredOrderMuonRates.findIndex( (k) => { return triggerA.NAME.indexOf(k) != -1; });
        let b = desiredOrderMuonRates.findIndex( (k) => { return triggerB.NAME.indexOf(k) != -1; });

        // if there is no match in the desiredSort for the given trigger, sort it all to the end
        if (a == -1) { a = Infinity; }
        if (b == -1) { b = Infinity; }

        if (a < b) return -1;
        if (b < a) return 1;
        return 0;
      };
    }

    if (desiredSort === 'lexicographical') {
      // currently just do a lexicographical sort (without taking into account numbers correctly)
      return function(triggerA, triggerB) {
        if (triggerA.NAME < triggerB.NAME) return -1;
        if (triggerB.NAME < triggerA.NAME) return 1;
        return 0;
      };
    }

    // leave ordering untouched if nothing above matches
    return function(triggerA, triggerB) {
      return 0;
    };
  }


  _getMuonOrdering(subsystems) {
    // for first panel with total and sorter rates we have defined a desired ordering
    if (JSON.stringify(subsystems) === JSON.stringify(['sorter', 'Total'])) {
      return 'muon total rates';
    }

    // the rest should just be ordered lexicographically at the moment
    return 'lexicographical';
  }
}
customElements.define(TriggerRates.is, TriggerRates);
