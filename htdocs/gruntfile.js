var fs = require('fs-extra');
var path = require('path');
var swPrecache = require('sw-precache');

var elements = fs.readdirSync("elements").filter(file => file.match(/^[a-z1-9]*-[a-z1-9\-]*$/gi));
console.log("elements:", elements);

module.exports = function(grunt) {

  grunt.initConfig({
    import: {
      options: {
        indent: true
      },
      sass: {
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/src/*-*.scss`),
          rename: (dest, src) => src.replace('src', '_build')
        }]
      },
      css: {
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/src/*-*.css`),
          rename: (dest, src) => src.replace('src', '_build')
        }]
      },
      javascript: {
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/src/*-*.js`),
          rename: (dest, src) => src.replace('src', '_build')
        }]
      },
      html: {
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/src/*-*.html`),
          rename: (dest, src) => src.replace('src', '_build')
        }]
      },
    },

    sass: {
      elements: {
        options: {
          sourcemap: 'none',
          outputStyle: 'expanded',
          noCache: true
        },
        files: [{
          expand: true,
          cwd: '',
          src: elements.map(element => `elements/${element}/_build/*-*.scss`),
          dest: '',
          ext: ".css"
        }]
      },
      css: {
        src: "sass/app.scss",
        dest: "app.css"
      }
    },

    /*
      Add css prefixes for compatibility (mainly for Firefox ESL)
     */
    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer')({
            browsers: ['firefox 45', 'last 2 versions']
          })
        ]
      },
      elements: {
        src: elements.map(element => `elements/${element}/_build/*-*.css`)
      },
      css: {
        src: ['app.css'],
        dest: 'app.css'
      }
    },

    /*
      Process JavaScript
     */
    uglify: {
      options: {
        preserveComments: false,
        srewIE8: true,
        sourceMap: true
      },

      js: {
        src: [
          // 'bower_components/webcomponentsjs/webcomponents.min.js',
          'bower_components/moment/moment.js',
          'bower_components/moment-timezone/builds/moment-timezone-with-data.min.js',
          'bower_components/socket-io-client/socket.io.js',
          'bower_components/jwt-decode/build/jwt-decode.js',
          'bower_components/moment/moment.js',
          'bower_components/fly-bira-fly/fly-bira-fly.js',
          'bower_components/jquery/dist/jquery.js',
          'bower_components/trumbowyg/dist/trumbowyg.js',
          'bower_components/trumbowyg/dist/plugins/colors/trumbowyg.colors.js',
          'bower_components/trumbowyg/dist/plugins/cleanpaste/trumbowyg.cleanpaste.js',
          'bower_components/trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.js',
          'bower_components/trumbowyg/dist/plugins/table/trumbowyg.table.js',
          // 'bower_components/trumbowyg/dist/plugins/emoji/trumbowyg.emoji.js',
          'js/*.js',
        ],
        dest: 'app.js'
      },
      elements: {
        options: {
          mangleProperties: false,
          mangle: {},
          sourceMap: false,
          wrap: true,
          ASCIIOnly: true,
          preserveComments: /\**/
        },
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/_build/*.js`),
        }]
      }
    },

    inline: {
      html: {
        files: [{
          expand: true,
          src: elements.map(element => `elements/${element}/_build/*-*.html`),
          rename: (dest, src) => src.replace('_build/', '')
        }]
      }
    },

    htmlmin: {
      options: {
        removeComments: true,
        collapseWhitespace: false,
        ignoreCustomComments: [ /^\n`/ ]
      },
      elements: {
        expand: true,
        cwd: '.',
        src: elements.map(element => `elements/${element}/*-*.html`),
        dest: '.'
      }
    },

    usebanner: {
      elements: {
        options: {
          position: 'top',
          banner: fs.readFileSync(__dirname + '/COPYRIGHT.html', 'utf8'),
          linebreak: true
        },
        files: [{
          expand: true,
          cwd: '.',
          src: elements.map(element => `elements/${element}/*-*.html`),
          dest: '.'
        }]
      }
    },

    clean: {
      elements: {
        options: {
          'no-write': false
        },
        // src: ["src_elements/**/*-min.css"]
        src: [elements.map(element => `elements/${element}/_build`), 'css/*-min.css']
      },
      all: {
        options: {
          'no-write': false
        },
        // src: ["src_elements/**/*-min.css"]
        src: ['elements/*/_build', 'elements/*/*-*.html', 'elements/index.html', 'service-worker.js', 'app.js', 'app.css', 'app.js.map', 'app.css.map']
      }
    },

    /*
     * Generate service worker for smart caching
     */
    swPrecache: {
      dev: {
        cacheId: require('./package.json').name,
        dynamicUrlToDependencies: {},
        // If handleFetch is false (i.e. because this is called from swPrecache:dev), then
        // the service worker will precache resources but won't actually serve them.
        // This allows you to test precaching behavior without worry about the cache preventing your
        // local changes from being picked up during the development cycle.
        handleFetch: true,
        logger: grunt.log.writeln,
        staticFileGlobs: [
          // 'index.html',
          'socket-loader.js',
          'app.js',
          'app.css',
          'js/demo.png',
          'js/demo.mp3',
          'elements/*/*.html',
          'elements/*/media/*',
          'bower_components/webcomponentsjs/*.js',
          'bower_components/shadycss/*.html',
          'bower_components/shadycss/*.js',
          'bower_components/web-animations-js/*.js',
          'bower_components/polymer/*.html',
          'bower_components/polymer/**/*.html',
          'bower_components/font-roboto/*.html',
          'bower_components/app-*/*.html',
          'bower_components/app-*/*/*.html',
          'bower_components/paper-*/*.html',
          'bower_components/paper-*/*/*.html',
          'bower_components/iron-*/*.html',
          'bower_components/iron-*/*/*.html',
          'bower_components/neon-*/*.html',
          'bower_components/neon-*/*/*.html',
          'bower_components/wysiwyg-e/*.html',
          'bower_components/wysiwyg-e/*/*.html',
          //'bower_components/selection-mgr/*.html',
          //'bower_components/undo-mgr/*.html',
          'bower_components/intl-messageformat/*/*.js',
          'favicons/manifest.json',
          '*.svg'
        ],
        stripPrefix: '/',
        // verbose defaults to false, but for the purposes of this demo, log more.
        verbose: false
      }
    },

    prettify: {
      options: {
        indent: 2,
        indent_char: ' ',
        wrap_line_length: 78,
        brace_style: 'end-expand'
      },
      elements: {
        expand: true,
        cwd: '.',
        src: elements.map(element => `elements/${element}/*-*.html`),
        dest: '.'
      }
    },

    /*
     * This will generate all possible favicons
     * this is only executed when specifically asked for by running
     * grunt realFavicon
     */
    realFavicon: {
      favicons: {
        src: 'CERN-logo.png',
        dest: 'favicons',
        options: {
          iconsPath: '/favicons',
          html: ['index.html'],
          design: {
            ios: {
              pictureAspect: 'backgroundAndMargin',
              backgroundColor: '#0053A1',
              margin: '14%',
              assets: {
                ios6AndPriorIcons: false,
                ios7AndLaterIcons: false,
                precomposedIcons: false,
                declareOnlyDefaultIcon: true
              }
            },
            desktopBrowser: {},
            windows: {
              pictureAspect: 'noChange',
              backgroundColor: '#0053A1',
              onConflict: 'override',
              assets: {
                windows80Ie10Tile: false,
                windows10Ie11EdgeTiles: {
                  small: false,
                  medium: true,
                  big: true,
                  rectangle: false
                }
              }
            },
            androidChrome: {
              pictureAspect: 'shadow',
              themeColor: '#0053A1',
              manifest: {
                name: 'L1Page',
                startUrl: 'https://l1page.cms',
                display: 'standalone',
                orientation: 'notSet',
                onConflict: 'override',
                declared: true
              },
              assets: {
                legacyIcon: false,
                lowResolutionIcons: false
              }
            },
            safariPinnedTab: {
              pictureAspect: 'blackAndWhite',
              threshold: 33.90625,
              themeColor: '#0053A1'
            }
          },
          settings: {
            scalingAlgorithm: 'Lanczos',
            errorOnImageTooSmall: false
          }
        }
      }
    }
  });

  require("load-grunt-tasks")(grunt);
  // grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-real-favicon');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-inline');
  // grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-banner');
  grunt.loadNpmTasks('grunt-prettify');
  // grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.registerMultiTask('swPrecache', function() {
    var done = this.async();
    var config = this.data;

    swPrecache.write('service-worker.js', config, error => {
      error ? grunt.fail.warn(error) : done();
    });
  });

  grunt.registerTask('default', ['import', 'sass', 'postcss', 'uglify', 'inline', 'htmlmin', 'prettify', 'usebanner', 'swPrecache', 'clean:elements'])
};
