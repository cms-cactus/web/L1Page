(function() {
  // setTimeout(function () {

  var script = document.createElement('script');
  script.src = location.protocol + "//" + location.hostname + ":3001/socket.io/socket.io.js";
  script.onload = function onSocketLoaded() {
    console.log("socket.io loaded");
    document.dispatchEvent(new CustomEvent('socket-io-loaded'));
  }
  script.onerror = function onSocketFailed() {
    console.log("socket failed");
    var modal = document.createElement('offline-modal');
    document.body.appendChild(modal);
  }
  document.head.appendChild(script);
// }, 10000);
})()
