$.trumbowyg.svgPath = '/bower_components/trumbowyg/dist/ui/icons.svg';
$(document).ready(function() {
  $("#wysiwyg").trumbowyg({
    btns: [
        ['viewHTML'],
        ['formatting'],
        'btnGrp-design',
        ['superscript', 'subscript'],
        ['link'],
        ['insertImage'],
        'btnGrp-justify',
        'btnGrp-lists',
        ['horizontalRule'],
        // ['removeformat'],
        ['foreColor', 'backColor'],
        ['table', 'tableAddRow', 'tableAddColumn'],
        // ['emoji'],
        ['fullscreen']
    ]
  });
})
