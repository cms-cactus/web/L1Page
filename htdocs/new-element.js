#!/usr/bin/env node

var fs = require('fs-extra');
var replace = require("replace");

var fileExists = filename => {
  var fileinfo;
  try {
    fileinfo = fs.statSync(filename);
  } catch (e) {
    fileinfo = null;
  }
  return fileinfo;
}

process.stdin.resume();
process.stdin.setEncoding('utf8');

var elementName;
var parentElement;
var minimal = true;

process.stdout.write('name of the new element: ');
process.stdin.on('data', text => {
  if (!elementName) {
    askElementName(text);
  }
  else if (!parentElement) {
    askParentElement(text);
  }
  else {
    askMinimal(text);
  }
});

askElementName = text => {
  var name = text.replace('\n', '');
  var userIsIdiot = true;

  if (name == '') {
    console.error('\nname cannot be empty');
  } else if (name.indexOf('-') == -1) {
    console.error('\nname must contain a dash (-)');
  } else {
    userIsIdiot = false;
  }

  if (userIsIdiot) {
    process.stdout.write('name of the new element: ');
  } else {
    elementName = name;
    process.stdout.write('name of the parent folder (' + elementName + '): ');
  }
};

askParentElement = text => {
  parentElement = text.replace('\n', '') || elementName;

  process.stdout.write('minimal template? (Y/n):');
}

askMinimal = text => {
  var answer = text.replace('\n', '').toLowerCase();
  if (answer != "y" && answer != "n" && answer != "") {
    process.stdout.write('minimal template? (Y/n):');
  }
  else {
    minimal = answer == "y" || answer == "";
    makeNewElement();
  }
}

makeNewElement = () => {
  var newRoot = "elements/" + parentElement;

  if (fileExists(`${newRoot}/src/${elementName}.html`)) {
    console.error(`element ${parentElement}/${elementName} already exists, exiting`);
    process.exit();
  }
  else {

    var folder = minimal ? "elements/template.min" : "elements/template";
    fs.copySync(folder, newRoot, {
      clobber: true,
      dereference: false
    });
    fs.renameSync(newRoot + "/src/template-element.html", newRoot + "/src/" + elementName + ".html");
    fs.renameSync(newRoot + "/src/template-element.js", newRoot + "/src/" + elementName + ".js");
    fs.renameSync(newRoot + "/src/template-element.scss", newRoot + "/src/" + elementName + ".scss");
    if (!minimal) {
      fs.renameSync(newRoot + "/test/template.html", newRoot + "/test/" + elementName + ".html");
    }

    replace({
      regex: "template-element",
      replacement: elementName,
      paths: [newRoot],
      recursive: true,
      silent: true,
    });
    var className = elementName[0].toUpperCase() + elementName.slice(1).replace(/-([a-z])/g, g => g[1].toUpperCase())
    replace({
      regex: "TemplateElement",
      replacement: className,
      paths: [newRoot],
      recursive: true,
      silent: true,
    });

    console.log("Created element", parentElement + '/' + elementName + '.html');
    process.exit();
  }

}
