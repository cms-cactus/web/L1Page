/*
 * This file contains:
 * - the topology of the subsystems
 * - the configuration for each subsystem (name, url, ...)
 */

const Systemctl = require("./process-controller/_base/Systemctl");
const Script = require("./process-controller/_base/Script");
var {types: healthChecks} = require.main.require("../settings/health_checks.js");


/*
 * This Object describes the topology that will be rendered in the interface.
 * The first layer are the names of sections.
 * The items are rendered in the interface in the order they appear here.
 * Any systems not specified in this object will be put in the 'UNKNOWN' section
 */
// module.exports.topology = {
//   "trigger": {
//       "CENTRAL": {
//           "UGT": {
//               "CALOL2": {
//                   "CALOL1": {}
//               },
//               "uGMT": {
//                   "BMTF": {
//                       "TWINMUX": {}
//                   },
//                   "OMTF": {
//                       "TWINMUX": {}
//                   },
//                   "EMTF": {}
//               }
//           }
//       }
//   },
//   "services": {
//       "L1CE": {},
//       "TFM": {},
//       "XDAQ_Services": {}
//   }
//
// }


/*
 * This Array lists the various subsystems that need monitoring.
 * L1Page watches the TS Flashlist, if any subsystems listed there are not
 * configured here this will be considered a configuration error. The system
 * will be added to this list assuming all default options. (see example)
 *
 * Items from the flashlist are matched by uri, the names can be changed freely.
 */
module.exports.subsystems = {
  // example (all optionals have default values):
  // // name of the subsystem, doesn't have to match with the one in the flashlist
  // "name of my subsystem": {
  //   // optional, control class for the process controller to perform
  //   // status|start|stop|restart
  //   // see ./process-controller/example.js
  //   // if not set, this subsystem will not show up in the process controller
  //   controller:
  //   // list of apps in this subsystem
  //   apps: {
  //     // name of the app, doesn't have to match the flashlist
  //     "name of my subsystem app": {
  //       // url to the app, has to match the flashlist
  //       // the user can try to visit this url
  //       uri: "http://full-url.to/my/app",
  //       telemetry: {
  //         // optional, specifies the basic health check L1Page performs
  //         // this basic checks only checks if the app is still alive or not
  //         healthCheck: healthChecks.httpGetIsOk,
  //         // optional, means that if the healthCheck fails, we are ducked
  //         critical: true
  //         // optional, source for additional app info
  //         // currently, only ts_flashlist is supported (and default)
  //         // if set to null, no info will be fetched
  //         dataSource: healthChecks.ts_flashlist
  //       }
  //       // Label that will be used for displaying this app (e.g. in the subystems-details panel)
  //       // If left empty or undefined, the name of the app will be used
  //       label: 'Fancy Displayed Name'
  //     }
  //   }
  // }

  "TFM": {
    apps: {
      "SUPERVISOR": {
        uri: "http://cmsrc-trigger.cms:19000/rcms/gui/servlet/RunGroupChooserServlet/",
        telemetry: {
          dataSource: null
        }
      }
    }
  },
  "XDAQ_Services": {
    apps: {
      "TRG_SLASH": {
        uri: "http://l1ts-xaas.cms:9945/urn:xdaq-application:lid=16",
        telemetry: {
          critical: false,
          dataSource: null
        }
      },
      "TRG_B2IN-EVENTING": {
        uri: "http://l1ts-xaas.cms:4002",
        telemetry: {
          critical: false,
          dataSource: null
        }
      }
    }
  }
};//end module exports

// some test systems for the health tests
// make sure that the uris point to different pages, otherwise they will get accidentally merged due to the uri matching
// this.subsystems["TEST_SYSTEMS"] = {
//   apps: {
//     "ALWAYS_TRUE": {
//       uri: 'https://www.google.com',
//       telemetry: {
//         critical: false,
//         healthCheck: healthChecks.dummy_check_true,
//         dataSource: null
//       }
//     },
//     "ALWAYS_FALSE": {
//       uri: 'https://www.google.com/maps',
//       telemetry: {
//         critical: true,
//         healthCheck: healthChecks.dummy_check_false,
//         dataSource: null
//       }
//     },
//     'RANDOM_TRUE': {
//       uri: 'https://www.cern.ch',
//       telemetry: {
//         critical: true,
//         healthCheck: healthChecks.dummy_random_true,
//         dataSource: null
//       }
//     }
//   }
// }

this.subsystems["L1CE"] = {
  controller: class extends Script {
    static get actions() { return {
      // action names are free to choose
      status: "sudo pm2 describe l1ce-api",
      start: "sudo pm2 start l1ce-api",
      stop: "sudo pm2 stop l1ce-api",
      reload: "sudo pm2 reload l1ce-api"
    }}
    static get host() { return "l1ce.cms"}
  },
  apps: {
    "API": {
      uri: `https://l1ce.cms`,
      telemetry: {
        dataSource: null
      }
    }
  }
}
this.subsystems["L1Page"] = {
  controller: class extends Script {
    static get actions() { return {
      // action names are free to choose
      status: "sudo pm2 describe l1page-api",
      start: "sudo pm2 start l1page-api",
      stop: "sudo pm2 stop l1page-api",
      reload: "sudo pm2 reload l1page-api"
    }}
    static get host() { return "l1page.cms"}
  },
  apps: {
    "API": {
      uri: `https://l1page.cms`,
      telemetry: {
        dataSource: null
      }
    }
  }
}
for (name of ["CENTRAL"]) {
  let name2 = name == "CENTRAL" ? "central" : name;
  let hostname = `l1ts-${name2.toLowerCase()}.cms`;
  this.subsystems[name] = {
    controller: class extends Systemctl {
      static get host() { return hostname}
    },
    apps: {
      "SUPERVISOR": {
        uri: `http://${hostname}:3333/urn:xdaq-application:lid=13`,
        label: 'Upgrade Central Cell'
      },
      "TSTORE": {
        uri: `http://${hostname}:7000`,
        telemetry: {
          healthCheck: healthChecks.httpGetIsValidTStore,
          dataSource: null
        }
      }
    }
  };
}

// for avoiding some ifs below, define the labels such that they match the names of the flashlist
const _swatchCellLabels = {'UGT': 'uGT', 'UGMT': 'uGMT'};
const _tcdsCellLabels = {'UGT': 'GTUP', 'UGMT': 'MUTFUP'};
for (name of ["UGT", "CALOL1", "CALOL2", "UGMT", "TWINMUX"]) {
  let hostname = `l1ts-${name.toLowerCase()}.cms`;
  let swatchName = _swatchCellLabels[name] || name;
  let tcdsName = _tcdsCellLabels[name] || name;
  this.subsystems[name] = {
    controller: class extends Systemctl {
      static get host() { return hostname}
    },
    apps: {
      "SUPERVISOR": {
        uri: `http://${hostname}:3333/urn:xdaq-application:lid=13`,
        label: `${swatchName} SWATCH Cell`
      },
      "TCDS iCI": {
        uri: `http://${hostname}:4500/urn:xdaq-application:lid=13`,
        label: `${tcdsName} TCDS ICI Cell`
      },
      "TCDS PI": {
        uri: `http://${hostname}:5500/urn:xdaq-application:lid=13`,
        label: `${tcdsName} TCDS PI Cell`
      },
      "TSTORE": {
        uri: `http://${hostname}:7000`,
        telemetry: {
          healthCheck: healthChecks.httpGetIsValidTStore,
          dataSource: null
        }
      }
    }
  };
}
for (name of ["BMTF", "EMTF", "OMTF", "CPPF"]) {
  let hostname = `l1ts-${name.toLowerCase()}.cms`;
  this.subsystems[name] = {
    controller: class extends Systemctl {
      static get host() { return hostname}
    },
    apps: {
      "SUPERVISOR": {
        uri: `http://${hostname}:3333/urn:xdaq-application:lid=13`,
        label: `${name} SWATCH Cell`
      }
    }
  };
}



/*
 * This checks the above config and fills in defaults
 */
 for (var subsystemName in this.subsystems) {
   var subsystemConfig = this.subsystems[subsystemName];
   if (!subsystemConfig.apps) {
     throw new Error(`subsystem configuration for ${subsystemName} contains no apps`);
   }
   for (var appName in subsystemConfig.apps) {
     var appConfig = subsystemConfig.apps[appName];
     if (!appConfig.uri) {
       throw new Error(`subsystem app configuration for ${subsystemName}.${appName} contains no uri`);
     }
     // add app name as default label if it is not defined
     if (!appConfig.label) {
       appConfig.label = appName;
     }

     appConfig.telemetry = appConfig.telemetry || {};
     var telemetry = appConfig.telemetry;
     telemetry.healthCheck = telemetry.healthCheck || healthChecks.httpGetIsOk;
     if (telemetry.critical == undefined) { telemetry.critical = true; }

     telemetry.dataSource = typeof telemetry.dataSource == "undefined" ? healthChecks.ts_flashlist : telemetry.dataSource;
   }
 }
