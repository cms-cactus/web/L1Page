module.exports = {
  "Database": [
    {
      name: "L1 Configuration Editor",
      url: "https://l1ce.cms"
    }
  ],

  "Monitoring": [
    {
      name: "CMS OMS",
      url: "https://cmsoms.cern.ch"
    },
    {
      name: "Run Summary history",
      url: "http://cmswbm.cms/cmsdb/servlet/RunSummary"
      // with specific date range; e.g. http://cmswbm.cms/cmsdb/servlet/RunSummary?TIME_BEGIN=2016.08.15&TIME_END=2016.08.29&STATUS_TRG=on
    },
    {
      name: "CMS DQM Run Registry",
      url: "https://cmswbm2.cern.ch/runregistry/"
    },
    {
      name: "CMS Data Quality [P5]",
      url: "http://dqm-prod-local.cms:8030/dqm/online"
    },
    {
      name: "CMS Data Quality",
      url: "https://cmsweb.cern.ch/dqm/online/session/"
    },
    {
      name: "Trigger Rate Monitoring Tools",
      url: "https://twiki.cern.ch/twiki/bin/view/CMS/RateMonitoringScriptWithReferenceComparison"
    },
    {
      name: "Instructions for HLT monitoring",
      url: "https://twiki.cern.ch/twiki/bin/viewauth/CMS/TriggerShiftHLTGuide"
    }
  ],

  "Documentation": [
    {
      name: "CMS ELOG",
      url: "http://cmsonline.cern.ch/portal/page/portal/CMS%20online%20system/Elog?_piref815_429145_815_429142_429142.strutsAction=%2FviewCat.do%3FcatId%3D3"
    },
    {
      name: "CMS Trigger Online Workbook",
      url: "https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBTrigger"
    },
    {
      name: "CMS Trigger Shifter Guide",
      url: "https://twiki.cern.ch/twiki/bin/view/CMS/TriggerShifterGuide"
    },
    {
      name: "Collision Prescales",
      url: "https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBL1CollisionPrescales"
    }
  ],

  "Online Software": [
    {
      name: "Trigger Function Manager",
      url: "http://cmsrc-trigger.cms:19000/rcms"
    },
    {
      name: "L0 Function Manager",
      url: "http://cmsrc-top.cms:10000/rcms"
    },
    {
      name: "RS Function Manager",
      url: "http://cmsrc-srv.cms:9000/rs_manager/pro/manager.jnlp"
    },
    {
      name: "Log Collector", // is this used at all?
      url: "http://cmsrc-trigger.cms:19007/Collector/Collector"
    }
  ],

  "Vistars": [
    {
      name: "LHC Page 1",
      url: "https://op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC1"
    }
  ]
}
