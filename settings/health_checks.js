/*
 * This Object specifies the various types of health checks that can be performed
 * on subsystem applications.
 */
module.exports.types = {
  httpGetIsOk: "CHECK_HTTP_GET_RETURNS_200",
  httpGetIsValidTStore: "CHECK_HTTP_GET_RETURNS_TSTORE",
  ts_flashlist: "CHECK_FLASHLIST",
  dummy_check_false: "DUMMY_CHECK_FALSE",
  dummy_check_true: "DUMMY_CHECK_TRUE",
  dummy_random_true: "DUMMY_RANDOM_TRUE"
}

module.exports.polling_interval = 5000; // in ms
module.exports.check_timeout = 3000;

/* time before the warning about stale flashlist information shows up */
module.exports.flashlist_stale_warning = {time: 5, unit: 'minutes'};

/*
 * time an app has to not respond to health checks or not receive flashlist
 * information before a notfication email is sent
 */
module.exports.notification_time = {time: 5, unit: 'minutes'};
