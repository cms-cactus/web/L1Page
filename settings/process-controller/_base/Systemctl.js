const Runner = require("./ProcessControlRunner");

module.exports = class Systemctl extends Runner {
  static get actions() { return [
    "status",
    "start",
    "stop",
    "restart"
  ] }
  static get serviceName() { return "trigger.target" }

  static getcommand(action) { return `sudo systemctl ${action} ${this.serviceName}`}
}
