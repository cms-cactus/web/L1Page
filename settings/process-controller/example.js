/**
 * example of controlling a subsystem using systemctl
 */
const Systemctl = require("./_base/Systemctl");

module.exports = class extends Systemctl {
  static get host() { return "l1ts-bmtf.cms"}
}

/**
 * example of controlling a subsystem using systemctl
 * where the subsystem is spread over multiple runners
 */
const Systemctl = require("./_base/Systemctl");

module.exports = class extends Systemctl {
  static get host() { return ["l1ts-bmtf1.cms", "l1ts-bmtf2.cms", "l1ts-bmtf3.cms"]}
}

/**
 * example of controlling a subsystem using a custom script
 */
const Systemctl = require("./_base/Script");

module.exports = class extends Script {
 static get host() { return "l1ts-bmtf.cms"}
 static get actions() { return {
   // action names are free to choose
   status: "/path/to/my/script.sh stats",
   start: "/path/to/my/script.sh up",
   stop: "/path/to/my/script.sh down",
   reload: "/path/to/my/script.sh hup"
 }}
}
