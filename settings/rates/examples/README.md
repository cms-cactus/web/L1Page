This folder contains some example files of how threshold files can look. The
files are loaded through the `require` mechanism of `node.js`. This means that
files ending on `.json` need to be valid JSON files and files ending on `.js`
need to be valid node modules.

Only one file containing *general* thresholds will be loaded, even if there are
multiple files in the directory! (There is also no guarantee which one will be
loaded).

*general* threshold files only specify a `high` and a `low` threshold and will
be applied to all algorithms that do not match any of the names specified in
other threshold files.
