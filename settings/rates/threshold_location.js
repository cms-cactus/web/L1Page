module.exports = {
  // point this to a directory where the rates threshold files are located
  // to see how these rates threshold files should look like, check the examples subfolder and the files there in
  // setting it to the empty string, disables the loading of threshold data
  location: ''
};
