/**
 * This first example doesn't have any business logic
 * It just always returns a suggestion in the form of a string
 */

module.exports = function(runInfo, subsytems) {
  return "I made my very first suggestion on " + new Date();
}
