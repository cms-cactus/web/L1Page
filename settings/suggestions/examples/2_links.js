/**
 * This example is very similar to the simple example
 * However, this returns an object, containing the message and some links
 *
 * A link can point to an external URL, in which case it starts with http:// or https://
 * A link can also point to an internal URL, in which case it starts with /
 */

module.exports = function(runInfo, subsytems) {
  return {
    message: "this is my second suggestion",
    links: {
      "view my code": "https://gitlab.cern.ch/cms-cactus/web/L1Page/blob/master/settings/suggestions/examples/simple2.js",
      "view the whole code": "https://gitlab.cern.ch/cms-cactus/web/L1Page/tree/master",
      "go to the process controller": "/process-controller"
    }
  }
}
