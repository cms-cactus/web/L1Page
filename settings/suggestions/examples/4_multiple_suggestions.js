/**
 * You can also trigger more than one suggestion
 */

module.exports = function(runInfo, subsytems) {
  return [
    "suggestion 1",
    "suggestion 2",
    "suggestion 3"
  ]
}
