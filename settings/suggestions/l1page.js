module.exports = function(runInfo, subsytems) {
  return subsytems.L1Page && subsytems.L1Page.apps.API.alarms.reduce( (suggestions, alarm) => {
    if (alarm.name == 'flashlist validator') {
      var name = alarm.details.substring(4, alarm.details.indexOf(" has no record"));
      return suggestions.concat(`${name} should receive data from the TS flashist.\nIf this persists for more than 5 minutes contact an expert`);
    } else {
      return suggestions;
    }
  }, []);
}
