module.exports = function(runInfo, subsystems) {
  if (runInfo.triggerState == "Running") {
    var suggestions = [];

    // go over all subsystems in run and detect cricital offline apps
    runInfo.includedSubsystems.forEach( subsystemName => {
      var subsystem = subsystems[subsystemName];

      // not all subsystems reported by the runInfo have a counterpart in the monitored subsystems
      var criticalOffline = subsystem && Object.keys(subsystem.apps).some( appName => {
        var app = subsystem.apps[appName];
        return !app.healthy && app.critical;
      });
      if (criticalOffline) {
        if (subsystemName == "UGT") {
          suggestions.push({
            message: "Stop the run, restart the UGT processes, then reconfigure and start a new run",
            links: {
              "go to the process controller": "/process-controller"
            }
          })
        }
        else if (subsystemName == "CALOL1") {
          suggestions.push({
            message: "Call the CaloL1 on-call (16-0176), Stop the run, Restart the CALOL1 process, reconfigure, start new run",
            links: {
              "go to the process controller": "/process-controller"
            }
          })
        }
        else if (subsystemName == "CALOL2") {
          suggestions.push("CALOL2 is down. Keep the run going and restart the cell before the next run")
        }
        else if (subsystemName == "UGMT") {
          suggestions.push("UGMT is down. Keep the run going and restart the cell before the next run. This will probably change once we start to provide rates via the cell, but for the time being follow this procedure")
        }
        else if (subsystemName == "BMTF") {
          suggestions.push("BMTF is down. Keep the run going and restart the cell before the configuration of the next run")
        }
        else if (subsystemName == "EMTF") {
          suggestions.push({
            message: "For the moment ignore the red status of the EMTF cell. It is known issue and it will be fixed soon"
          }, {
            message: "EMTF is down. stop the run, restart the process, then reconfigure then start a new run. If the problem persists call the EMTF DOC.",
            links: {
              "go to the process controller": "/process-controller"
            }
          })
        }
        else if (subsystemName == "OMTF") {
          suggestions.push({
            message: "OMTF is down. First watch to see that it has really stopped and isn't a temporary issue\n" +
            "If it has really stopped, please call OMTF expert during working hours.\n" +
            "It can be restarted at any time, please post an e-log about this if you do",
            links: {
              "go to the process controller": "/process-controller",
              "write an e-log entry": "http://cmsonline.cern.ch/portal/page/portal/CMS%20online%20system/Elog?_piref815_429145_815_429142_429142.strutsAction=%2FviewCat.do%3FcatId%3D3"
            }
          })
        }
        else if (subsystemName == "TWINMUX") {
          suggestions.push("TWINMUX is down. Keep the run going and restart the cell before the configuration of the next run")
        }
        else if (subsystemName == "L1CE") {
          suggestions.push("L1CE is down. Do not stop the run, it is not needed")
        }
      }
    });

    return suggestions;
  }
}
