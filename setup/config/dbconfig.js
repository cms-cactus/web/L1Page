// list of databases and corresponding connection strings from /etc/tnsnames.ora
// currently only 'development' and 'production' are supported

//exports.databaseName = "development";
exports.databaseName = "production";

exports.databases = {
  development: {
    username: "cms_trg_r",
    connectString: "int2r_lb",
    securePath: "/opt/cactus/secure/database/dev/",
    suffix: ".txt"
  },
 production: {
    username: "cms_trg_r",
    connectString: "cms_omds_lb",
    securePath: `/nfshome0/centraltspro/secure/`,
    // securePath: `${__dirname}/../secure/`,
    suffix: ".txt"
 }
}

/* list of recipients for notification e-mails */
exports.notifyTo = [
  'cms-l1t-operations@cern.ch'
]
