/*
 * list of recipients for notification emails when app is not healthy or doesn't
 * receive flashlist information. Leave empty to disable notifications but leave
 * logging of problems
 */
module.exports.crash_notify_recipients = [
  'cactus-l1page-crash-reports@cern.ch',
  'cms-l1t-operations@cern.ch'
];
