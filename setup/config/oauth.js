module.exports = {
  client_id: "l1page.cms",
  redirect_uri: "https://l1page.cms/api/v0/handle_oauth",
  secret: "mchED1WLofIsNkHU0VQ12YIsbytaDFt3HaZ0cSPxH541",
  egroup_whitelist: [
    "cms-cactus",
    "cms-rackwizard",
    "belgian-xmas",
    "cms-B40-secr",
    "cms-p5-l1-docs",
    "cms-l1t-operations",
    "cms-cactus-admins"
  ],

  egroup_admins: [
    "cms-l1t-operations",
    "cms-cactus-admins"
  ]
}
