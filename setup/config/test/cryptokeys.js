// keys are self-signed and have the wrong common name
// not usable for anything other than tests
module.exports = {
  location: "/opt/cactus/L1Page/config/",
  private_key: "shitty.key",
  public_key: "shitty.crt"
}
