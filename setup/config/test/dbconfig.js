exports.databaseName = "development";

exports.databases = {
  development: {
    username: "cms_trg_r",
    connectString: "int2r_lb",
    // securePath: `/nfshome0/centraltspro/secure/dev/`,
    securePath: "/opt/cactus/L1Page/config/test/",
    suffix: ""
  },
  // production: {
  //   username: "CMS_TRG_R",
  //   connectString: "CMS_OMDS_LB",
  //   // securePath: `/nfshome0/centraltspro/secure/`,
  //   securePath: "/opt/cactus/L1Page/config/test/",
  //   suffix: ""
  // }
};

