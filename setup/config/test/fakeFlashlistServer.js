#!/usr/bin/env node

var restify = require('restify');
var fs = require('fs');
var server = restify.createServer();

var flashlist = JSON.parse(fs.readFileSync("/opt/cactus/L1Page/config/fakeFlashlist.json").toString());
server.get("/", (request, response, next) => {
  response.send(flashlist);
  return next();
});
server.listen( 8080, "localhost", () => console.log(`${server.name} listening at ${server.url}`) );
