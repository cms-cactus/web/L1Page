#!/bin/bash

### if it's running in a docker container, only test basics
/bin/systemctl status > /dev/null 2>&1
ISDOCKER=$?
if [[ $ISDOCKER != 0 ]]
then
	### pm2-logrotate
	mkdir -p /var/log/l1page
	cd /opt/cactus/L1Page/setup/pm2-logrotate
	tar -xzf node_modules.tar.gz -C ~
	cp module_conf.json ~/.pm2/module_conf.json
	cd ../../api
	pm2 kill
	sleep 1
	pm2 stop all
	pm2 start l1page-api.json
	pm2 save
	pm2 stop all
	### nginx
	cd ../setup
	rm -f /etc/nginx/conf.d/default.conf
	rm -f /var/log/l1page/*
	exit 0
fi

#######
# PM2 #
#######
# cd /opt/cactus/L1Page/setup/pm2
pm2 startup systemd 
sleep 1
cd /opt/cactus/L1Page/api
pm2 stop all
pm2 start l1page-api.json
pm2 save
cd ../setup

#########
# NGINX #
#########
rm -f /etc/nginx/conf.d/default.conf
systemctl enable nginx
nginx -s stop
sleep 1
nginx
