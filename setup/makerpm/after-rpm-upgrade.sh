#!/bin/bash

#######
# PM2 #
#######
pm2 reload all

#########
# NGINX #
#########
nginx -s reload
