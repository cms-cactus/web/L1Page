# redirect all HTTP traffic to HTTPS
server {
  server_name l1page.cms;
  #add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
  listen 80;
  listen [::]:80;
  location / {
    return 301 https://$host$request_uri;
  }
}

server {
  #add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
  add_header Cache-Control private;

  server_name l1page.cms;
  listen 443 ssl http2;
  listen [::]:443 ssl http2;

  #ssl_certificate /opt/cactus/L1Page/secure/keys/l1page.crt;
  #ssl_certificate_key /opt/cactus/L1Page/secure/keys/l1page.key;
  ssl_certificate /nfshome0/centraltspro/secure/keys/l1page.crt;
  ssl_certificate_key /nfshome0/centraltspro/secure/keys/l1page.key;
  root /opt/cactus/L1Page/htdocs;
  index index.html;

  gzip on;
  gzip_min_length 1;
  gzip_proxied any;
  gzip_types *;

  location ~ service-worker\.js {
    add_header Cache-Control "no-cache, no-store, must-revalidate" always;
    add_header Pragma "no-cache" always;
    add_header Expires "0" always;
  }

  location / {
      #try_files $uri $uri/ =404;
      # this will serve index.html in case of a 404
      # this is how we make html5 pushstate possible
      # we override this behavior for certain folders later on
      try_files $uri $uri/ /index.html;
  }
  location /bower_components {
      try_files $uri $uri/ =404;
  }
  location /elements {
      try_files $uri $uri/ =404;
  }

  location /api {
      proxy_pass http://[::1]:8091;
      proxy_http_version 1.1;
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }

  error_page 404 @404;
  location @404 {
    root /opt/cactus/L1Page/errors;
    try_files /404.html =404;
    internal;
  }
  error_page 500 503 @500;
  location @500 {
    root /opt/cactus/L1Page/errors;
    try_files /50x.html =500;
    internal;
  }
  # to test the 5xx page
  location /fail {
    fastcgi_pass unix:/nope/nope/nope;
  }
  # 502 means the upstream server is down
  error_page 502 @502;
  location @502 {
    default_type application/json;
    return 502 '{"code":"502", "message": "The upstream API server is offline"}';
    internal;
  }
  # 504 means the upstream server is down
  error_page 504 @504;
  location @504 {
    default_type application/json;
    return 504 '{"code":"504", "message": "The upstream API server timed out for this request"}';
    internal;
  }
}

# websocket connection
server {

  listen 3001 ssl http2;
  listen [::]:3001 ssl http2;
  server_name l1page.cms;

  #ssl_certificate /opt/cactus/L1Page/secure/keys/l1page.crt;
  #ssl_certificate_key /opt/cactus/L1Page/secure/keys/l1page.key;
  ssl_certificate /nfshome0/centraltspro/secure/keys/l1page.crt;
  ssl_certificate_key /nfshome0/centraltspro/secure/keys/l1page.key;
  location / {
    proxy_pass http://[::1]:3010;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    # STOP PUTTING '*' IN ACCESS-CONTROL-ALLOW-ORIGIN!
    proxy_hide_header 'Access-Control-Allow-Origin';
    proxy_hide_header 'Access-Control-Allow-Credentials';
    add_header 'Access-Control-Allow-Origin' https://l1page.cms always;
    add_header 'Access-Control-Allow-Credentials' true always;
  }

  # 502 means the upstream server is down
  error_page 502 @502;
  location @502 {
    default_type application/json;
    add_header 'Access-Control-Allow-Origin' https://l1page.cms always;
    add_header 'Access-Control-Allow-Credentials' "true" always;
    return 502 '{"code":"502", "message": "The upstream API server is offline"}';
    internal;
  }
}
