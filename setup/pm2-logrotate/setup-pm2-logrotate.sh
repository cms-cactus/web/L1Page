#!/bin/bash

#################
# PM2 logrotate #
#################

### copy logrotate config
mkdir -p /var/log/l1page
pm2 install pm2-logrotate
cp /opt/cactus/L1Page/setup/pm2-logrotate/module_conf.json ~/.pm2/module_conf.json

### kill runnning instances
pm2 kill
sleep 1

### setup script for pm2 at boot
pm2 startup systemd
sleep 1
pm2 stop all

### start brand new pm2 config
cd /opt/cactus/L1Page/api
pm2 start l1page-api.json
pm2 set pm2-logrotate:compress true
pm2 set pm2-logrotate:workerInterval 60
pm2 save

pm2 reload all
